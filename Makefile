# Developed at ASC ANL
# May 2014 

include ./Makefile.arch
#include ${ROOTSYS}/etc/Makefile.arch

ROOTCFLAGS    = $(shell root-config --nonew --cflags)
ROOTLIBS      = $(shell root-config --nonew --libs)
ROOTGTTLIBS   = $(shell root-config --nonew --glibs)
CXXFLAGS     += $(ROOTCFLAGS)
LIBS         += $(ROOTLIBS) -lxml2 -lXMLIO -lXMLParser -lz -lTMVA
#LIBS         +=$(shell addLibs $(PWD)/lib_ext/RootCore/lib)
LIBS         += -L./lib_ext/RootCore/lib
LIBS         += -lPileupReweighting
LIBS         += -lPATCore -lElectronEfficiencyCorrection -legammaAnalysisUtils 
LIBS         += -lGoodRunsLists
LIBS         += -lMuonEfficiencyCorrections -lMuonMomentumCorrections -lTrigMuonEfficiency
LIBS         += -lTrigRootAnalysis 
LIBS         += -lElectronPhotonSelectorTools 
LIBS         += -lMuonIsolationCorrection 
LIBS         += -lMissingETUtility
LIBS         += -lIsolationScaleFactors

SOURCE_FILES := $(shell ls -1 *.cxx)
SOURCE_FILES += $(shell ls -1 src/*.cxx)

INCLUDE1= -I./inc
INCLUDE2= -I.
INCLUDE3= -I./lib_ext/RootCore/include
INCLUDE4= -I./lib_ext/TrigRootAnalysis/TrigRootAnalysis -I./lib_ext/
 

# build object files 
objects       = $(patsubst %.cxx,%.o,$(SOURCE_FILES))


%.o: %.cxx
	$(CXX) $(OPT) $(CXXFLAGS) $(INCLUDE1) $(INCLUDE2) $(INCLUDE3) $(INCLUDE4) -o $@ -c $<

Tasks:   main  

goodrun:
	make -C GoodRuns/cmt -f Makefile.Standalone
	make -C GoodRuns/cmt -f Makefile.Standalone static

tar:
	rm -f ana.tgz; cd ../; tar -cvzf ana.tgz --exclude='*.o' --exclude='*.root' \
	--exclude='*.log'  --exclude='*.tup' --exclude='*.eps' --exclude='*.png' --exclude='*.log.gz'\
	--exclude='main' --exclude='*.root.log.gz' --exclude='*.so' --exclude='*~' --exclude='*.pyc' ana; \
        mv ana.tgz ana/; cd ana; 

main: $(objects)
	$(LD) $(LDFLAGS) $^ $(LIBS) $(OutPutOpt)$@

clean:
	@rm -f *.o  main *~ src/*.o src/*~ inc/*~; echo "Clear.." 
