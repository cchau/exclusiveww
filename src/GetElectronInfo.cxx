/*
 *      Cut.cxx
 *      
 *      
 */


#include "Ana.h"
#include "Global.h"
#include "Histo.h"

#include<stdio.h>
#include<stdlib.h>
#include "SystemOfUnits.h"
#include<TROOT.h>
#include<stdio.h>
#include<TMath.h>
#include<iostream>
#include<string.h>
#include<vector>


extern Histo h;
extern Global glob;

//
// Retrieve electron info from the el_ container or the local pt-ordered vector
// Input: isLeading = 1 for leading lepton, 2 for subleading
//
int Ana::GetElectronInfo(int isLeading, LParticle & electron_lp)
{
    std::vector<Double32_t> par = electron_lp.GetParameters();
    if (isLeading == 1) { //if true: leading lepton
        glob.lepPt1 = (isMC) ? par.at(EL_cor_Et): par.at(EL_Et);
        glob.lepEta1 = par.at(EL_eta);
        glob.lepPhi1 = par.at(EL_phi);
        glob.lepM1 = par.at(EL_m);
        glob.lepCharge1 = par.at(EL_charge);
        glob.lepIsMediumPP1 = par.at(EL_mediumPP);
        //glob.lepIsTight1;
        glob.lepIsTightPP1 = par.at(EL_tightPP);
        //glob.lepIsMediumPPIso1;
        //glob.lepIsTightPPIso1;
        glob.lepIsVeryTightLH1 = par.at(EL_veryTightLH);
        glob.lepIsCombined1 = 0;
        glob.lepEtcone20_1 = par.at(EL_cor_Etcone20);
        //glob.lepEtcone20_1 = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(i), el_cl_E->at(i), el_etas2->at(i), el_etap->at(i), el_cl_eta->at(i), 20., (isMC? 1: 0), el_topoEtcone20->at(i));
        glob.lepEtcone30_1 = par.at(EL_cor_Etcone30);
        //glob.lepEtcone30_1 = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(i), el_cl_E->at(i), el_etas2->at(i), el_etap->at(i), el_cl_eta->at(i), 30., (isMC? 1: 0), el_topoEtcone30->at(i));
        glob.lepEtcone40_1 = par.at(EL_cor_Etcone40);
        //glob.lepEtcone40_1 = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(i), el_cl_E->at(i), el_etas2->at(i), el_etap->at(i), el_cl_eta->at(i), 40., (isMC? 1: 0), el_topoEtcone40->at(i));
        glob.lepPtcone20_1 = par.at(EL_ptcone20);
        glob.lepPtcone30_1 = par.at(EL_ptcone30);
        glob.lepPtcone40_1 = par.at(EL_ptcone40);
        glob.lepTrackPt1 = par.at(EL_trackpt);
        glob.lepTrackEta1 = par.at(EL_tracketa);
        glob.lepTrackPhi1 = par.at(EL_trackphi);
        glob.lepTrackz0pv1 = par.at(EL_trackz0pv);
        glob.lepTrackd0pv1 = par.at(EL_trackd0pv);
        glob.lepTrackz0beam1 = par.at(EL_trackz0beam);
        glob.lepTrackd0beam1 = par.at(EL_trackd0beam);
        glob.lepTrackTheta1 = par.at(EL_tracktheta);
        glob.lepTrackz0pvunbiased1 = par.at(EL_trackz0pvunbiased);
        glob.lepTrackd0pvunbiased1 = par.at(EL_trackd0pvunbiased);
        glob.lepTracksigd0pvunbiased1 = par.at(EL_tracksigd0pvunbiased);
        glob.lepnPixHits1 = par.at(EL_nPixHits);
        glob.lepnSCTHits1 = par.at(EL_nSCTHits);
        glob.lepnPixDeadSensors1 = par.at(EL_nPixelDeadSensors);
        glob.lepnSCTDeadSensors1 = par.at(EL_nSCTDeadSensors);
        glob.lepnPixHoles1 = par.at(EL_nPixHoles);
        glob.lepnSCTHoles1 = par.at(EL_nSCTHoles);

        // electron can be medium++, tight or tight++; however, there are scale factor only for medium or tight. 
        if(isMC ) {
            glob.lepRecoSFWeight1 = par.at(EL_recoSF);
            glob.lepRecoSFWeight1_sys = par.at(EL_recoSF_sys);
            if (glob.lepIsVeryTightLH1) {
                glob.lepIDSFWeight1 = par.at(EL_idSF_vTightLH); 
                glob.lepIDSFWeight1_sys = par.at(EL_idSF_vTightLH_sys); 
            } else if (glob.lepIsTight1 || glob.lepIsTightPP1 || glob.lepIsTightPPIso1) { 
                glob.lepIDSFWeight1 = par.at(EL_idSF_tight);
                glob.lepIDSFWeight1_sys = par.at(EL_idSF_tight_sys);
            } else if (glob.lepIsMediumPP1 || glob.lepIsMediumPPIso1) {
                glob.lepIDSFWeight1 = par.at(EL_idSF_medium);
                glob.lepIDSFWeight1_sys = par.at(EL_idSF_medium_sys);
            }
            glob.lepIsolationSF1 = par.at(EL_isoSF);
            glob.lepIsolationSF1_sys = par.at(EL_isoSF_sys);
        } else {
            glob.lepRecoSFWeight1 = 1.;
            glob.lepIDSFWeight1 = 1.; 
            glob.lepIsolationSF1 = 1.;
            glob.lepRecoSFWeight1_sys = 0.;
            glob.lepIDSFWeight1_sys = 0.; 
            glob.lepIsolationSF1_sys = 0.;
        }

    } else if (isLeading == 2) { //if true: subleading lepton

        glob.lepPt2 = (isMC) ? par.at(EL_cor_Et): par.at(EL_Et);
        glob.lepEta2 = par.at(EL_eta);
        glob.lepPhi2 = par.at(EL_phi);
        glob.lepM2 = par.at(EL_m);
        glob.lepCharge2 = par.at(EL_charge);
        glob.lepIsMediumPP2 = par.at(EL_mediumPP);
        //glob.lepIsTight2;
        glob.lepIsTightPP2 = par.at(EL_tightPP);
        //glob.lepIsMediumPPIso2;
        //glob.lepIsTightPPIso2;
        glob.lepIsVeryTightLH2 = par.at(EL_veryTightLH);
        glob.lepIsCombined2 = 0;
        glob.lepEtcone20_2 = par.at(EL_cor_Etcone20);
        glob.lepEtcone30_2 = par.at(EL_cor_Etcone30);
        glob.lepEtcone40_2 = par.at(EL_cor_Etcone40);
        glob.lepPtcone20_2 = par.at(EL_ptcone20);
        glob.lepPtcone30_2 = par.at(EL_ptcone30);
        glob.lepPtcone40_2 = par.at(EL_ptcone40);
        glob.lepTrackPt2 = par.at(EL_trackpt);
        glob.lepTrackEta2 = par.at(EL_tracketa);
        glob.lepTrackPhi2 = par.at(EL_trackphi);
        glob.lepTrackz0pv2 = par.at(EL_trackz0pv);
        glob.lepTrackd0pv2 = par.at(EL_trackd0pv);
        glob.lepTrackz0beam2 = par.at(EL_trackz0beam);
        glob.lepTrackd0beam2 = par.at(EL_trackd0beam);
        glob.lepTrackTheta2 = par.at(EL_tracktheta);
        glob.lepTrackz0pvunbiased2 = par.at(EL_trackz0pvunbiased);
        glob.lepTrackd0pvunbiased2 = par.at(EL_trackd0pvunbiased);
        glob.lepTracksigd0pvunbiased2 = par.at(EL_tracksigd0pvunbiased);
        glob.lepnPixHits2 = par.at(EL_nPixHits);
        glob.lepnSCTHits2 = par.at(EL_nSCTHits);
        glob.lepnPixDeadSensors2 = par.at(EL_nPixelDeadSensors);
        glob.lepnSCTDeadSensors2 = par.at(EL_nSCTDeadSensors);
        glob.lepnPixHoles2 = par.at(EL_nPixHoles);
        glob.lepnSCTHoles2 = par.at(EL_nSCTHoles);

        // electron can be medium++, tight or tight++; however, there are scale factor only for medium or tight. 
        if(isMC ) {
            glob.lepRecoSFWeight2 = par.at(EL_recoSF);
            glob.lepRecoSFWeight2_sys = par.at(EL_recoSF_sys);
            if (glob.lepIsVeryTightLH2) {
                glob.lepIDSFWeight2 = par.at(EL_idSF_vTightLH); 
                glob.lepIDSFWeight2_sys = par.at(EL_idSF_vTightLH_sys); 
            } else if (glob.lepIsTight2 || glob.lepIsTightPP2 || glob.lepIsTightPPIso2) {
                glob.lepIDSFWeight2 = par.at(EL_idSF_tight);
                glob.lepIDSFWeight2_sys = par.at(EL_idSF_tight_sys);
            } else if (glob.lepIsMediumPP2 || glob.lepIsMediumPPIso2) {
                glob.lepIDSFWeight2 = par.at(EL_idSF_medium);
                glob.lepIDSFWeight2_sys = par.at(EL_idSF_medium_sys);
            }
            glob.lepIsolationSF2 = par.at(EL_isoSF);
            glob.lepIsolationSF2_sys = par.at(EL_isoSF_sys);
        } else {
            glob.lepRecoSFWeight2 = 1.;
            glob.lepIDSFWeight2 = 1.; 
            glob.lepIsolationSF2 = 1.;
            glob.lepRecoSFWeight2_sys = 0.;
            glob.lepIDSFWeight2_sys = 0.; 
            glob.lepIsolationSF2_sys = 0.;
        }

    } else { // wrong value: something is wrong
        std::cout << "ERROR: GEtElectronInfo cannot retrieve info for leptons other than the two leading leptons" << std::endl;
        return -1;
    }
    //std::cout << "INFO: Done retrieving electron info" << std::endl;

    return 0;
}
