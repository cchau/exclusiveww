// Developed at ASC ANL

#include <iostream>
using namespace std;

#include<iostream>
#include"TFile.h"
#include"TH1D.h"
#include"TH2F.h"
#include"Histo.h"
#include "Ana.h"
#include<stdlib.h>

#include<TClonesArray.h>
#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include"Global.h"
#include "TMultiGraph.h"
#include "TCanvas.h"

extern string floatToString(float); 
extern Global glob;

// constructor
Histo::Histo() {} 


// constructor
void Histo::setOutput() {

		ffile="output.root";
    cout << "\n -> Output file is =" << ffile << endl;
    RootFile = new TFile(ffile, "RECREATE", "Histogram file");


}




// constructor
void Histo::setHistograms() { 

	dilep = new TTree("dilep","dilepton");
	dilep->Branch("lbn", &glob.lbn);
	dilep->Branch("bcid", &glob.bcid);
    dilep->Branch("datasetID", &glob.dsID);
    dilep->Branch("XSection", &glob.xSection);
    dilep->Branch("kfactor", &glob.kFactor);
    dilep->Branch("filterEff", &glob.filterEff);
    dilep->Branch("NEvents", &glob.Nevents);
    dilep->Branch("isAFII", &glob.isAFII);
	dilep->Branch("mc_event_weight", &glob.mc_event_weight);	
	dilep->Branch("mcEventWeight", &glob.mcEventWeight);	
	dilep->Branch("pileUpEventWeight", &glob.pileUpEventWeight);	
	dilep->Branch("vertexPositionWeight", &glob.vertexPositionWeight);	
	dilep->Branch("averageIntPerXing", &glob.averageIntPerXing);	
	dilep->Branch("runNumber", &glob.runNumber);	
	dilep->Branch("eventNumber", &glob.eventNumber);	
	dilep->Branch("channel", &glob.channel);	
	dilep->Branch("larError", &glob.larError);	
	dilep->Branch("tileError", &glob.tileError);	
	dilep->Branch("EF_e24vhi_medium1", &glob.EF_e24vhi_medium1);
	dilep->Branch("EF_e60_medium1", &glob.EF_e60_medium1);
	dilep->Branch("EF_mu24i_tight", &glob.EF_mu24i_tight);
	dilep->Branch("EF_mu36_tight", &glob.EF_mu36_tight);
	dilep->Branch("EF_mu18_tight_mu8_EFFS", &glob.EF_mu18_tight_mu8_EFFS);
	dilep->Branch("EF_2e12Tvh_loose1", &glob.EF_2e12Tvh_loose1);
	dilep->Branch("EF_e12Tvh_medium1_mu8", &glob.EF_e12Tvh_medium1_mu8);
	dilep->Branch("nMuAll", &glob.nMuAll);	
	dilep->Branch("nMu", &glob.nMu);	
	dilep->Branch("nEleAll", &glob.nEleAll);	
	dilep->Branch("nEle", &glob.nEle);	
	dilep->Branch("lepPt1", &glob.lepPt1);	
	dilep->Branch("lepPt2", &glob.lepPt2);	
	dilep->Branch("lepEta1", &glob.lepEta1);	
	dilep->Branch("lepEta2", &glob.lepEta2);	
	dilep->Branch("lepPhi1", &glob.lepPhi1);	
	dilep->Branch("lepPhi2", &glob.lepPhi2);	
	dilep->Branch("lepM1", &glob.lepM1);
	dilep->Branch("lepM2", &glob.lepM2);
	dilep->Branch("lepPDGID1", &glob.lepPDGID1);
	dilep->Branch("lepPDGID2", &glob.lepPDGID2);
	dilep->Branch("lepCharge1", &glob.lepCharge1);	
	dilep->Branch("lepCharge2", &glob.lepCharge2);	
	dilep->Branch("lepIsMediumPP1", &glob.lepIsMediumPP1);	
	dilep->Branch("lepIsMediumPP2", &glob.lepIsMediumPP2);	
	dilep->Branch("lepIsTight1", &glob.lepIsTight1);	
	dilep->Branch("lepIsTight2", &glob.lepIsTight2);	
	dilep->Branch("lepIsTightPP1", &glob.lepIsTightPP1);	
	dilep->Branch("lepIsTightPP2", &glob.lepIsTightPP2);	
	dilep->Branch("lepIsMediumPPIso1", &glob.lepIsMediumPPIso1);	
	dilep->Branch("lepIsMediumPPIso2", &glob.lepIsMediumPPIso2);	
	dilep->Branch("lepIsTightPPIso1", &glob.lepIsTightPPIso1);	
	dilep->Branch("lepIsTightPPIso2", &glob.lepIsTightPPIso2);	
	dilep->Branch("lepIsVeryTightLH1", &glob.lepIsVeryTightLH1);	
	dilep->Branch("lepIsVeryTightLH2", &glob.lepIsVeryTightLH2);	
	dilep->Branch("lepIsCombined1", &glob.lepIsCombined1);
	dilep->Branch("lepIsCombined2", &glob.lepIsCombined2);
	dilep->Branch("lepEtcone20_1", &glob.lepEtcone20_1);
	dilep->Branch("lepEtcone20_2", &glob.lepEtcone20_2);
	dilep->Branch("lepEtcone30_1", &glob.lepEtcone30_1);
	dilep->Branch("lepEtcone30_2", &glob.lepEtcone30_2);
	dilep->Branch("lepEtcone40_1", &glob.lepEtcone40_1);
	dilep->Branch("lepEtcone40_2", &glob.lepEtcone40_2);
	dilep->Branch("lepPtcone20_1", &glob.lepPtcone20_1);
	dilep->Branch("lepPtcone20_2", &glob.lepPtcone20_2);
	dilep->Branch("lepPtcone30_1", &glob.lepPtcone30_1);
	dilep->Branch("lepPtcone30_2", &glob.lepPtcone30_2);
	dilep->Branch("lepPtcone40_1", &glob.lepPtcone40_1);
	dilep->Branch("lepPtcone40_2", &glob.lepPtcone40_2);
	dilep->Branch("lepTrackPt1", &glob.lepTrackPt1);
	dilep->Branch("lepTrackPt2", &glob.lepTrackPt2);
	dilep->Branch("lepTrackEta1", &glob.lepTrackEta1);
	dilep->Branch("lepTrackEta2", &glob.lepTrackEta2);
	dilep->Branch("lepTrackPhi1", &glob.lepTrackPhi1);
	dilep->Branch("lepTrackPhi2", &glob.lepTrackPhi2);
	//dilep->Branch("lepTrackqoverp1", &glob.lepTrackqoverp1);
	//dilep->Branch("lepTrackqoverp2", &glob.lepTrackqoverp2);
	dilep->Branch("lepTrackz0beam1", &glob.lepTrackz0beam1);
	dilep->Branch("lepTrackz0beam2", &glob.lepTrackz0beam2);
	dilep->Branch("lepTrackd0beam1", &glob.lepTrackd0beam1);
	dilep->Branch("lepTrackd0beam2", &glob.lepTrackd0beam2);
	//dilep->Branch("absD0ll", &glob.absD0ll);
	dilep->Branch("lepTrackTheta1", &glob.lepTrackTheta1);
	dilep->Branch("lepTrackTheta2", &glob.lepTrackTheta2);
	dilep->Branch("lepTrackz0pvunbiased1", &glob.lepTrackz0pvunbiased1);
	dilep->Branch("lepTrackz0pvunbiased2", &glob.lepTrackz0pvunbiased2);
	dilep->Branch("lepTrackd0pvunbiased1", &glob.lepTrackd0pvunbiased1);
	dilep->Branch("lepTrackd0pvunbiased2", &glob.lepTrackd0pvunbiased2);
	dilep->Branch("lepTracksigd0pvunbiased1", &glob.lepTracksigd0pvunbiased1);
	dilep->Branch("lepTracksigd0pvunbiased2", &glob.lepTracksigd0pvunbiased2);
	dilep->Branch("lepnPixHits1", &glob.lepnPixHits1);
	dilep->Branch("lepnPixHits2", &glob.lepnPixHits2);
	dilep->Branch("lepnSCTHits1", &glob.lepnSCTHits1);
	dilep->Branch("lepnSCTHits2", &glob.lepnSCTHits2);
	dilep->Branch("lepnSCTHoles1", &glob.lepnSCTHoles1);
	dilep->Branch("lepnSCTHoles2", &glob.lepnSCTHoles2);
	dilep->Branch("lepnPixHoles1", &glob.lepnPixHoles1);
	dilep->Branch("lepnPixHoles2", &glob.lepnPixHoles2);
	dilep->Branch("lepnPixDeadSensors1", &glob.lepnPixDeadSensors1);
	dilep->Branch("lepnPixDeadSensors2", &glob.lepnPixDeadSensors2);
	dilep->Branch("lepnSCTDeadSensors1", &glob.lepnSCTDeadSensors1);
	dilep->Branch("lepnSCTDeadSensors2", &glob.lepnSCTDeadSensors2);
	dilep->Branch("lepIDSFWeight1", &glob.lepIDSFWeight1);
	dilep->Branch("lepIDSFWeight2", &glob.lepIDSFWeight2);
	dilep->Branch("lepIDSFWeight1_sys", &glob.lepIDSFWeight1_sys);
	dilep->Branch("lepIDSFWeight2_sys", &glob.lepIDSFWeight2_sys);
	dilep->Branch("lepRecoSFWeight1", &glob.lepRecoSFWeight1);
	dilep->Branch("lepRecoSFWeight2", &glob.lepRecoSFWeight2);
	dilep->Branch("lepRecoSFWeight1_sys", &glob.lepRecoSFWeight1_sys);
	dilep->Branch("lepRecoSFWeight2_sys", &glob.lepRecoSFWeight2_sys);
	dilep->Branch("lepIsolationSF1", &glob.lepIsolationSF1);
	dilep->Branch("lepIsolationSF2", &glob.lepIsolationSF2);
	dilep->Branch("lepIsolationSF1_sys", &glob.lepIsolationSF1_sys);
	dilep->Branch("lepIsolationSF2_sys", &glob.lepIsolationSF2_sys);
	dilep->Branch("lepTrigMatched1", &glob.lepTrigMatched1);	
	dilep->Branch("lepTrigMatched2", &glob.lepTrigMatched2);	
	dilep->Branch("dilepTrigMatched", &glob.dilepTrigMatched);	
	//dilep->Branch("dPhill", &glob.dPhill);	
	dilep->Branch("mll", &glob.mll);	
	dilep->Branch("pTll", &glob.pTll);	
	//dilep->Branch("trigSF_single_eTight", &glob.trigSF_single_eTight);
	//dilep->Branch("trigSF_single_eMedium", &glob.trigSF_single_eMedium);
	dilep->Branch("trigSF_single_eVTLH", &glob.trigSF_single_eVTightLH);
	dilep->Branch("dilepAverageZ0", &glob.dilepAverageZ0);
	dilep->Branch("dilepdz0", &glob.dilepdz0);
	dilep->Branch("nTrkUnmatched", &glob.nTrkUnmatched);
	dilep->Branch("nTrkUnmatched_100mm", &glob.nTrkUnmatched_100mm);
	dilep->Branch("nTrkUnmatched_125mm", &glob.nTrkUnmatched_125mm);
	dilep->Branch("nTrkUnmatched_150mm", &glob.nTrkUnmatched_150mm);
	dilep->Branch("nTrkUnmatched_175mm", &glob.nTrkUnmatched_175mm);
	dilep->Branch("nTrkUnmatched_200mm", &glob.nTrkUnmatched_200mm);
	dilep->Branch("nTrkUnmatched_225mm", &glob.nTrkUnmatched_225mm);
	dilep->Branch("nTrkUnmatched_250mm", &glob.nTrkUnmatched_250mm);
	dilep->Branch("nTrkUnmatched_275mm", &glob.nTrkUnmatched_275mm);
	dilep->Branch("nTrkUnmatched_300mm", &glob.nTrkUnmatched_300mm);
	dilep->Branch("trkdz0_0", &glob.trkdz0_0);
	dilep->Branch("trkdz0_1", &glob.trkdz0_1);
	dilep->Branch("trkdz0_2", &glob.trkdz0_2);
	dilep->Branch("trkdz0_3", &glob.trkdz0_3);
	dilep->Branch("trkdz0_4", &glob.trkdz0_4);
	dilep->Branch("trkdz0_5", &glob.trkdz0_5);
	dilep->Branch("trkdz0_6", &glob.trkdz0_6);
	dilep->Branch("trkdz0_7", &glob.trkdz0_7);
	dilep->Branch("trkdz0_8", &glob.trkdz0_8);
	dilep->Branch("trkdz0_9", &glob.trkdz0_9);
	dilep->Branch("trkpt_0", &glob.trkpt_0);
	dilep->Branch("trkpt_1", &glob.trkpt_1);
	dilep->Branch("trkpt_2", &glob.trkpt_2);
	dilep->Branch("trkpt_3", &glob.trkpt_3);
	dilep->Branch("trkpt_4", &glob.trkpt_4);
	dilep->Branch("trkpt_5", &glob.trkpt_5);
	dilep->Branch("trkpt_6", &glob.trkpt_6);
	dilep->Branch("trkpt_7", &glob.trkpt_7);
	dilep->Branch("trkpt_8", &glob.trkpt_8);
	dilep->Branch("trkpt_9", &glob.trkpt_9);
	dilep->Branch("trknpix_0", &glob.trknpix_0);
	dilep->Branch("trknpix_1", &glob.trknpix_1);
	dilep->Branch("trknpix_2", &glob.trknpix_2);
	dilep->Branch("trknpix_3", &glob.trknpix_3);
	dilep->Branch("trknpix_4", &glob.trknpix_4);
	dilep->Branch("trknpix_5", &glob.trknpix_5);
	dilep->Branch("trknpix_6", &glob.trknpix_6);
	dilep->Branch("trknpix_7", &glob.trknpix_7);
	dilep->Branch("trknpix_8", &glob.trknpix_8);
	dilep->Branch("trknpix_9", &glob.trknpix_9);
	dilep->Branch("trknsct_0", &glob.trknsct_0);
	dilep->Branch("trknsct_1", &glob.trknsct_1);
	dilep->Branch("trknsct_2", &glob.trknsct_2);
	dilep->Branch("trknsct_3", &glob.trknsct_3);
	dilep->Branch("trknsct_4", &glob.trknsct_4);
	dilep->Branch("trknsct_5", &glob.trknsct_5);
	dilep->Branch("trknsct_6", &glob.trknsct_6);
	dilep->Branch("trknsct_7", &glob.trknsct_7);
	dilep->Branch("trknsct_8", &glob.trknsct_8);
	dilep->Branch("trknsct_9", &glob.trknsct_9);
	dilep->Branch("nTracksAll", &glob.nTracksAll);
	//dilep->Branch("nJets", &glob.nJets);
	//dilep->Branch("nJetsB4", &glob.nJetsB4);
	//dilep->Branch("jetEta1", &glob.jetEta1);
	//dilep->Branch("jetPhi1", &glob.jetPhi1);
	//dilep->Branch("jetPt1", &glob.jetPt1);
	//dilep->Branch("transMass", &glob.transMass);
	//dilep->Branch("transMass_recalculated", &glob.transMass_recalculated);
	//dilep->Branch("met", &glob.met);
	//dilep->Branch("MET_default", &glob.MET_default);
	//dilep->Branch("MET_default_x", &glob.MET_default_x);
	//dilep->Branch("MET_default_y", &glob.MET_default_y);
	//dilep->Branch("nom_met_recalculated", &glob.nom_met_recalculated);
	//dilep->Branch("nom_metx_recalculated", &glob.nom_metx_recalculated);
	//dilep->Branch("nom_mety_recalculated", &glob.nom_mety_recalculated);
	dilep->Branch("lepNMatchedTrk1", &glob.lepNMatchedTrk1);
	dilep->Branch("lepNMatchedTrk2", &glob.lepNMatchedTrk2);
	dilep->Branch("matchedTrkPt1_0", &glob.matchedTrkPt1_0);
	dilep->Branch("matchedTrkPt1_1", &glob.matchedTrkPt1_1);
	dilep->Branch("matchedTrkPt1_2", &glob.matchedTrkPt1_2);
	dilep->Branch("matchedTrkPt1_3", &glob.matchedTrkPt1_3);
	dilep->Branch("matchedTrkPt2_0", &glob.matchedTrkPt2_0);
	dilep->Branch("matchedTrkPt2_1", &glob.matchedTrkPt2_1);
	dilep->Branch("matchedTrkPt2_2", &glob.matchedTrkPt2_2);
	dilep->Branch("matchedTrkPt2_3", &glob.matchedTrkPt2_3);
	dilep->Branch("trkLep1_ptWidth", &glob.matchedLep1_width);
	dilep->Branch("trkLep1_coreResidue", &glob.matchedLep1_coreRes);
	dilep->Branch("trkLep1_haloResidue", &glob.matchedLep1_softRes);
	dilep->Branch("trkLep1_deltaR", &glob.matchedLep1_dr);
	dilep->Branch("trkLep2_ptWidth", &glob.matchedLep2_width);
	dilep->Branch("trkLep2_coreResidue", &glob.matchedLep2_coreRes);
	dilep->Branch("trkLep2_haloResidue", &glob.matchedLep2_softRes);
	dilep->Branch("trkLep2_deltaR", &glob.matchedLep2_dr);
	dilep->Branch("nVxp", &glob.nVxp);
	dilep->Branch("vtx_matched", &glob.vtx_index);
	dilep->Branch("vtx_dz", &glob.dvtx);
	dilep->Branch("vtx_dzPV", &glob.dvtxPV);
	dilep->Branch("vtx_nTrk_closest", &glob.vtx_nTracks);
	dilep->Branch("vtx_nTrk_PV", &glob.vtx_nTracksPV);
	dilep->Branch("vtxpv_matched", &glob.vtxpv_index);
	dilep->Branch("vtxpv_dz", &glob.dvtxpv);
	dilep->Branch("vtxpv_dzPV", &glob.dvtxpvPV);
	dilep->Branch("vtxpv_nTrk_closest", &glob.vtxpv_nTracks);
	dilep->Branch("vtxpv_nTrk_PV", &glob.vtxpv_nTracksPV);

	//dilep->Branch("ele_Et_vec", &glob.vec_ele_Et);
	//dilep->Branch("ele_EtCorrected_vec", &glob.ele_Etcor);
	//dilep->Branch("ele_recoSF_vec", &glob.ele_recoSF);
	//dilep->Branch("ele_vTightLHSF_vec", &glob.ele_vTightLHSF);
	//dilep->Branch("mu_pt_vec", &glob.vec_mu_pt);
	//dilep->Branch("mu_ptCorrected_vec", &glob.mu_ptCor);
	//dilep->Branch("mu_IDSF_vec", &glob.mu_IDSF);


  cutFlow = new TH1D("cutflow", "cutflow", 20, -0.5, 19.5);
  cutFlow_weighted = new TH1D("cutflow_weighted", "cutflow_weighted", 20, -0.5, 19.5);
  nLepton = new TH1D("nLepton", "", 10, -0.5, 9.5);
  elecSelection = new TH1D("electronSelection", "electronSelection", 20, -0.5, 19.5);
  muonSelection = new TH1D("muonSelection", "muonSelection", 20, -0.5, 19.5);
  trkPixel = new TH1D("trkPixelHits", "trkPixelHits", 11, -0.5, 10.5);
  trkSCT = new TH1D("trkSCTHits", "trkSCTHits", 15, -0.5, 14.5);
  trkHoles = new TH1D("trkHoles", "trkHoles", 15, -0.5, 14.5);
  trkpt = new TH1D("trkpt", "trkpt", 100, 0., 50.);
  trketa = new TH1D("trketa", "trketa", 30, -3., 3.);
  trkphi = new TH1D("trkphi", "trkphi", 42, -3.2987, 3.2987);
  trkptPix1SCT4 = new TH1D("trkpt_1pix4sct", "trkpt_1pix4sct", 100, 0., 50.);
  trketaPix1SCT4 = new TH1D("trketa_1pix4sct", "trketa_1pix4sct", 30, -3., 3.);
  trkphiPix1SCT4 = new TH1D("trkphi_1pix4sct", "trkphi_1pix4sct", 42, -3.2987, 3.2987);
  trkpt_badQual = new TH1D("trkpt_badQual", "trkpt_badQual", 100, 0., 50.);
  trketa_badQual = new TH1D("trketa_badQual", "trketa_badQual", 30, -3., 3.);
  trkphi_badQual = new TH1D("trkphi_badQual", "trkphi_badQual", 42, -3.2987, 3.2987);
  trk_dz0BL2PV = new TH1D("trk_dz0BL2PV", "trk_dz0BL2PV", 40, -10., 10.);
  lep_dz0BL2PV = new TH1D("lep_dz0BL2PV", "lep_dz0BL2PV", 40, -10., 10.);

  nElOverlap = new TH1D("el_overlap", "", 3,0,3);


  sumWeights = new TH1D("sumWeights", "sumWeights", 10, 0., 10.);
}

/*
* Destructor
**/

Histo::~Histo() { }



// write histograms  
void Histo::finalize() { 

    // Fill the total sum of weights in histogram 
    sumWeights->SetBinContent(1, glob.unWeightedEvent);
    sumWeights->SetBinContent(2, glob.sumEventWeights_tot);
    sumWeights->SetBinContent(3, glob.sumEventWeights_tot_vtxpos);
    //sumWeights->SetBinContent(3, glob.sumEventWeights_tot, glob.sumEventWeights_tot_vtxpos);


 //     cout << "Print the numbers of events after the selection cuts (\"debug\" histogram)" << endl;
     // debug->Print();
  /*   for (Int_t j=1; j<debug->GetNbinsX(); j++) {
      cout << " ->Cut Nr=" << j << " accepted events=" << debug->GetBinContent(j+1) << endl;
     }
*/

    cout << "\n\n-- Write output file=" << ffile << endl;
    cout << "\n\n";
    RootFile->Write();
    RootFile->Print();
    RootFile->Close();
}

