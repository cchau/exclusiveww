#include "LParticle.h"


LParticle::LParticle() {
  m_charge =0;
  momentum  = TLorentzVector(0.,0.,0.,0.);
  parameters.clear();
}


LParticle::~LParticle() {

}

LParticle::LParticle(LParticle* part){
  m_charge    = part->GetCharge();
  momentum    = part->GetP();
  parameters  = part->GetParameters();
}



LParticle::LParticle(Int_t charge) {
  m_charge = charge;
  momentum  = TLorentzVector(0.,0.,0.,0.);
  parameters.clear();
}


LParticle::LParticle(Double_t px, Double_t py, Double_t pz, Double_t e, Int_t charge) {
  m_charge = charge;
  momentum = TLorentzVector(px,py,pz,e);
  parameters.clear();
}
