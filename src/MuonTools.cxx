
#include "MuonTools.h"

MuonTools::MuonTools():
  fDebug     (false)
{}
  
MuonTools::~MuonTools(){}

void MuonTools::initialize()
{
	//---Muon Smearing (Momentum Corrections)--------
	mcp_smear = new SmearingClass("Data12","staco","pT","Rel17.2Repro","lib_ext/MuonMomentumCorrections/share/");
//	mcp_smear->RestrictCurvatureCorrections(2.5);
	mcp_smear->UseImprovedCombine();

	//---Muon Scale Factors----
  Analysis::AnalysisMuonConfigurableScaleFactors::Configuration config=Analysis::AnalysisMuonConfigurableScaleFactors::AverageOverPeriods;
  muonSF = new Analysis::AnalysisMuonConfigurableScaleFactors("lib_ext/MuonEfficiencyCorrections/share/", "STACO_CB_2012_SF.txt.gz", "MeV", config); 
	
	muonSF->Initialise();
}

//----sort vectors in descending order-----
void MuonTools::tlvSort(vector<TLorentzVector> vec) {
    vector<TLorentzVector>::iterator i, j;
    for (i = vec.begin(); i != vec.end(); ++i) {
				TLorentzVector i_tmp=*i; 
        for (j = vec.begin(); j < i; ++j) {
						TLorentzVector j_tmp=*j;
            if (i_tmp.Pt() > j_tmp.Pt()) {
                std::iter_swap(i, j);
            }
        }
    }
}

// Sort 1st vector in descending order of pT and, at the same time, 
// swap the elements of the 2nd vector to keep the one-to-one map
void MuonTools::tlvSortWithIndex(std::vector<TLorentzVector> &vec, std::vector<int> &index) {
    unsigned int vec_n = (unsigned int)vec.size();
    for (unsigned int i = 0; i < vec_n; ++i) {
        for (int j = 0; j < i; ++j) {
            if ((vec.at(i)).Pt() > (vec.at(j)).Pt()) {
                // get addresses
                TLorentzVector vec_tmp = vec.at(i);
                int index_tmp = index.at(i);
                // swap addresses
                vec.at(i) = vec.at(j);
                vec.at(j) = vec_tmp;
                index.at(i) = index.at(j);
                index.at(j) = index_tmp;
            }
        }
    }
}

//-----IDSF for muons-------------------------------------
std::pair<double,double> MuonTools::GetIDSF(TLorentzVector &muons_tlv, int muon_charge, int randomRunNumber){
   double SF = 1.; double Error = 0.;
   SF = muonSF->scaleFactor(muon_charge, muons_tlv);
   Error = muonSF->scaleFactorUncertainty(muon_charge, muons_tlv);

   return std::pair<double, double> (SF, Error);
}


