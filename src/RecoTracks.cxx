/*
 * RecoTracks.cxx
 *
 *
 *
 *
 */

#include "Ana.h"
#include "Global.h"
#include "Histo.h"
#include "SystemOfUnits.h"
#include <iostream>
//#include <cstdlib>

#include<TROOT.h>
#include<TMath.h>
//#include<TClonesArray.h>
#include<TH1D.h>
#include<TH2F.h>
#include "LParticle.h"

#include<iostream>
#include<vector>
using namespace std;

extern Histo h;
extern Global glob;
const double kPI = TMath::Pi();

int Ana::RecoTracks(Long64_t entry) {

    //std::cout << "INFO  Processing track " << std::endl;

    // Empty reco track containers
    glob.recoMatchedTrackObjects.clear();
    glob.recoUnmatchedTrackObjects.clear();

    //-----------------------------------------------------------
    // Get LParticle for the appropriate channel
    //-----------------------------------------------------------
    std::vector<Double32_t> parameterLep1;  // Leading lepton
    std::vector<Double32_t> parameterLep2;  // Second leading lepton
    if (glob.channel==1) {
        parameterLep1 = (glob.recoElectronObjects.at(0)).GetParameters();
        parameterLep2 = (glob.recoElectronObjects.at(1)).GetParameters();
    } else if (glob.channel==2) {
        parameterLep1 = (glob.recoMuonObjects.at(0)).GetParameters();
        parameterLep2 = (glob.recoMuonObjects.at(1)).GetParameters();
    } else if (glob.channel==3) {
        parameterLep1 = (glob.recoElectronObjects.at(0)).GetParameters();
        parameterLep2 = (glob.recoMuonObjects.at(0)).GetParameters();
    } else if (glob.channel==4) {
        parameterLep1 = (glob.recoMuonObjects.at(0)).GetParameters();
        parameterLep2 = (glob.recoElectronObjects.at(0)).GetParameters();
    } else {
        std::cout << "Warning: unknown channel. Please verify, events should be ee (1), mumu (2) or emu/mue (3 or 4) channel." << std::endl;
    }

    //-----------------------------------------------------------
    // Get lepton track parameters
    //-----------------------------------------------------------
    double lepTrackz0pv1 = -999.9;
    double lepTrackz0pv2 = -999.9;
    double lepTrackz0beam1 = -999.9;
    double lepTrackz0beam2 = -999.9;
    double lepTrackPt1 = -9.9;
    double lepTrackPt2 = -9.9;
    double lepTrackEta1 = -99.9;
    double lepTrackEta2 = -99.9;
    double lepTrackPhi1 = -99.9;
    double lepTrackPhi2 = -99.9;
    double lepM1 = -9.9;
    double lepM2 = -9.9;
    if (TMath::Abs(glob.lepPDGID1) == 11) {
        lepTrackz0beam1 = parameterLep1.at(EL_trackz0beam);
        lepTrackz0pv1 = parameterLep1.at(EL_trackz0pv);
        lepTrackPt1 = parameterLep1.at(EL_trackpt);
        lepTrackEta1 = parameterLep1.at(EL_tracketa);
        lepTrackPhi1 = parameterLep1.at(EL_trackphi);
        lepM1 = parameterLep1.at(EL_m);
    } else if (TMath::Abs(glob.lepPDGID1) == 13) {
        lepTrackz0beam1 = parameterLep1.at(MU_trackz0beam);
        lepTrackz0pv1 = parameterLep1.at(MU_trackz0pv);
        lepTrackPt1 = TMath::Abs(TMath::Sin(parameterLep1.at(MU_id_theta))  / parameterLep1.at(MU_id_qoverp));
        lepTrackEta1 = -1. * TMath::Log(TMath::Tan(parameterLep1.at(MU_id_theta) / 2.));
        lepTrackPhi1 = parameterLep1.at(MU_id_phi);
        lepM1 = parameterLep1.at(MU_m);
    } else {
        std::cout << "Warning: Leading lepton pdg id should be 11 or 13, but is " << glob.lepPDGID1 << std::endl;
    }
    
    if (TMath::Abs(glob.lepPDGID2) == 11) {
        lepTrackz0beam2 = parameterLep2.at(EL_trackz0beam);
        lepTrackz0pv2 = parameterLep2.at(EL_trackz0pv);
        lepTrackPt2 = parameterLep2.at(EL_trackpt);
        lepTrackEta2 = parameterLep2.at(EL_tracketa);
        lepTrackPhi2 = parameterLep2.at(EL_trackphi);
        lepM2 = parameterLep2.at(EL_m);
    } else if (TMath::Abs(glob.lepPDGID2) == 13) {
        lepTrackz0beam2 = parameterLep2.at(MU_trackz0beam);
        lepTrackz0pv2 = parameterLep2.at(MU_trackz0pv);
        lepTrackPt2 = TMath::Abs(TMath::Sin(parameterLep2.at(MU_id_theta))  / parameterLep2.at(MU_id_qoverp));
        lepTrackEta2 = -1. * TMath::Log(TMath::Tan(parameterLep2.at(MU_id_theta) / 2.));
        lepTrackPhi2 = parameterLep2.at(MU_id_phi);
        lepM2 = parameterLep2.at(MU_m);
    } else {
        std::cout << "Warning: Second leading lepton pdg id should be 11 or 13, but is " << glob.lepPDGID2 << std::endl;
    }

    //-----------------------------------------------------------
    // dilepton system: distance and average z0
    //-----------------------------------------------------------
    double z0_average = (lepTrackz0beam1 + lepTrackz0beam2) / 2.;
    glob.dilepAverageZ0 = z0_average;
    glob.dilepdz0 = (lepTrackz0beam1 - lepTrackz0beam2);

    //-----------------------------------------------------------
    // Find the closest vertex reconstructed with
    // the ATLAS algorithm using trackz0beam
    //-----------------------------------------------------------
    glob.vtx_index = -1;
    glob.dvtx = -99.9;
    glob.dvtxPV = -99.9;
    glob.vtx_nTracks = -1;
    glob.vtx_nTracksPV = -1;
    int ind_vtx = -1;    
    double dvtx = -9999.9;
    // impose a maximum of 1mm between the two lepton z0
    if (TMath::Abs(lepTrackz0beam1-lepTrackz0beam2) < 1.) {
        //dvtx = -9999.9;
        for (int i = 0; i < vxp_n; ++i) {
            if (TMath::Abs(vxp_z->at(i) - z0_average) < TMath::Abs(dvtx)) {
                ind_vtx = i;
                dvtx = vxp_z->at(i) - z0_average;
            }
        }
        glob.vtx_index = ind_vtx;
        glob.dvtx = dvtx;
        if (ind_vtx > -1) {
            glob.dvtxPV = vxp_z->at(0) - z0_average;
            glob.vtx_nTracks = vxp_nTracks->at(ind_vtx);
            glob.vtx_nTracksPV = vxp_nTracks->at(0);
        }
    }

    //-----------------------------------------------------------
    // Find the closest vertex reconstructed with
    // the ATLAS algorithm using trackz0pv
    //-----------------------------------------------------------
    glob.vtxpv_index = -1;
    glob.dvtxpv = -99.9;
    glob.dvtxpvPV = -99.9;
    glob.vtxpv_nTracks = -1;
    glob.vtxpv_nTracksPV = -1;
    ind_vtx = -1;    
    dvtx = -9999.9;
    double z0pv_average = (lepTrackz0pv1 + lepTrackz0pv2) / 2.;
    // impose a maximum of 1mm between the two lepton z0
    if (TMath::Abs(lepTrackz0pv1-lepTrackz0pv2) < 1.) {
        //dvtx = -9999.9;
        for (int i = 0; i < vxp_n; ++i) {
            if (TMath::Abs(vxp_z->at(i) - vxp_z->at(0) - z0pv_average) < TMath::Abs(dvtx)) {
                ind_vtx = i;
                dvtx = vxp_z->at(i) - vxp_z->at(0) - z0pv_average;
            }
        }
        glob.vtxpv_index = ind_vtx;
        glob.dvtxpv = dvtx;
        if (ind_vtx > -1) {
            glob.dvtxpvPV = vxp_z->at(0) - z0pv_average;
            glob.vtxpv_nTracks = vxp_nTracks->at(ind_vtx);
            glob.vtxpv_nTracksPV = vxp_nTracks->at(0);
        }
    }

    //-----------------------------------------------------------
    // Matching tracks to leptons
    //-----------------------------------------------------------
    const double DR_CUT = 0.01;
    const double DZ0_CUT = 1.0;
    // TLorentzVector for leading and sub-leading leptons
    TLorentzVector lep1_tlv, lep2_tlv;
    lep1_tlv.SetPtEtaPhiM( lepTrackPt1, lepTrackEta1, lepTrackPhi1, lepM1);
    lep2_tlv.SetPtEtaPhiM( lepTrackPt2, lepTrackEta2, lepTrackPhi2, lepM2);

    //std::cout << "INFO  Looping over trk_ " << std::endl;
    //------------------------------------------------------
    // Loop over trk container to characterize tracks
    // Four categories: 
    //     - track matched to leading lepton
    //     - track matched to subleading lepton
    //     - track matched to both lepton
    //     - track doesn't match to any lepton
    //------------------------------------------------------
    int countMatchTrk1 = 0;
    int countMatchTrk2 = 0;
    int countUnmatchTrk = 0;
    glob.nTrkUnmatched = 0; glob.nTrkUnmatched_100mm = 0; glob.nTrkUnmatched_125mm = 0; glob.nTrkUnmatched_150mm = 0; glob.nTrkUnmatched_175mm = 0;
    glob.nTrkUnmatched_200mm = 0; glob.nTrkUnmatched_225mm = 0; glob.nTrkUnmatched_250mm = 0; glob.nTrkUnmatched_275mm = 0; glob.nTrkUnmatched_300mm = 0;

    double evtWeight = glob.mcEventWeight;
    for (unsigned int iTrk = 0; iTrk < (unsigned int)trk_n; ++iTrk) {

        //std::cout << "INFO  Porcessing trk "<< iTrk << std::endl;
        // reset flags
        bool isMatchedToLep1 = false;
        bool isMatchedToLep2 = false;
        
        //---------track requirement--------------
        int trknPixHits = trk_nPixHits->at(iTrk);
        int trknSCTHits = trk_nSCTHits->at(iTrk);
        int trknPixHoles = trk_nPixHoles->at(iTrk);
        int trknSCTHoles = trk_nSCTHoles->at(iTrk);

        // Track quality
        int trkQuality = 0;  // 1 if good track, anything else otherwise
        if ((trknPixHits > 0) && (trknSCTHits > 3)) {
            trkQuality = 1;
            glob.nTrkPix1SCT4 += 1;
        }

        // track pt, 0-50 GeV
        h.trkpt->Fill(trk_pt->at(iTrk)/1000., evtWeight);
        h.trketa->Fill(trk_eta->at(iTrk), evtWeight);
        h.trkphi->Fill(trk_phi_wrtBL->at(iTrk), evtWeight);
        if(trkQuality==1) {
            h.trkptPix1SCT4->Fill(trk_pt->at(iTrk)/1000., evtWeight);
            h.trketaPix1SCT4->Fill(trk_eta->at(iTrk), evtWeight);
            h.trkphiPix1SCT4->Fill(trk_phi_wrtBL->at(iTrk), evtWeight);
        } else {
            h.trkpt_badQual->Fill(trk_pt->at(iTrk)/1000., evtWeight);
            h.trketa_badQual->Fill(trk_eta->at(iTrk), evtWeight);
            h.trkphi_badQual->Fill(trk_phi_wrtBL->at(iTrk), evtWeight);
        }

        //std::cout << "INFO  check pixel hits " << std::endl;
        // pixel hit requirement
        bool flag = true;
        int counter = 0;
        float trkbin = 0;
        if (trknSCTHits > 3) { h.trkPixel->Fill(0., evtWeight); }
        while (flag) {
            if ((trknSCTHits > 3) && (trknPixHits > counter)) { 
                trkbin = 1. + counter * 2.;
                if (trkbin > 9.) trkbin = 9.;
                h.trkPixel->Fill(trkbin, evtWeight); 
                if ((trk_expectBLayerHit->at(iTrk) == 1) && (trk_nBLHits->at(iTrk) > 0)) {
                    h.trkPixel->Fill( (trkbin+1.), evtWeight);
                } //else {
                //    ++counter;
                //    continue;
                //}
            } else {
                //flag = false;
                break;
            }
            ++counter;
        }

        //std::cout << "INFO  check sct hits " << std::endl;
        // SCT hit requirement
        flag = true;
        counter = 0;
        trkbin = 0;
        if (trknPixHits > 0) { h.trkSCT->Fill(0., evtWeight); }
        while (flag) {
            if ((trknPixHits > 0) && (trknSCTHits > counter)) { 
                trkbin = counter+1.;
                if (trkbin > 14.) trkbin = 14.;
                h.trkSCT->Fill(trkbin, evtWeight); 
            } else {
                //flag = false;
                break;
            }
            ++counter;
        }
        
        //std::cout << "INFO  check ID holes " << std::endl;
        // ID hole requirement
        flag = true;
        counter = 0;
        trkbin = 0;
        if ((trknPixHoles+trknSCTHoles) == 0) {h.trkHoles->Fill(trkbin, evtWeight);}
        while (flag) {
            if ((trknPixHoles+trknSCTHoles) > counter) { 
                trkbin = counter + 1.;
                if (trkbin > 14.) trkbin = 14.;
                h.trkHoles->Fill(trkbin, evtWeight); 
            } else {
                //flag = false;
                break;
            }
            ++counter;
        }
        
        // Verify offset between z0_wrtBL and z0_wrtPV
        double dz0_bl2pv = trk_z0_wrtBL->at(iTrk) - (trk_z0_wrtPV->at(iTrk) + vxp_z->at(0));
        h.trk_dz0BL2PV->Fill(dz0_bl2pv, evtWeight);

        // Skip tracks with bad quality
        if (trkQuality != 1) continue;

        // Track TLorentzVector
        TLorentzVector trk_tlv;
        trk_tlv.SetPtEtaPhiM( trk_pt->at(iTrk), trk_eta->at(iTrk), trk_phi_wrtBL->at(iTrk), 0.);

        // try to match track to leading lepton
        double dz01 = TMath::Abs(lepTrackz0beam1 - trk_z0_wrtBL->at(iTrk));
        double dz02 = TMath::Abs(lepTrackz0beam2 - trk_z0_wrtBL->at(iTrk));
        double dR1 = lep1_tlv.DeltaR(trk_tlv);
        double dR2 = lep2_tlv.DeltaR(trk_tlv);
        if ((trkQuality==1) && (trk_pt->at(iTrk)>2000.) && (dz01 < DZ0_CUT) && (dR1 < DR_CUT)) {
            isMatchedToLep1 = true;
        }
        if ((trkQuality==1) && (trk_pt->at(iTrk)>2000.) && (dz02 < DZ0_CUT) && (dR2 < DR_CUT)) {
            isMatchedToLep2 = true;
        }
        
        // For case where a track is matched to both leptons
        if (isMatchedToLep1 && isMatchedToLep2) {
            if(dz01 < dz02) {
                isMatchedToLep2 = false;
            } else { 
                isMatchedToLep1 = false;
            }
        }


        //std::cout << "INFO  check matched trk " << std::endl;
        //------------------------------------------
        // Save matched and unmatched tracks in their 
        // respective container
        //------------------------------------------

        if (isMatchedToLep1 || isMatchedToLep2) {  // Track is matched to the lepton

            int matchInd = isMatchedToLep1? 1: 2;

            LParticle recoMatchTrk_lp;
            recoMatchTrk_lp.SetCharge(0.);
            recoMatchTrk_lp.SetP(trk_tlv);

            recoMatchTrk_lp.SetParameter( matchInd );
            recoMatchTrk_lp.SetParameter( (trk_z0_wrtBL->at(iTrk) - z0_average) );
            recoMatchTrk_lp.SetParameter( trk_pt->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_eta->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_phi_wrtBL->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_d0_wrtBL->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_z0_wrtBL->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_d0_wrtPV->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_z0_wrtPV->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_phi_wrtPV->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_theta_wrtPV->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_qoverp_wrtPV->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_nPixHits->at(iTrk) );
            recoMatchTrk_lp.SetParameter( trk_nSCTHits->at(iTrk) );
            recoMatchTrk_lp.SetParameter( (trk_nPixelDeadSensors->at(iTrk) + trk_nSCTDeadSensors->at(iTrk)) );
            recoMatchTrk_lp.SetParameter( (trk_nPixHoles->at(iTrk) + trk_nSCTHoles->at(iTrk)) );

            glob.recoMatchedTrackObjects.push_back(recoMatchTrk_lp);
            if (isMatchedToLep1) {++countMatchTrk1;}
            else {++countMatchTrk2;}

        } else {  // Track is not matched

            LParticle recoUnmatchTrk_lp;
            recoUnmatchTrk_lp.SetCharge(0.);
            recoUnmatchTrk_lp.SetP(trk_tlv);

            recoUnmatchTrk_lp.SetParameter( 0. );
            recoUnmatchTrk_lp.SetParameter( (trk_z0_wrtBL->at(iTrk) - z0_average) );
            recoUnmatchTrk_lp.SetParameter( trk_pt->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_eta->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_phi_wrtBL->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_d0_wrtBL->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_z0_wrtBL->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_d0_wrtPV->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_z0_wrtPV->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_phi_wrtPV->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_theta_wrtPV->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_qoverp_wrtPV->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_nPixHits->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( trk_nSCTHits->at(iTrk) );
            recoUnmatchTrk_lp.SetParameter( (trk_nPixelDeadSensors->at(iTrk) + trk_nSCTDeadSensors->at(iTrk)) );
            recoUnmatchTrk_lp.SetParameter( (trk_nPixHoles->at(iTrk) + trk_nSCTHoles->at(iTrk)) );

            glob.recoUnmatchedTrackObjects.push_back(recoUnmatchTrk_lp);
            ++countUnmatchTrk;

            // Count tracks within some window
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 3.0) continue;
            glob.nTrkUnmatched_300mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 2.75) continue;
            glob.nTrkUnmatched_275mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 2.5) continue;
            glob.nTrkUnmatched_250mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 2.25) continue;
            glob.nTrkUnmatched_225mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 2.0) continue;
            glob.nTrkUnmatched_200mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 1.75) continue;
            glob.nTrkUnmatched_175mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 1.5) continue;
            glob.nTrkUnmatched_150mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 1.25) continue;
            glob.nTrkUnmatched_125mm += 1;
            //
            if (TMath::Abs(trk_z0_wrtBL->at(iTrk) - z0_average) > 1.0) continue;
            glob.nTrkUnmatched_100mm += 1;
        }

    } //--- End loop over tracks

    glob.lepNMatchedTrk1 = countMatchTrk1;
    glob.lepNMatchedTrk2 = countMatchTrk2;
    glob.nTrkUnmatched = countUnmatchTrk;


    //std::cout << "INFO  Track characteristic" << std::endl;
    //-----------------------------------------------------------
    // Characteristic of tracks match to selected leptons
    //-----------------------------------------------------------
    if ((glob.lepNMatchedTrk1 + glob.lepNMatchedTrk2) > 0) {

        int nMatchedTrk = (int)(glob.recoMatchedTrackObjects.size());
        if (nMatchedTrk != (glob.lepNMatchedTrk1+glob.lepNMatchedTrk2))
            std::cout << "Error: Something went wrong while counting the number of tracks matched to the selected leptons" << std::endl;

        // Loop over matched track objects to separate lepton1 from lepton2 tracks
        std::vector<LParticle> trkLep1;
        std::vector<LParticle> trkLep2;
        for (int i = 0; i < nMatchedTrk; ++i) {
            // Get parameters from track LParticle
            std::vector<Double32_t> trkPar = (glob.recoMatchedTrackObjects.at(i)).GetParameters();

            if ((trkPar.at(0) > 0.) && (trkPar.at(0) < 1.5)) { // 0th element is zero, matched to leading lepton
                trkLep1.push_back( glob.recoMatchedTrackObjects.at(i) );
            } else if(trkPar.at(0) < 2.5) { // 0th element is two, matched to subleading lepton
                trkLep2.push_back( glob.recoMatchedTrackObjects.at(i) );
            } else {
                std::cout << "Error: Expected some tracks matched to selected leptons, but found none" << std::endl;
            }
        }

        // Process tracks matched to lepton 1
        std::vector<double> trkLep1_res;
        int nTrkLep1 = (int)trkLep1.size();
        if (nTrkLep1 > 0) {
            ProcessMatchedTracks(trkLep1, lep1_tlv, trkLep1_res);
            glob.matchedLep1_width = trkLep1_res.at(0); 
            glob.matchedLep1_coreRes = trkLep1_res.at(1); 
            glob.matchedLep1_softRes = trkLep1_res.at(2); 
            glob.matchedLep1_dr = trkLep1_res.at(3);
        }
        // Process tracks matched to lepton 2
        std::vector<double> trkLep2_res;
        int nTrkLep2 = (int)trkLep2.size();
        if (nTrkLep2 > 0) {
            ProcessMatchedTracks(trkLep2, lep2_tlv, trkLep2_res);
            glob.matchedLep2_width = trkLep2_res.at(0); 
            glob.matchedLep2_coreRes = trkLep2_res.at(1); 
            glob.matchedLep2_softRes = trkLep2_res.at(2); 
            glob.matchedLep2_dr = trkLep2_res.at(3);
        }
    } else {
        // tracks of lepton1
        glob.matchedLep1_width=0.; glob.matchedLep1_coreRes=-1.; glob.matchedLep1_softRes=-1.; glob.matchedLep1_dr=-1.0;
#include "egammaAnalysisUtils/EnergyRescalerUpgrade.h"
        // tracks of lepton2
        glob.matchedLep2_width=0.; glob.matchedLep2_coreRes=-1.; glob.matchedLep2_softRes=-1.; glob.matchedLep2_dr=-1.0;
    }

    //std::cout << "INFO  done track characteristic" << std::endl;
    return 0;
}
