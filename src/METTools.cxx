#include "METTools.h"

METTools::METTools():
  fDebug     (false)
{}
  
METTools::~METTools(){}

void METTools::initialize()
{

	printf("**** Initializing MET ****");

	m_util = new METUtility;

	m_util->setVerbosity(false);
	m_util->configEgammaJetFix(true, false, false, 0.3);
	m_util->setIsMuid(false);
	m_util->setDoMuEloss(true);
	m_util->setJetPUcode(MissingETTags::DEFAULT);

}

void METTools::MET_Recalculation(
																vector<vector<float>* > &electrons,
																vector<vector<vector<float> >* > &electrons_extra,
																vector<vector<unsigned int> > *electron_sw,
																vector<vector<float>* > &muons,
																vector<vector<vector<float> >* > &muons_extra,
																vector<vector<unsigned int> > *muon_sw,
																vector<vector<float>* > &jets,
																vector<vector<vector<float> >* > &jets_extra,
																vector<vector<unsigned int> > *jet_sw,
																vector<float> met,
                              	float &met_et, float &met_x, float &met_y 
																)
{
	m_util->reset();

	if(electrons.size()!=3 || electrons_extra.size()!=3 )printf("Error:: Please fill electrons correctly\n");
	if(muons.size()!= 8 || muons_extra.size()!=3 )printf("Error:: Please fill muons correctly\n");
	if(met.size()!=9)printf("Error:: Please fill MET correctly\n");

	m_util->setElectronParameters(electrons.at(0), electrons.at(1), electrons.at(2), electrons_extra.at(0), electrons_extra.at(1), electrons_extra.at(2), electron_sw);
  m_util->setMuonParameters(muons.at(0), muons.at(1), muons.at(2), muons_extra.at(0), muons_extra.at(1), muons_extra.at(2), muon_sw);
  m_util->setExtraMuonParameters(muons.at(3), muons.at(4), muons.at(5), muons.at(6));

	m_util->setMuonEloss(muons.at(7));

  m_util->setJetParameters( jets.at(0), jets.at(1), jets.at(2), jets.at(3), jets_extra.at(0), jets_extra.at(1), jets_extra.at(2), jet_sw);

  m_util->setMETTerm(METUtil::RefTau, met.at(0) * cos(met.at(1)),  met.at(0) * sin(met.at(1)), met.at(2));
  m_util->setMETTerm(METUtil::RefGamma, met.at(3) * cos(met.at(4)),  met.at(3) * sin(met.at(4)), met.at(5));
  m_util->setMETTerm(METUtil::SoftTerms, met.at(6)*cos(met.at(7)), met.at(6)*sin(met.at(7)), met.at(8));


	//get new values for each term
  METUtility::METObject RefEle_util = m_util->getMissingET(METUtil::RefEle);
  METUtility::METObject RefJet_util = m_util->getMissingET(METUtil::RefJet);
  METUtility::METObject MuonTotal_util = m_util->getMissingET(METUtil::MuonTotal);

  METUtility::METObject SoftTerms_util = m_util->getMissingET(METUtil::SoftTerms);
  METUtility::METObject RefFinal_util = m_util->getMissingET(METUtil::RefFinal);

  //////////nominal MET 
  met_x = RefFinal_util.etx(); 
  met_y = RefFinal_util.ety(); 
  met_et = sqrt( met_x * met_x +  met_y * met_y ); 
  //printf("MET: Etx=%8.2f, Ety=%8.2f, Et=%8.2f,\n",  met_x, met_y, met_et); 
 
  return; 
 
}

