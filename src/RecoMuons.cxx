/*
 * RecoMuons.cxx
 *
 *
 *
 *
 */

#include "Ana.h"
#include "Global.h"
#include "Histo.h"
#include "SystemOfUnits.h"
#include <iostream>
//#include <cstdlib>

#include<TROOT.h>
#include<TMath.h>
//#include<TClonesArray.h>
#include<TH1D.h>
#include<TH2F.h>
#include "LParticle.h"

#include<iostream>
#include<vector>
using namespace std;

extern Histo h;
extern Global glob;
const double kPI = TMath::Pi();

int Ana::RecoMuons(Long64_t entry) {

    // Empty reco muon container
    glob.recoMuonObjects.clear();
    glob.truthMuonObjects.clear();

    //------------------------------------------------------
    // Loop over number of muons
    //------------------------------------------------------
    for (unsigned int iMu = 0; iMu < (unsigned int)mu_staco_n; ++iMu) {

        // Skip muon is pT is too low
        if (mu_staco_pt->at(iMu) < 1000.) continue;

        int npixHits = mu_staco_nPixHits->at(iMu);
        int npixHoles = mu_staco_nPixHoles->at(iMu);
        int npixDeadSensors = mu_staco_nPixelDeadSensors->at(iMu);
        int nsctHits = mu_staco_nSCTHits->at(iMu);
        int nsctHoles = mu_staco_nSCTHoles->at(iMu);
        int nsctDeadSensors = mu_staco_nSCTDeadSensors->at(iMu);
        int nTRTHits = mu_staco_nTRTHits->at(iMu);
        int nTRTOutliers = mu_staco_nTRTOutliers->at(iMu);
        double muCharge = mu_staco_charge->at(iMu);
        double muEta = mu_staco_eta->at(iMu);
        double muPt = mu_staco_pt->at(iMu);
        int muAuthor = mu_staco_author->at(iMu);

        float bin = 0.;
        h.muonSelection->Fill(bin);
        ++bin;
        //---- Applying muon filters 
        if (muAuthor != 6) continue;
        h.muonSelection->Fill(bin);
        ++bin;
        if (!mu_staco_isCombinedMuon->at(iMu)) continue; //isCombined
        h.muonSelection->Fill(bin);
        ++bin;
        //if (!glob.mu_isTight) continue; //tight
        if (TMath::Abs(muEta) > 2.4) continue;
        h.muonSelection->Fill(bin);
        ++bin;
        if ((npixHits + npixDeadSensors + nsctHits + nsctDeadSensors) < 6) continue;
        h.muonSelection->Fill(bin);
        ++bin;
        if ((npixHits + npixDeadSensors) < 1) continue;
        h.muonSelection->Fill(bin);
        ++bin;
        if ((nsctHits + nsctDeadSensors) < 5) continue;
        h.muonSelection->Fill(bin);
        ++bin;
        if ((npixHoles + nsctHoles) > 2) continue;
        h.muonSelection->Fill(bin);
        ++bin;
        if ((TMath::Abs(muEta) > 0.1) && (TMath::Abs(muEta) < 1.9)) {
            if ((nTRTHits+nTRTOutliers) <= 5) continue;
            if (nTRTOutliers/(nTRTHits+nTRTOutliers) > 0.9) continue;
        }
        h.muonSelection->Fill(bin);
        ++bin;

        //---- Correct momentum
        double pTCB_smeared = muPt; double pTMS_smeared=-1.; double pTID_smeared=-1.;
        //
        //double pTCB_IDUP(-1.), pTMS_IDUP(-1.), pTID_IDUP(-1.);
        //double pTCB_IDLOW(-1.), pTMS_IDLOW(-1.), pTID_IDLOW(-1.);
        ////
        //double pTCB_MSUP(-1.), pTMS_MSUP(-1.), pTID_MSUP(-1.);
        //double pTCB_MSLOW(-1.), pTMS_MSLOW(-1.), pTID_MSLOW(-1.);
        ////
        //double pTCB_SCALEUP(-1.), pTMS_SCALEUP(-1.), pTID_SCALEUP(-1.);
        //double pTCB_SCALELOW(-1.), pTMS_SCALELOW(-1.), pTID_SCALELOW(-1.);

        if (isMC) {
            // Retrieve PtCB, PtMS and PtID from containers/ntuples
            //float ptms=(TMath::Sin(mu_staco_ms_theta->at(iMu))*muCharge ) / (mu_staco_ms_qoverp->at(iMu)); //WHAT IS mu_ms?
            //float ptid=(TMath::Sin(mu_staco_id_theta->at(iMu))*muCharge ) / (mu_staco_id_qoverp->at(iMu)); 
            double ptms=fabs(TMath::Sin(mu_staco_me_theta->at(iMu)) / mu_staco_me_qoverp->at(iMu)); //WHAT IS mu_ms?
            double ptid=fabs(TMath::Sin(mu_staco_id_theta->at(iMu)) / mu_staco_id_qoverp->at(iMu)); 
            //float eta_id = -1. * log(tan(mu_staco_id_theta->at(iMu)/2.));
            float eta_id = muEta;

            // Use the MC event number to set seed so that the random numbers are reproducible by different analyzers
            glob.mcp_smear->SetSeed(EventNumber,iMu);
            //glob.mcp_smear->Event(ptms,ptid,muPt,muEta,muCharge);
            glob.mcp_smear->Event(ptms,ptid,muPt,eta_id,muCharge);
 
            // Get Smeared & Scaled Pts
            pTCB_smeared = glob.mcp_smear->pTCB();
            pTMS_smeared = glob.mcp_smear->pTMS();
            pTID_smeared = glob.mcp_smear->pTID();
            // save pts for systematic variation
            //pTCB_IDUP = glob.mcp_smear->pTCB();  pTCB_IDLOW = glob.mcp_smear->pTCB();
            //pTMS_IDUP = glob.mcp_smear->pTMS();  pTMS_IDLOW = glob.mcp_smear->pTMS();
            //pTID_IDUP = glob.mcp_smear->pTID();  pTID_IDLOW = glob.mcp_smear->pTID();
            ////
            //pTCB_MSUP = glob.mcp_smear->pTCB();  pTCB_MSLOW = glob.mcp_smear->pTCB();
            //pTMS_MSUP = glob.mcp_smear->pTMS();  pTMS_MSLOW = glob.mcp_smear->pTMS();
            //pTID_MSUP = glob.mcp_smear->pTID();  pTID_MSLOW = glob.mcp_smear->pTID();
            ////
            //pTCB_SCALEUP = glob.mcp_smear->pTCB();  pTCB_SCALELOW = glob.mcp_smear->pTCB();
            //pTMS_SCALEUP = glob.mcp_smear->pTMS();  pTMS_SCALELOW = glob.mcp_smear->pTMS();
            //pTID_SCALEUP = glob.mcp_smear->pTID();  pTID_SCALELOW = glob.mcp_smear->pTID();
            
            // systematic variation
            if (glob.doMuonIDResolution == 1) {
                glob.mcp_smear->PTVar(pTMS_smeared, pTID_smeared, pTCB_smeared, "IDUP");
                std::cout << "INFO  Muon systematic variation set to IDUP\n";
            } else if (glob.doMuonIDResolution == -1) {
                glob.mcp_smear->PTVar(pTMS_smeared, pTID_smeared, pTCB_smeared, "IDLOW");
                std::cout << "INFO  Muon systematic variation set to IDLOW\n";
            }
            //
            if (glob.doMuonMSResolution == 1) {
                glob.mcp_smear->PTVar(pTMS_smeared, pTID_smeared, pTCB_smeared, "MSUP");
                std::cout << "INFO  Muon systematic variation set to MSUP\n";
            } else if (glob.doMuonMSResolution == -1) {
                glob.mcp_smear->PTVar(pTMS_smeared, pTID_smeared, pTCB_smeared, "MSLOW");
                std::cout << "INFO  Muon systematic variation set to MSLOW\n";
            }
            //
            if (glob.doMuonScale == 1) {
                glob.mcp_smear->PTVar(pTMS_smeared, pTID_smeared, pTCB_smeared, "SCALEUP");
                std::cout << "INFO  Muon systematic variation set to SCALEUP\n";
            } else if (glob.doMuonScale == -1) {
                glob.mcp_smear->PTVar(pTMS_smeared, pTID_smeared, pTCB_smeared, "SCALELOW");
                std::cout << "INFO  Muon systematic variation set to SCALELOW\n";
            }

        }

        //---- Applying more muon filters 
        muPt = pTCB_smeared;
        if (TMath::IsNaN(pTCB_smeared)) {
            std::cout <<"Muon Nan smear pT, initial value is " << mu_staco_pt->at(iMu) << std::endl;
            muPt = mu_staco_pt->at(iMu);
        }
        if (muPt < 10000.) continue;
        h.muonSelection->Fill(bin);
        ++bin;

        // corrected calorimeter etcone
        double mu_etcone30_corrected = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone30->at(iMu), glob.npv_trk3, muEta, "cone30Comb");

        if (muPt <= 15000.) {
            if ((mu_etcone30_corrected / muPt) > 0.06) continue;
            h.muonSelection->Fill(bin); ++bin;
            if ((mu_staco_ptcone30->at(iMu) / muPt) > 0.06) continue;
            h.muonSelection->Fill(bin); ++bin;
        } else if (muPt <= 20000.) {
            if ((mu_etcone30_corrected / muPt) > 0.12) continue;
            h.muonSelection->Fill(bin); ++bin;
            if ((mu_staco_ptcone30->at(iMu) / muPt) > 0.08) continue;
            h.muonSelection->Fill(bin); ++bin;
        } else if (muPt <= 25000.) {
            if ((mu_etcone30_corrected / muPt) > 0.18) continue;
            h.muonSelection->Fill(bin); ++bin;
            if ((mu_staco_ptcone30->at(iMu) / muPt) > 0.12) continue;
            h.muonSelection->Fill(bin); ++bin;
        } else {
            if ((mu_etcone30_corrected / muPt) > 0.30) continue;
            h.muonSelection->Fill(bin); ++bin;
            if ((mu_staco_ptcone30->at(iMu) / muPt) > 0.12) continue;
            h.muonSelection->Fill(bin); ++bin;
        }


        // muon TLorentzvector
        TLorentzVector muon_tlv;
        muon_tlv.SetPtEtaPhiM(muPt, muEta, mu_staco_phi->at(iMu), mu_staco_m->at(iMu));

        //-----------------------------------------------------------
        // Compute Muon Identification Scale Factors in MC
        //  and isolation scale factors
        //-----------------------------------------------------------
        // identification scale factors
        double muon_IdSF = 1.;
        double muon_IdSF_statErr = 0.;
        double muon_IdSF_sysErr = 0.;
        //
        // isolatin scale factor
        double muIsoSF = 1.0;
        double muIsoSF_sys = 0.0;

        if (isMC) {
            // Identification
            muon_IdSF = glob.muonSF->scaleFactor(muCharge, muon_tlv);
            muon_IdSF_statErr = glob.muonSF->scaleFactorUncertainty(muCharge, muon_tlv);
            muon_IdSF_sysErr = glob.muonSF->scaleFactorSystematicUncertainty(muCharge, muon_tlv);

            // Isolation
            if (glob.isAFII == 1) {
                muIsoSF = glob.tool_muonIsoSF->GetIsolationSF(muPt, 15000., false, "mc12aPaperAFII");
                muIsoSF_sys = glob.tool_muonIsoSF->GetIsolationSFError(muPt, 15000., false, "mc12aPaperAFII");
            } else {
                muIsoSF = glob.tool_muonIsoSF->GetIsolationSF(muPt, 15000., false, "mc12aPaperFullSim");
                muIsoSF_sys = glob.tool_muonIsoSF->GetIsolationSFError(muPt, 15000., false, "mc12aPaperFullSim");
            }
        }

        //-----------------------------------------------------------
        // Get muon branches
        //-----------------------------------------------------------

        double mu_etcone20_corrected = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone20->at(iMu), glob.npv_trk3, muEta, "cone20Comb");
        double mu_etcone40_corrected = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone40->at(iMu), glob.npv_trk3, muEta, "cone40Comb");

        // Reco muons
        LParticle recoMu_lp;
        recoMu_lp.SetCharge(muCharge);
        recoMu_lp.SetP(muon_tlv);
        // 
        recoMu_lp.SetParameter( mu_staco_E->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_pt->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_m->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_eta->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_phi->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_charge->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_author->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_etcone20->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_etcone30->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_etcone40->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_ptcone20->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_ptcone30->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_ptcone40->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_isCombinedMuon->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_tight->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_nPixHits->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_nSCTHits->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_nPixelDeadSensors->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_nSCTDeadSensors->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_nPixHoles->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_nSCTHoles->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackd0->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackz0->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackd0pv->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackz0pv->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackd0beam->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackz0beam->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackd0pvunbiased->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackz0pvunbiased->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_tracksigd0pvunbiased->at(iMu) );
        recoMu_lp.SetParameter( TMath::Abs(TMath::Sin(mu_staco_tracktheta->at(iMu)) / mu_staco_trackqoverp->at(iMu)) );
        recoMu_lp.SetParameter( mu_staco_trackphi->at(iMu) );
        recoMu_lp.SetParameter( -1 * TMath::Log(TMath::Tan(mu_staco_tracktheta->at(iMu) / 2)) );
        recoMu_lp.SetParameter( mu_staco_tracktheta->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_trackqoverp->at(iMu) );
        recoMu_lp.SetParameter( muon_IdSF );
        recoMu_lp.SetParameter( muon_IdSF_statErr );
        recoMu_lp.SetParameter( muon_IdSF_sysErr );
        recoMu_lp.SetParameter( muIsoSF );
        recoMu_lp.SetParameter( muIsoSF_sys );
        recoMu_lp.SetParameter( muPt );
        recoMu_lp.SetParameter( mu_etcone20_corrected );
        recoMu_lp.SetParameter( mu_etcone30_corrected );
        recoMu_lp.SetParameter( mu_etcone40_corrected );
        recoMu_lp.SetParameter( mu_staco_ptcone30->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_id_theta->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_id_phi->at(iMu) );
        recoMu_lp.SetParameter( mu_staco_id_qoverp->at(iMu) );
        // Save reco muon
        glob.recoMuonObjects.push_back(recoMu_lp);

        // Truth muon
        if (isMC) {
            LParticle truthMu_lp;
            truthMu_lp.SetParameter( mu_staco_truth_E->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_pt->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_eta->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_phi->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_type->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_status->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_barcode->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_motherbarcode->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_truth_matched->at(iMu) );
            truthMu_lp.SetParameter( 0 );  // index, exist for electron
            truthMu_lp.SetParameter( 0 );  // has bremsstrahlung, exist for electron
            truthMu_lp.SetParameter( mu_staco_type->at(iMu) );
            truthMu_lp.SetParameter( mu_staco_origin->at(iMu) );
            // Save truth muon
            glob.truthMuonObjects.push_back(truthMu_lp);
        }

    } //--- End loop over muons

    return 0;
}
