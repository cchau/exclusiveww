/*
 *      Loop.cxx
 *      
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

  
#include "Ana.h"
#include "Global.h"
#include "Histo.h"

#include <TSystem.h>
#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<algorithm>

#include "TriggerTools.h"
#include "MuonTools.h"
#include "METTools.h"

extern Global glob;
extern Histo h;

MuonTools muonTools;
METTools metTools;

// compare for a vector of floats, sorting from lowest to highest
bool my_compare( float a, float b){
	return a < b; 
}
bool my_compare_vec( std::vector<double> a, std::vector<double> b){
	return a.at(0) < b.at(0); 
}

// compare function for vector of LParticles, sorting with pT
bool my_compare_LParticle( LParticle lpa, LParticle lpb){
	return (lpa.GetP()).Pt() > (lpb.GetP()).Pt(); 
}

// compare function for Tracks, sorting with dz0
bool my_compare_track( LParticle lpa, LParticle lpb){
    int ind = TRK_dz0TrkLep;
	return TMath::Abs((lpa.GetParameters()).at(ind)) < TMath::Abs((lpb.GetParameters()).at(ind)); 
}

void doOverlapRemoval(const std::vector<TLorentzVector>& rFirst,
           std::vector<TLorentzVector>& rSecond,
           float DeltaRCut)
{
  // different template types
  std::vector<TLorentzVector>::const_iterator it = rFirst.begin();
  std::vector<TLorentzVector>::iterator sec;
  while(it != rFirst.end())
  {
		TLorentzVector it_tmp=*it;
    sec = rSecond.begin();
    while(sec != rSecond.end())
    {
	TLorentzVector sec_tmp=*sec;
      if(it_tmp.DeltaR(sec_tmp) < DeltaRCut)
      {
          rSecond.erase(sec--);
      }
      ++sec;
    }
    ++it;
  }
}

/*******************************************************************
 * Event loop.
 *   Is executed for each event
 *   Apply some filters to throw away uninteresting events
 *   Produce ntuple recording events passing the filters
 *    - Events saved according to the final state: ee, mumu and emu
 *******************************************************************/
void Ana::Loop()
{

    // ensure that fChain exist
    if (fChain == 0) return;
    
    // Get number of events from the TTrees in fChain
    Long64_t nentries = fChain->GetEntriesFast();
    std::cout << "INFO  Number of entries =" << nentries << endl;
    
    // Disable all branches and then enable only the relevant ones to save time
    fChain->SetBranchStatus("*",0);  //disable all branches
    fChain->SetBranchStatus("lbn",1); 
    if(isMC)fChain->SetBranchStatus("bcid",1); 
    fChain->SetBranchStatus("larError",1); 
    fChain->SetBranchStatus("tileError",1); 
    fChain->SetBranchStatus("coreFlags",1); 
    fChain->SetBranchStatus("top_hfor_type",1); 
    fChain->SetBranchStatus("trk_*",1); 
    fChain->SetBranchStatus("Run*",1); 
    fChain->SetBranchStatus("Event*",1); 
    fChain->SetBranchStatus("averageIntPerXing",1); 
    fChain->SetBranchStatus("vxp_*",1); 
    fChain->SetBranchStatus("el_*",1);  
    fChain->SetBranchStatus("mu_*",1);  
    fChain->SetBranchStatus("MET_*",1);  
    fChain->SetBranchStatus("EF_*",1);  
    fChain->SetBranchStatus("trig_*",1);  
    fChain->SetBranchStatus("jet_AntiKt4TopoEM_*",1);  
    fChain->SetBranchStatus("jet_AntiKt4LCTopo_*",1);  

    if(isMC){
        fChain->SetBranchStatus("mc*",1); 
    } 

    // Enable lepton trigger
    glob.muon_trigger_chain = "mu24i_tight_or_mu36_tight_or_mu18_tight_mu8_EFFS";
    glob.electron_trigger_chain = "e24vhi_medium1_or_e60_medium1_or_2e12Tvh_loose1";
    glob.electronMuon_trigger_chain = "EF_e12Tvh_medium1_mu8";


    //debug file
    //ofstream fdebug;
    //fdebug.open("debug_log.txt", std::ofstream::out | std::ofstream::app);
    //fdebug << std::endl;


    //***************************************************************
    // Define and/or initialize the CP tools if not already done
    // upstream (e.g. in src/Global.cxx)
    // Most (if not to say all) of the CP corrections will be
    //                                      applied on the MC sample
    //***************************************************************
    //if(isMC){

        //------------Pileup reweighting----------------------
        glob.my_PileUpReweighting = new Root::TPileupReweighting("my_PileUpReweighting");
        glob.my_PileUpReweighting->AddConfigFile("./external/my_PileUpReweighting_mc12ab.root");
        //glob.my_PileUpReweighting->AddConfigFile("./external/mc12ab_prw_20141128.root"); // for FPMC samples only
        glob.my_PileUpReweighting->SetDataScaleFactors(1./1.09);
        //glob.my_PileUpReweighting->AddLumiCalcFile("./external/data_A-M_complete_20.7.root");
        glob.my_PileUpReweighting->AddLumiCalcFile("./external/ilumicalc_histograms_None_200842-215643.root");
        glob.my_PileUpReweighting->SetDefaultChannelByMCRunNumber(0,195847);
        glob.my_PileUpReweighting->SetDefaultChannelByMCRunNumber(1,195848);
        glob.my_PileUpReweighting->SetUnrepresentedDataAction(2);
        int isGood = glob.my_PileUpReweighting->initialize();
        std::cout << "If (0) then proper initialization of the PileUp tools: " << isGood << std::endl;
    //}
        
    //-----------Initialize LeptonTriggerSF------------
    TriggerTools triggerTools;
    triggerTools.my_leptonTriggerSF = new LeptonTriggerSF(2012, "./external", "muon_trigger_sf_2012_AtoL.p1328.root",  "./lib_ext/ElectronEfficiencyCorrection/data", "rel17p2.GEO20.v08");//rel17p2.v03
    //triggerTools.my_leptonTriggerSF->setAFII(false);
    triggerTools.my_leptonTriggerSF_ee = new LeptonTriggerSF(2012, "./external", "muon_trigger_sf_2012_AtoL.p1328.root",  "./lib_ext/ElectronEfficiencyCorrection/data", "rel17p2.GEO20.v08");
    
    triggerTools.my_triggerNavigationVariables = new TriggerNavigationVariables();
    triggerTools.my_triggerNavigationVariables->set_trig_DB_SMK(trig_DB_SMK);
    triggerTools.my_triggerNavigationVariables->set_trig_Nav_n(trig_Nav_n);
    triggerTools.my_triggerNavigationVariables->set_trig_Nav_chain_ChainId(trig_Nav_chain_ChainId);
    triggerTools.my_triggerNavigationVariables->set_trig_Nav_chain_RoIType(trig_Nav_chain_RoIType);
    triggerTools.my_triggerNavigationVariables->set_trig_Nav_chain_RoIIndex(trig_Nav_chain_RoIIndex);
    // electron 
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_e_egammaContainer_egamma_Electrons(trig_RoI_EF_e_egammaContainer_egamma_Electrons);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus(trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_el_n(trig_EF_el_n);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_el_eta(trig_EF_el_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_el_phi(trig_EF_el_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_el_Et(trig_EF_el_Et);
    // muon 
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_mu_Muon_ROI(trig_RoI_EF_mu_Muon_ROI);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFInfoContainer(trig_RoI_EF_mu_TrigMuonEFInfoContainer);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus(trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_L2_mu_CombinedMuonFeature(trig_RoI_L2_mu_CombinedMuonFeature);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_L2_mu_CombinedMuonFeatureStatus(trig_RoI_L2_mu_CombinedMuonFeatureStatus);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_L2_mu_MuonFeature(trig_RoI_L2_mu_MuonFeature);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_L2_mu_Muon_ROI(trig_RoI_L2_mu_Muon_ROI);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_CB_pt(trig_EF_trigmuonef_track_CB_pt);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_CB_eta(trig_EF_trigmuonef_track_CB_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_CB_phi(trig_EF_trigmuonef_track_CB_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_SA_pt(trig_EF_trigmuonef_track_SA_pt);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_SA_eta(trig_EF_trigmuonef_track_SA_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_SA_phi(trig_EF_trigmuonef_track_SA_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmugirl_track_CB_pt(trig_EF_trigmugirl_track_CB_pt);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmugirl_track_CB_eta(trig_EF_trigmugirl_track_CB_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmugirl_track_CB_phi(trig_EF_trigmugirl_track_CB_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_L2_combmuonfeature_eta(trig_L2_combmuonfeature_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_L2_combmuonfeature_phi(trig_L2_combmuonfeature_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_L2_muonfeature_eta(trig_L2_muonfeature_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_L2_muonfeature_phi(trig_L2_muonfeature_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_L1_mu_eta(trig_L1_mu_eta);
    triggerTools.my_triggerNavigationVariables->set_trig_L1_mu_phi(trig_L1_mu_phi);
    triggerTools.my_triggerNavigationVariables->set_trig_L1_mu_thrName(trig_L1_mu_thrName);
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFIsolationContainer(trig_RoI_EF_mu_TrigMuonEFIsolationContainer); // for 2012 isolated trigger
    triggerTools.my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus(trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus); // for 2012 isolated trigger
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_EF_mu24i_tight(trig_EF_trigmuonef_EF_mu24i_tight);  // for ntuple made with TriggerMenuAnalysis-00-02-86
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_EF_mu36_tight(trig_EF_trigmuonef_EF_mu36_tight); // for ntuple made with TriggerMenuAnalysis-00-02-86
    triggerTools.my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_MuonType(trig_EF_trigmuonef_track_MuonType);  // for full scan trigger matching 
    // set trigger matching tools
    triggerTools.my_muonTriggerMatching = new MuonTriggerMatching(triggerTools.my_triggerNavigationVariables);
    triggerTools.my_muonTriggerMatching->setDeltaR(0.15);
    triggerTools.my_electronTriggerMatching = new ElectronTriggerMatching(triggerTools.my_triggerNavigationVariables);
    triggerTools.my_electronTriggerMatching->setDeltaR(0.15);

    if (not triggerTools.my_triggerNavigationVariables->isValid()) {
    std::cerr << "VARIABLES NOT CORRECTLY SET\n";
    }  

    // muon isolation etcone correction
    glob.muonIsoTool = new CorrectCaloIso(); 

    //-----------Initialize electron Likelihood tool
    glob.electronLLHTool = new Root::TElectronLikelihoodTool; 
    glob.electronLLHTool->setPDFFileName("./lib_ext/ElectronPhotonSelectorTools/data/ElectronLikelihoodPdfs.root");
    glob.electronLLHTool->setOperatingPoint(LikeEnum::VeryTight);
    glob.electronLLHTool->initialize();

    //----- Set the file containing the pt leakage correction graphs
    CaloIsoCorrection::SetPtLeakageCorrectionsFile("./lib_ext/egammaAnalysisUtils/share/isolation_leakage_corrections.root");


	//------Initialize MET tool--------------------
	metTools.initialize();




    std::cout << "Initialized CP tools" << std::endl;

    //***************************************************************
    // Begin Event Loop
    //***************************************************************
    std::cout << "INFO  Starting loop over the event entries" << std::endl;
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t n=0; n<nentries; n++) {
        if (glob.nev>=glob.maxEvents) break; 
	
        //std::cout << "INFO  Processing entry "<< n << std::endl;

        Long64_t ientry = LoadTree(n);
        if (ientry < 0) break;
        // read the entry n from fChain
        nb = fChain->GetEntry(n);   nbytes += nb;
      /*  if ( ( glob.nev<=10 ) ||
        ( glob.nev<=100 && (glob.nev%10) == 0 ) ||
        ( glob.nev<=1000 && (glob.nev%100) == 0)  ||
        ( glob.nev>1000 && (glob.nev%1000) == 0 ) ) {
        std::cout << "Events= " << glob.nev << endl; } 
      */

        // Initialize variables
        glob.initializeVar();

        //----Initialize weights--------
        if(isMC) {
            if ((mcevt_weight->at(0)).size() > 0) 
                glob.mc_event_weight = (mcevt_weight->at(0))[0];
            else
                glob.mc_event_weight = mc_event_weight;
        } else {
            glob.mc_event_weight = 1.;
        }
        glob.mcEventWeight = 1.;
        glob.lepTrigSFEventWeight = 1.;
        glob.lepTrigSFEventWeightUp = 1.;
        glob.lepTrigSFEventWeightDown = 1.;
        glob.pileUpEventWeight = 1.;
        glob.lepIDSFWeight1 = 1.;
        glob.lepIDSFWeight2 = 1.;
        glob.lepRecoSFWeight1 = 1.;
        glob.lepRecoSFWeight2 = 1.;
        glob.nev++;

        //-----------------------------------------------------------
        // Get general events
        //-----------------------------------------------------------
        //std::cout << "General event" << std::endl;
        glob.lbn = lbn;
        glob.bcid = bcid; 
        //if(isData)glob.bcid = 1;  //Prague skim doesn't have bcid
        glob.runNumber = RunNumber;
        glob.eventNumber = EventNumber;
        glob.larError = larError;
        glob.tileError = tileError;
        glob.EF_e24vhi_medium1 = EF_e24vhi_medium1;
        glob.EF_e60_medium1 = EF_e60_medium1;
        glob.EF_mu24i_tight = EF_mu24i_tight;
        glob.EF_mu36_tight = EF_mu36_tight;
        glob.EF_mu18_tight_mu8_EFFS = EF_mu18_tight_mu8_EFFS;
        glob.EF_2e12Tvh_loose1 = EF_2e12Tvh_loose1;
        glob.EF_e12Tvh_medium1_mu8 = EF_e12Tvh_medium1_mu8;
        
        int RandomRunNumber = RunNumber;
        glob.isGoodMC = 0;
        Dataset cur_dataset;
        cur_dataset.dsID = 0;
        if(isMC){

            //if(glob.doTruthMatching)TrueParticles(ientry);

            //-------------------------------------------------------
            // pileup reweighting
            //-------------------------------------------------------
            double mu = (lbn==1 && int(averageIntPerXing+0.5)==1) ? 0. : averageIntPerXing;
            glob.averageIntPerXing = mu;
            glob.pileUpEventWeight = glob.my_PileUpReweighting->GetCombinedWeight(RunNumber,mc_channel_number,mu);	
            glob.my_PileUpReweighting->SetRandomSeed( 314159 + mc_channel_number*2718 + EventNumber);
            RandomRunNumber = glob.my_PileUpReweighting->GetRandomRunNumber(RunNumber);
            glob.runNumber = RandomRunNumber;

            //-------------------------------------------------------
            // Vertex position reweighting 
            //-------------------------------------------------------
            double zvtx_pos = 0.;
            double z_cut = 0.000001;
            for (int i = 0; i < mc_n; ++i) {
                zvtx_pos = mc_vx_z->at(i);
                if (fabs(zvtx_pos) > z_cut) break;  //use the first non-zero value as sometimes th 0th value is filled with zero
            }
            //for (int i = 0; i < vxp_n; ++i) {
            //    zvtx_pos = vxp_z->at(i);
            //    if (fabs(zvtx_pos) > z_cut) break;  //use the first non-zero value as sometimes th 0th value is filled with zero
            //}
            if (fabs(zvtx_pos) < z_cut)
                std::cout << "Ana::Loop  Warning: Vertex z-pos used for reweighting is zero. Could not find a non-zero value" << std::endl;
            glob.vertexPositionWeight = glob.zvtx_tool->GetWeight(zvtx_pos);

            //-------------------------------------------------------
            //get the dataset xsection, k-factor, filter efficiency, number fo events
            // temporary implementation-->Have to move out of the event loop
            //-------------------------------------------------------
            if (n==0) {
                cur_dataset.dsID = 0;
                glob.isGoodMC = 0;
                std::vector<Dataset*>::iterator it;
                for(it = glob.dsContainer.begin(); it != glob.dsContainer.end(); ++it){
                   int dsid = (*it)->dsID;
                   if (dsid == mc_channel_number) {
                      //std::cout << "found dsid: "<< dsid << std::endl;
                      cur_dataset = *(*it);
                      glob.isGoodMC = 1;
                      break;
                   }
                }
            }
        } else {
            glob.averageIntPerXing = averageIntPerXing;
            glob.pileUpEventWeight = 1.0;	
        }
         
        //std::cout << "mc_ch = " << mc_channel_number << "   " << "dsid = " << cur_dataset.dsID << std::endl;

        // temporary implementation-->Have to move out of the event loop
        if (n==0) {
            if (glob.isGoodMC == 1) {
               glob.dsID = cur_dataset.dsID;
               glob.xSection = cur_dataset.xSection;
               glob.kFactor = cur_dataset.kFactor;
               glob.filterEff = cur_dataset.filterEff;
               glob.Nevents = cur_dataset.Nevents;
               glob.isAFII = 0;
               if (cur_dataset.dataType == "AFII") {
                    glob.isAFII = 1;
                    //triggerTools.my_leptonTriggerSF->setAFII(true);
                }
            } else {
               if(!isData) std::cout << "WARNING: dataset is not found in the XSection file :" << cur_dataset.dsID << std::endl;
               glob.dsID = 0;
               glob.xSection = 1.;
               glob.kFactor = 1.;
               glob.filterEff = 1.;
               glob.Nevents = 1.;
               glob.isAFII = 0;
            }
        }

        // simplest mc event weight
        glob.mcEventWeight = glob.mc_event_weight * glob.pileUpEventWeight;

        // Count all events
        h.cutFlow->Fill(0.);
        h.cutFlow_weighted->Fill(0., glob.mcEventWeight);


        //-----------------------------------------------------------
        // Compute the sum of weights
        //-----------------------------------------------------------
        glob.SumMCEventWeight();


        //============== Starting applying filters ===================

        //-----------------------------------------------------------
        // Remove events with runnumber not in the good run list
        //-----------------------------------------------------------
        if ( isData && glob.grl.HasRunLumiBlock(RunNumber,lbn) == false) {
            continue;
        }

        // no cuts for MC samples, GRL filter for data
        h.cutFlow->Fill(1.);
        h.cutFlow_weighted->Fill(1., glob.mcEventWeight);

        //-----------------------------------------------------------
        // skip bad events
        //-----------------------------------------------------------
        //std::cout << "bad event" << std::endl;
        int badEvent = 0;
        if (tileError == 2) ++badEvent;
        if (larError == 2) ++badEvent;
        if ((coreFlags&0x40000) != 0) ++ badEvent;
        if (top_hfor_type == 4) ++badEvent;

        if (badEvent > 0) {
            continue;
        } 

        // Bad events removed (defined by WW twiki)
        h.cutFlow->Fill(2.);
        h.cutFlow_weighted->Fill(2., glob.mcEventWeight); // bad events (defined by WW twiki)	

        //-----------------------------------------------------------
        // Count number of vertices
        //   Need for electron likelihood tool
        //-----------------------------------------------------------
        // vertices with 3 tracks
        int npv_trk3 = 0;
        for (int i = 0; i < vxp_n; ++i) {
            if (vxp_nTracks->at(i) >= 3) { ++npv_trk3; }
        }
        glob.npv_trk3 = npv_trk3;

        // vertices with 2 tracks
        int npv_trk2 = 0;
        for (int i = 0; i < vxp_n; ++i) {
            if (vxp_trk_n->at(i) >= 2 && (vxp_type->at(i)==1 || vxp_type->at(i)==3)) { ++npv_trk2; }
        }
        glob.npv_trk2 = npv_trk2;


        //-----------------------------------------------------------
        // Apply event trigger
        //-----------------------------------------------------------
        //if( !((EF_e12Tvh_medium1_mu8==1)||(EF_mu18_tight_mu8_EFFS==1)||(EF_2e12Tvh_loose1==1)||(EF_e24vhi_medium1==1)||(EF_mu24i_tight==1)||(EF_e60_medium1==1)||(EF_mu36_tight==1)) ) continue;

        std::string trigger_chain = "";
        bool passTrigger = false;
        bool only_pass_dilep = true;

        if( (EF_mu18_tight_mu8_EFFS==1)||(EF_2e12Tvh_loose1==1)||(EF_e24vhi_medium1==1)||(EF_mu24i_tight==1)||(EF_e60_medium1==1)||(EF_mu36_tight==1) ) {
                passTrigger = true;
                only_pass_dilep = false;
                trigger_chain = "";    //default

            if ( !((EF_mu24i_tight==1)||(EF_mu36_tight==1)||(EF_mu18_tight_mu8_EFFS==1)) ) {
                trigger_chain = glob.electron_trigger_chain;
            } else if ( !((EF_e24vhi_medium1==1)||(EF_e60_medium1==1)||(EF_2e12Tvh_loose1==1)) )  {
                trigger_chain = glob.muon_trigger_chain;
            }

        } else if (EF_e12Tvh_medium1_mu8==1) {
            passTrigger = true;
            only_pass_dilep = true;  //redundant due to if-else-if statement
            trigger_chain = glob.electronMuon_trigger_chain;
        }

        if (!passTrigger) {
            only_pass_dilep = false;
            continue;
        }


        // Events surviving trigger
        h.cutFlow->Fill(3.);
        h.cutFlow_weighted->Fill(3.,glob.mcEventWeight); // do not pass trigger	


        //-----------------------------------------------------------
        // Get Truth stable mc_ particles
        //-----------------------------------------------------------
        if (isMC) {
            TruthParticles(ientry); 
        }
        
        //-----------------------------------------------------------
        //  Get good el_ electrons
        //-----------------------------------------------------------
        //std::cout << "Begin processing electron" << std::endl;
        glob.nEleAll = el_n;

        // LParticle electrons
        RecoElectrons(ientry);

        //-----------------------------------------------------------
        // Get good mu_staco_ muons
        //-----------------------------------------------------------
        //std::cout << "Begin processing muons" << std::endl;
        glob.nMuAll = mu_staco_n;

        // LParticle muons
        RecoMuons(ientry);

        //----------------------------
        //Get Vertices
        //----------------------------
        glob.nVxp  = vxp_n;

        //-----------------------------
        //Get Jets 
        //-----------------------------

        //-------------------------------------
        //Get MET
        //------------------------------------

        //-------------------------------------
        // Overlap removal 
        // Remove electrons within DR < 0.1 of muons
        //------------------------------------
        const double DeltaRCut = 0.1;
        std::vector<LParticle>::iterator elIt;
        std::vector<LParticle>::iterator muIt = glob.recoMuonObjects.begin();
        h.nElOverlap->Fill(1., (float)glob.recoElectronObjects.size() );

        while (muIt != glob.recoMuonObjects.end()) {  // looping over muon objects
      	    TLorentzVector tmp_mu = muIt->GetP();
            elIt = glob.recoElectronObjects.begin();
            while (elIt != glob.recoElectronObjects.end()) { // looping over electron objects
      	        TLorentzVector tmp_el = elIt->GetP();
                if(tmp_mu.DeltaR(tmp_el) < DeltaRCut) { // remove electron object if needed
                    std::cout << "INFO  Removing electron too close to muon, DR = " << tmp_mu.DeltaR(tmp_el) << std::endl;
                    glob.recoElectronObjects.erase(elIt--);
                }
                ++elIt;
            }
            ++muIt;
        }
        h.nElOverlap->Fill(2., (float)glob.recoElectronObjects.size() );


        //-------------------------------------
        // Sorting electron and muon object
        //   from highest pT to lowest
        //------------------------------------
        // Sort LParticle electrons with pT
        std::sort(glob.recoElectronObjects.begin(), glob.recoElectronObjects.end(), my_compare_LParticle); 
        // Sort muons descending in pT
        std::sort(glob.recoMuonObjects.begin(), glob.recoMuonObjects.end(), my_compare_LParticle); 

        /*------- Done processing electrons, muons, jets, met
        *************************************************
        *************************************************
        *************************************************
        -------------------------------------------------*/



        //-----------------------------------------------------------
        // Select the leading and subleading leptons
        // - Retrieve info on the leptons for the output ntuple
        // - Save lepton TLorentzVector for computing Trigger scale factor
        // - Do trigger matching
        //-----------------------------------------------------------
        glob.lepPDGID1 = 0; glob.lepPDGID2 = 0;
        glob.lepTrigMatched1 = false; glob.lepTrigMatched2 = false;
        bool trig1_matched(false), trig2_matched(false), trig3_matched(false);
        std::vector<TLorentzVector> eleSelected_tlv, muSelected_tlv; // TLorentzVector for computing Trigger scale factor
        TLorentzVector dilep_tlv;

        // Update number of good electrons and good muons after overlap removal
        glob.nEle = glob.recoElectronObjects.size();
        glob.nMu = glob.recoMuonObjects.size();
        h.nLepton->Fill( (glob.nEle+glob.nMu) );

        if ((glob.nEle+glob.nMu) < 2) {
            continue;
        }

        h.cutFlow->Fill(4.);
        h.cutFlow_weighted->Fill(4., glob.mcEventWeight);

        //-----------------------------------------------------------
        // Get the leading and subleading electrons and muons
        //-----------------------------------------------------------
        double elePt1(-1), elePt2(-1);
        double muPt1(-1), muPt2(-1);
        // get corrected electron ET for MC samples
        if (glob.nEle > 0) {
            TLorentzVector tmp_tlv = (glob.recoElectronObjects.at(0)).GetP();
            //eleSelected_tlv.push_back(tmp_tlv);
            elePt1 = tmp_tlv.Pt();
        }
        if (glob.nEle > 1) {
            TLorentzVector tmp_tlv = (glob.recoElectronObjects.at(1)).GetP();
            //eleSelected_tlv.push_back(tmp_tlv);
            elePt2 = tmp_tlv.Pt();
        }

        // get corrected muon pT for MC samples
        if (glob.nMu > 0) {
            TLorentzVector tmp_tlv = (glob.recoMuonObjects.at(0)).GetP();
            //muSelected_tlv.push_back(tmp_tlv);
            muPt1 = tmp_tlv.Pt();
        }
        if (glob.nMu > 1) {
            TLorentzVector tmp_tlv = (glob.recoMuonObjects.at(1)).GetP();
            //muSelected_tlv.push_back(tmp_tlv);
            muPt2 = tmp_tlv.Pt();
        }
        //std::cout << "INFO: Got the two highest pT leptons" << std::endl;

        //-----------------------------------------------------------
        // Require exactly two "good" leptons
        // Classify events into ee, mumu or emu channel
        //-----------------------------------------------------------
        if ((glob.nEle+glob.nMu) != 2) {
            continue;
        }
        h.cutFlow->Fill(5.);
        h.cutFlow_weighted->Fill(5., glob.mcEventWeight);

        //std::cout << "Begin classifying events" << std::endl;
        glob.channel = 0;
        eleSelected_tlv.clear(); 
        muSelected_tlv.clear();
        if( elePt1 > muPt1 && elePt2 > muPt1 ){ //----ee channel

            glob.channel = 1;
            //std::cout << "Channel: ee" << std::endl;
            // get first two electrons
            LParticle elec1_lp = glob.recoElectronObjects.at(0);
            LParticle elec2_lp = glob.recoElectronObjects.at(1);

            double lepCharge = elec1_lp.GetCharge();
            glob.lepPDGID1 = 11;
            if (lepCharge < 0.) glob.lepPDGID1 = -11; 
            GetElectronInfo(1, elec1_lp);
            eleSelected_tlv.push_back( elec1_lp.GetP() );
            double ele_tracketa = (elec1_lp.GetParameters()).at(EL_tracketa);
            double ele_trackphi = (elec1_lp.GetParameters()).at(EL_trackphi);
            trig1_matched = EF_e24vhi_medium1 && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e24vhi_medium1");
            trig2_matched = EF_e60_medium1 && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e60_medium1");
            glob.lepTrigMatched1 = trig1_matched || trig2_matched;

            // subleading lepton is an electron
            lepCharge = elec2_lp.GetCharge();
            glob.lepPDGID2 = 11;
            if (lepCharge < 0.) glob.lepPDGID2 = -11; 
            GetElectronInfo(2, elec2_lp);
            eleSelected_tlv.push_back( elec2_lp.GetP() );
            ele_tracketa = (elec2_lp.GetParameters()).at(EL_tracketa);
            ele_trackphi = (elec2_lp.GetParameters()).at(EL_trackphi);
            trig1_matched = EF_e24vhi_medium1 && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e24vhi_medium1");
            trig2_matched = EF_e60_medium1 && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e60_medium1");
            glob.lepTrigMatched2 = trig1_matched || trig2_matched;

            // dilepton trigger matching
            std::pair<bool, bool> result1;
            std::pair<bool, bool> result2;
            trig3_matched = EF_2e12Tvh_loose1 && triggerTools.trigmatch_di_electron(eleSelected_tlv.at(0), eleSelected_tlv.at(1), "EF_2e12Tvh_loose1", result1, result2);
            glob.dilepTrigMatched = trig3_matched;

            // dilepton system
            dilep_tlv = eleSelected_tlv.at(0) + eleSelected_tlv.at(1);

        } else if( muPt1 > elePt1 && muPt2 > elePt1 ){ //---mumu channel

            //std::cout << "Channel mumu" << std::endl;
            glob.channel = 2;
            LParticle muon1_lp = glob.recoMuonObjects.at(0);
            LParticle muon2_lp = glob.recoMuonObjects.at(1);

            double lepCharge = muon1_lp.GetCharge();
            glob.lepPDGID1 = 13;
            if (lepCharge < 0.) glob.lepPDGID1 = -13; 
            GetMuonInfo(1, muon1_lp);
            muSelected_tlv.push_back( muon1_lp.GetP() );
            double mu_eta = (muon1_lp.GetParameters()).at(MU_eta);
            double mu_phi = (muon1_lp.GetParameters()).at(MU_phi);
            trig1_matched = EF_mu24i_tight && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu24i_tight");
            trig2_matched = EF_mu36_tight && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu36_tight");
            glob.lepTrigMatched1 = trig1_matched || trig2_matched;


            // subleading lepton is an muon
            lepCharge = muon2_lp.GetCharge();
            glob.lepPDGID2 = 13;
            if (lepCharge < 0.) glob.lepPDGID2 = -13; 
            GetMuonInfo(2, muon2_lp);
            muSelected_tlv.push_back( muon2_lp.GetP() );
            mu_eta = (muon2_lp.GetParameters()).at(MU_eta);
            mu_phi = (muon2_lp.GetParameters()).at(MU_phi);
            trig1_matched = EF_mu24i_tight && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu24i_tight");
            trig2_matched = EF_mu36_tight && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu36_tight");
            glob.lepTrigMatched2 = trig1_matched || trig2_matched;

            // dilepton trigger matching
            std::pair<bool, bool> result1;
            std::pair<bool, bool> result2;
            trig3_matched = EF_mu18_tight_mu8_EFFS && triggerTools.trigmatch_di_muon(muSelected_tlv.at(0), muSelected_tlv.at(1), "EF_mu18_tight_mu8_EFFS", result1, result2);
            glob.dilepTrigMatched = trig3_matched;

            // dilepton system
            dilep_tlv = muSelected_tlv.at(0) + muSelected_tlv.at(1);

        } else if( elePt1 > muPt1 && muPt1 > elePt2 ){ //---emu channel

            //std::cout << "Channel emu" << std::endl;
            glob.channel = 3;
            LParticle elec1_lp = glob.recoElectronObjects.at(0);
            LParticle muon2_lp = glob.recoMuonObjects.at(0);

            // leading lepton is an electron
            double lepCharge = elec1_lp.GetCharge();
            glob.lepPDGID1 = 11;
            if (lepCharge < 0.) glob.lepPDGID1 = -11; 
            GetElectronInfo(1, elec1_lp);
            eleSelected_tlv.push_back( elec1_lp.GetP() );
            double ele_tracketa = (elec1_lp.GetParameters()).at(EL_tracketa);
            double ele_trackphi = (elec1_lp.GetParameters()).at(EL_trackphi);
            trig1_matched = EF_e24vhi_medium1 && (elePt1>25000.) && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e24vhi_medium1");
            trig2_matched = EF_e60_medium1 && (elePt1>25000.) && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e60_medium1");
            glob.lepTrigMatched1 = trig1_matched || trig2_matched;

            // subleading lepton is an electron
            lepCharge = muon2_lp.GetCharge();
            glob.lepPDGID2 = 13;
            if (lepCharge < 0.) glob.lepPDGID2 = -13; 
            GetMuonInfo(2, muon2_lp);
            muSelected_tlv.push_back( muon2_lp.GetP() );
            double mu_eta = (muon2_lp.GetParameters()).at(MU_eta);
            double mu_phi = (muon2_lp.GetParameters()).at(MU_phi);
            trig1_matched = EF_mu24i_tight && (muPt1>25000.) && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu24i_tight");
            trig2_matched = EF_mu36_tight && (muPt1>25000.) && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu36_tight");
            glob.lepTrigMatched2 = trig1_matched || trig2_matched;

            // dilepton trigger matching
            trig3_matched = EF_e12Tvh_medium1_mu8 && triggerTools.my_electronTriggerMatching->matchElectronMuon(eleSelected_tlv.at(0), muSelected_tlv.at(0), "EF_e12Tvh_medium1_mu8");
            if ((elePt1 < 15000.) || (muPt1 < 10000.)) trig3_matched = false;
            glob.dilepTrigMatched = trig3_matched;

            // dilepton system
            dilep_tlv = eleSelected_tlv.at(0) + muSelected_tlv.at(0);

        } else if( muPt1 > elePt1 && elePt1 > muPt2 ) { //---mue channel

            //std::cout << "Channel mue" << std::endl;
            glob.channel = 4;
            LParticle muon1_lp = glob.recoMuonObjects.at(0);
            LParticle elec2_lp = glob.recoElectronObjects.at(0);

            // leading lepton is an muon
            double lepCharge = muon1_lp.GetCharge();
            glob.lepPDGID1 = 13;
            if (lepCharge < 0.) glob.lepPDGID1 = -13; 
            GetMuonInfo(1, muon1_lp);
            muSelected_tlv.push_back( muon1_lp.GetP() );
            double mu_eta = (muon1_lp.GetParameters()).at(MU_eta);
            double mu_phi = (muon1_lp.GetParameters()).at(MU_phi);
            trig1_matched = EF_mu24i_tight && (muPt1>25000.) && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu24i_tight");
            trig2_matched = EF_mu36_tight && (muPt1>25000.) && triggerTools.my_muonTriggerMatching->match(mu_eta, mu_phi, "EF_mu36_tight");
            glob.lepTrigMatched1 = trig1_matched || trig2_matched;

            // subleading lepton
            lepCharge = elec2_lp.GetCharge();
            glob.lepPDGID2 = 11;
            if (lepCharge < 0.) glob.lepPDGID2 = -11; 
            GetElectronInfo(2, elec2_lp);
            eleSelected_tlv.push_back( elec2_lp.GetP() );
            double ele_tracketa = (elec2_lp.GetParameters()).at(EL_tracketa);
            double ele_trackphi = (elec2_lp.GetParameters()).at(EL_trackphi);
            trig1_matched = EF_e24vhi_medium1 && (elePt1>25000.) && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e24vhi_medium1");
            trig2_matched = EF_e60_medium1 && (elePt1>25000.) && triggerTools.my_electronTriggerMatching->match(ele_tracketa, ele_trackphi, "EF_e60_medium1");
            glob.lepTrigMatched2 = trig1_matched || trig2_matched;

            // dilepton trigger matching
            trig3_matched = EF_e12Tvh_medium1_mu8 && triggerTools.my_electronTriggerMatching->matchElectronMuon(eleSelected_tlv.at(0), muSelected_tlv.at(0), "EF_e12Tvh_medium1_mu8");
            if ((elePt1 < 15000.) || (muPt1 < 10000.)) trig3_matched = false;
            glob.dilepTrigMatched = trig3_matched;

            // dilepton system
            dilep_tlv = eleSelected_tlv.at(0) + muSelected_tlv.at(0);

        }
        //std::cout << "Classified events" << std::endl;

        //-----------------------------------------------------------
        // Compute lepton trigger scale factor
        //-----------------------------------------------------------
        bool useGeV = false;
        // compute event scale factor for single electron and single muon trigger
        if(isMC){
            // For Atfast 2 samples    
            if (glob.isAFII == 1) {
                triggerTools.my_leptonTriggerSF->setAFII(true);
                triggerTools.my_leptonTriggerSF_ee->setAFII(true);
            }

            //std::cout << "Electron and muon size: " << eleSelected_tlv.size() << "  " << muSelected_tlv.size() << " chain: " << trigger_chain << std::endl;
            std::pair<double, double> trigSF_vtlh;

            if (glob.channel==1 && eleSelected_tlv.size()==2) {
                //std::cout << "INFO: lepton Trigger scale factor for ee" << std::endl;
                std::vector<electron_quality> ele_quality(eleSelected_tlv.size(), VeryTightLLH); 
                trigSF_vtlh = triggerTools.my_leptonTriggerSF_ee->GetTriggerSF(RandomRunNumber, useGeV, eleSelected_tlv, ele_quality, "EF_2e12Tvh_loose1");
                glob.trigSF_single_eVTightLH = trigSF_vtlh.first;
            }
            if (glob.channel==2 && muSelected_tlv.size()==2) {
                //std::cout << "INFO: lepton Trigger scale factor for mumu" << std::endl;
                std::vector<muon_quality> mu_quality(muSelected_tlv.size(), combined); 
                trigSF_vtlh = triggerTools.my_leptonTriggerSF_ee->GetTriggerSF(RandomRunNumber, useGeV, muSelected_tlv, mu_quality, "EF_mu18_tight_mu8_EFFS");
                glob.trigSF_single_eVTightLH = trigSF_vtlh.first;
            }
            if ((glob.channel==3 || glob.channel==4) && eleSelected_tlv.size()==1 && muSelected_tlv.size()==1) {
                //std::cout << "INFO: lepton Trigger scale factor for emu" << std::endl;
                std::vector<muon_quality> mu_quality(muSelected_tlv.size(), combined); 
                std::vector<electron_quality> ele_quality(eleSelected_tlv.size(), VeryTightLLH); 
                trigSF_vtlh = triggerTools.my_leptonTriggerSF->GetTriggerSF(RandomRunNumber, useGeV, muSelected_tlv, mu_quality, eleSelected_tlv, ele_quality);
                glob.trigSF_single_eVTightLH = trigSF_vtlh.first;
                //std::cout << "INFO: lepton Trigger sf: " << trigSF_vtlh.first << " +-" << trigSF_vtlh.second << std::endl;
            }
        } else {
            glob.trigSF_single_eTight = 1.;
            glob.trigSF_single_eMedium = 1.;
            glob.trigSF_single_eVTightLH = 1.;
        }
        //std::cout << "INFO: Get trigger SF" << std::endl;

        //std::cout << "mediumpp=" << mediumpp <<" verytightLLH=" << VeryTightLLH << " combined=" << combined << " loose=" << loose << std::endl;

        // Update mc event weight
        glob.mcEventWeight *= (glob.lepRecoSFWeight1 * glob.lepRecoSFWeight2 * glob.lepIDSFWeight1 * glob.lepIDSFWeight2 * glob.trigSF_single_eVTightLH);
        //glob.mcEventWeight=glob.pileUpEventWeight*glob.mc_event_weight*glob.lepRecoSFWeight1*glob.lepRecoSFWeight2*glob.lepIDSFWeight1*glob.lepIDSFWeight2;

        // Events with exactly two leptons (electrons or muons)
        h.cutFlow->Fill(6.);
        h.cutFlow_weighted->Fill(6.,glob.mcEventWeight); // must have only two leptons
        

		//Compute mll and mT  
        glob.mll=dilep_tlv.M();
        glob.pTll=dilep_tlv.Pt();
        //glob.transMass=TMath::Sqrt(dilep_tlv.M()*dilep_tlv.M()+2*(sqrt( dilep_tlv.Pt()*dilep_tlv.Pt() + dilep_tlv.M()*dilep_tlv.M())*MET.Pt()-MET.Px()*dilep_tlv.Px()-MET.Py()*dilep_tlv.Py()));
        //glob.transMass_recalculated = TMath::Sqrt(dilep_tlv.M()*dilep_tlv.M()+2*(sqrt( dilep_tlv.Pt()*dilep_tlv.Pt() + dilep_tlv.M()*dilep_tlv.M())*glob.nom_met_recalculated-glob.nom_metx_recalculated*dilep_tlv.Px()-glob.nom_mety_recalculated*dilep_tlv.Py()));

        // Verify offset between z0_wrtBL and z0_wrtPV
        double lepdz0BL2PV = glob.lepTrackz0beam1 - (glob.lepTrackz0pv1 + vxp_z->at(0));
        h.lep_dz0BL2PV->Fill(lepdz0BL2PV, glob.mcEventWeight);
        lepdz0BL2PV = glob.lepTrackz0beam2 - (glob.lepTrackz0pv2 + vxp_z->at(0));
        h.lep_dz0BL2PV->Fill(lepdz0BL2PV, glob.mcEventWeight);


        //////////////////////////////////////////////////////////////
        //
        // Event selection
        // -- Match tracks to selected leptons
        // -- Form the lepton vertex
        // -- Count tracks unmatched to leptons
        //
        //////////////////////////////////////////////////////////////

        //std::cout << "Begin processing tracks" << std::endl;
        glob.nTracksAll = trk_n;
        int nLeptons=glob.nEle+glob.nMu;
        if(nLeptons>1) {
            // determine whether tracks match to selected leptons
            RecoTracks(ientry);
        }

        // Sortin LParticle tracks in ascending dz0(unmatchedTrack, lepton)
        std::sort(glob.recoUnmatchedTrackObjects.begin(), glob.recoUnmatchedTrackObjects.end(), my_compare_track);

        //std::cout << "Done processing tracks" << std::endl;
        //--------------------------------------------------
        // keep 10th smallest dz0 of un-matched tracks
        //--------------------------------------------------
        int counter1(0), counter2(0);
        // Get values for ouput ntuple          
        std::vector<double> signed_dz0TrkLep;
        std::vector<double> pTUnmatchedTrk;
        std::vector<int> trkpix;
        std::vector<int> trksct;
        std::vector<LParticle>::iterator it;
        for (it = glob.recoUnmatchedTrackObjects.begin(); it != glob.recoUnmatchedTrackObjects.end(); ++it){
            if (counter1 < 10) {
                std::vector<Double32_t> par = it->GetParameters();
                signed_dz0TrkLep.push_back( par.at(TRK_dz0TrkLep) );
                pTUnmatchedTrk.push_back( par.at(TRK_pt) );
                trkpix.push_back( par.at(TRK_nPixHits) );
                trksct.push_back( par.at(TRK_nSCTHits) );
                ++counter1;
            } else {
                break;
            }
        }
	
        // If less than 10 unmatched tracks
        if (glob.nTrkUnmatched < 10) {
            int dn = 10 - glob.nTrkUnmatched;
            for (int i = 0; i < dn; ++i) {
                signed_dz0TrkLep.push_back(-999999);
                pTUnmatchedTrk.push_back(-99999);
                trkpix.push_back(-99999);
                trksct.push_back(-99999);
            }
        }
       	glob.trkdz0_0 = signed_dz0TrkLep.at(0); glob.trkdz0_1 = signed_dz0TrkLep.at(1); glob.trkdz0_2 = signed_dz0TrkLep.at(2); glob.trkdz0_3 = signed_dz0TrkLep.at(3); glob.trkdz0_4 = signed_dz0TrkLep.at(4); glob.trkdz0_5 = signed_dz0TrkLep.at(5); glob.trkdz0_6 = signed_dz0TrkLep.at(6); glob.trkdz0_7 = signed_dz0TrkLep.at(7); glob.trkdz0_8 = signed_dz0TrkLep.at(8); glob.trkdz0_9 = signed_dz0TrkLep.at(9); 

        glob.trkpt_0 = pTUnmatchedTrk.at(0); glob.trkpt_1 = pTUnmatchedTrk.at(1); glob.trkpt_2 = pTUnmatchedTrk.at(2); glob.trkpt_3 = pTUnmatchedTrk.at(3); glob.trkpt_4 = pTUnmatchedTrk.at(4); glob.trkpt_5 = pTUnmatchedTrk.at(5); glob.trkpt_6 = pTUnmatchedTrk.at(6); glob.trkpt_7 = pTUnmatchedTrk.at(7); glob.trkpt_8 = pTUnmatchedTrk.at(8); glob.trkpt_9 = pTUnmatchedTrk.at(9); 

        glob.trknsct_0 = trksct.at(0); glob.trknsct_1 = trksct.at(1); glob.trknsct_2 = trksct.at(2); glob.trknsct_3 = trksct.at(3); glob.trknsct_4 = trksct.at(4); glob.trknsct_5 = trksct.at(5); glob.trknsct_6 = trksct.at(6); glob.trknsct_7 = trksct.at(7); glob.trknsct_8 = trksct.at(8); glob.trknsct_9 = trksct.at(9); 

        glob.trknpix_0 = trkpix.at(0); glob.trknpix_1 = trkpix.at(1); glob.trknpix_2 = trkpix.at(2); glob.trknpix_3 = trkpix.at(3); glob.trknpix_4 = trkpix.at(4); glob.trknpix_5 = trkpix.at(5); glob.trknpix_6 = trkpix.at(6); glob.trknpix_7 = trkpix.at(7); glob.trknpix_8 = trkpix.at(8); glob.trknpix_9 = trkpix.at(9); 

        //--------------------------------------------------
        // Keep 4th highest pT matched-tracks
        //--------------------------------------------------
        std::sort(glob.recoMatchedTrackObjects.begin(), glob.recoMatchedTrackObjects.end(), my_compare_LParticle);
        counter1 = 0; counter2 = 0;
        std::vector<double> matchedTrack_pt1, matchedTrack_pt2;
        for (it = glob.recoMatchedTrackObjects.begin(); it != glob.recoMatchedTrackObjects.end(); ++it){
            std::vector<Double32_t> par = it->GetParameters();
            int type = 0;
            if (par.at(TRK_match)>0. && par.at(TRK_match)<2.) {type = 1;}
            else if (par.at(TRK_match) > 1.5) {type = 2;}

            // if track matched to leading lepton
            if (type==1 && counter1<4) {
                matchedTrack_pt1.push_back(par.at(TRK_pt));
                ++counter1;
            }
            // if track matched to second leading lepton
            if (type==2 && counter2<4) {
                matchedTrack_pt2.push_back(par.at(TRK_pt));
                ++counter2;
            }

            // Exit loop when got 4 highest pT
            if ((counter1 >= 4) && (counter2 >= 4)) {
                break;
            }
        }

        // If less than 4 matched tracks
        if ((int)matchedTrack_pt1.size() < 4) {
            int end = (4 - (int)matchedTrack_pt1.size());
            for (int i = 0; i < end; ++i)
                matchedTrack_pt1.push_back(-1.0);
        }
        if ((int)matchedTrack_pt2.size() < 4) {
            int end = (4 - (int)matchedTrack_pt2.size());
            for (int i = 0; i < end; ++i)
                matchedTrack_pt2.push_back(-1.0);
    }
        //std::cout << "size1: " << (int)matchedTrack_pt1.size() << std::endl;;
        //std::cout << "size1: " << (int)matchedTrack_pt2.size() << std::endl;;
        glob.matchedTrkPt1_0 = matchedTrack_pt1.at(0); glob.matchedTrkPt1_1 = matchedTrack_pt1.at(1); glob.matchedTrkPt1_2 = matchedTrack_pt1.at(2); glob.matchedTrkPt1_3 = matchedTrack_pt1.at(3);
        glob.matchedTrkPt2_0 = matchedTrack_pt2.at(0); glob.matchedTrkPt2_1 = matchedTrack_pt2.at(1); glob.matchedTrkPt2_2 = matchedTrack_pt2.at(2); glob.matchedTrkPt2_3 = matchedTrack_pt2.at(3);

        //std::cout << "write tree \n";
        // Write to TTree dilep
        h.dilep->Fill();

        //glob.SumMCEventWeight();
      	glob.TotalEvents++;

   } // end loop over nentries

   //fdebug.close();

     //if(isMC){
         delete glob.my_PileUpReweighting;
         delete triggerTools.my_leptonTriggerSF;
         delete triggerTools.my_leptonTriggerSF_ee;
         delete triggerTools.my_triggerNavigationVariables;
         delete triggerTools.my_muonTriggerMatching;
         delete triggerTools.my_electronTriggerMatching;
    //}
    //std::cout << "INFO: End event loop" << std::endl;
}
