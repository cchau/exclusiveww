/*
 *      Cut.cxx
 *      
 *      
 */


#include "Ana.h"
#include "Global.h"
#include "Histo.h"

#include<stdio.h>
#include<stdlib.h>
#include "SystemOfUnits.h"
#include<TROOT.h>
#include<stdio.h>
#include<TMath.h>
#include<iostream>
#include<string.h>
#include<vector>


extern Histo h;
extern Global glob;


int Ana::GetMuonInfo(int isLeading, LParticle & muon_lp)
{
    std::vector<Double32_t> par = muon_lp.GetParameters();
    if (isLeading == 1) { //if true: leading lepton
        glob.lepPt1 = (isMC) ? par.at(MU_cor_pt): par.at(MU_pt);
        glob.lepEta1 = par.at(MU_eta);
        glob.lepPhi1 = par.at(MU_phi);
        glob.lepM1 = par.at(MU_m);
        glob.lepCharge1 = par.at(MU_charge);
        glob.lepIsMediumPP1 = 0;
        glob.lepIsTight1 = par.at(MU_tight);
        glob.lepIsTightPP1 = 0;
        glob.lepIsMediumPPIso1 = 0;
        glob.lepIsTightPPIso1 = 0;
        glob.lepIsVeryTightLH1 = 0;
        glob.lepIsCombined1 = par.at(MU_isCombinedMuon);
        //glob.lepEtcone20_1 = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone20->at(i), glob.npv_trk3, mu_staco_eta->at(i), "cone20Comb");
        glob.lepEtcone20_1 = par.at(MU_cor_etcone20);
        //glob.lepEtcone30_1 = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone30->at(i), glob.npv_trk3, mu_staco_eta->at(i), "cone30Comb");
        glob.lepEtcone30_1 = par.at(MU_cor_etcone30);
        //glob.lepEtcone40_1 = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone40->at(i), glob.npv_trk3, mu_staco_eta->at(i), "cone40Comb");
        glob.lepEtcone40_1 = par.at(MU_cor_etcone40);
        glob.lepPtcone20_1 = par.at(MU_ptcone20);
        glob.lepPtcone30_1 = par.at(MU_ptcone30);
        glob.lepPtcone40_1 = par.at(MU_ptcone40);
        //glob.lepTrackPt1 = (TMath::Sin(mu_staco_id_theta->at(indexMuCont)) * glob.lepCharge1) / (mu_staco_id_qoverp->at(indexMuCont));
        glob.lepTrackPt1 = par.at(MU_trackpt);
        //glob.lepTrackEta1 = -1 * TMath::Log(TMath::Tan(mu_staco_id_theta->at(indexMuCont) / 2));
        glob.lepTrackEta1 = par.at(MU_tracketa);
        glob.lepTrackPhi1 = par.at(MU_trackphi);
        glob.lepTrackz0pv1 = par.at(MU_trackz0pv);
        glob.lepTrackd0pv1 = par.at(MU_trackd0pv);
        glob.lepTrackz0beam1 = par.at(MU_trackz0beam);
        glob.lepTrackd0beam1 = par.at(MU_trackd0beam);
        glob.lepTrackTheta1 = par.at(MU_tracktheta);
        glob.lepTrackz0pvunbiased1 = par.at(MU_trackz0pvunbiased);
        glob.lepTrackd0pvunbiased1 = par.at(MU_trackd0pvunbiased);
        glob.lepTracksigd0pvunbiased1 = par.at(MU_tracksigd0pvunbiased);
        glob.lepnPixHits1 = par.at(MU_nPixHits);
        glob.lepnSCTHits1 = par.at(MU_nSCTHits);
        glob.lepnPixHoles1 = par.at(MU_nPixHoles);
        glob.lepnSCTHoles1 = par.at(MU_nSCTHoles);
        glob.lepnPixDeadSensors1 = par.at(MU_nPixelDeadSensors);
        glob.lepnSCTDeadSensors1 = par.at(MU_nSCTDeadSensors);

        //if(isMC ) glob.lepIDSFWeight1 = glob.mu_IDSF.at(indexLocal);
        if (isMC) {
            glob.lepIDSFWeight1 = par.at(MU_idSF);
            glob.lepIDSFWeight1_sys = par.at(MU_idSF_sysError) + par.at(MU_idSF_statError);
            glob.lepIsolationSF1 = par.at(MU_isolationSF);
            glob.lepIsolationSF1_sys = par.at(MU_isolationSF_sys);
        } else {
            glob.lepIDSFWeight1 = 1.;
            glob.lepIDSFWeight1_sys = 0.;
            glob.lepIsolationSF1 = 1.;
            glob.lepIsolationSF1_sys = 0.;
        }
    } else if (isLeading == 2) { //if true: subleading lepton
        glob.lepPt2 = (isMC) ? par.at(MU_cor_pt): par.at(MU_pt);
        glob.lepEta2 = par.at(MU_eta);
        glob.lepPhi2 = par.at(MU_phi);
        glob.lepM2 = par.at(MU_m);
        glob.lepCharge2 = par.at(MU_charge);
        glob.lepIsMediumPP2 = 0;
        glob.lepIsTight2 = par.at(MU_tight);
        glob.lepIsTightPP2 = 0;
        glob.lepIsMediumPPIso2 = 0;
        glob.lepIsTightPPIso2 = 0;
        glob.lepIsVeryTightLH2 = 0;
        glob.lepIsCombined2 = par.at(MU_isCombinedMuon);
        //glob.lepEtcone20_2 = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone20->at(i), glob.npv_trk3, mu_staco_eta->at(i), "cone20Comb");
        glob.lepEtcone20_2 = par.at(MU_cor_etcone20);
        //glob.lepEtcone30_2 = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone30->at(i), glob.npv_trk3, mu_staco_eta->at(i), "cone30Comb");
        glob.lepEtcone30_2 = par.at(MU_cor_etcone30);
        //glob.lepEtcone40_2 = glob.muonIsoTool->CorrectEtCone(mu_staco_etcone40->at(i), glob.npv_trk3, mu_staco_eta->at(i), "cone40Comb");
        glob.lepEtcone40_2 = par.at(MU_cor_etcone40);
        glob.lepPtcone20_2 = par.at(MU_ptcone20);
        glob.lepPtcone30_2 = par.at(MU_ptcone30);
        glob.lepPtcone40_2 = par.at(MU_ptcone40);
        //glob.lepTrackPt2 = (TMath::Sin(mu_staco_id_theta->at(indexMuCont)) * glob.lepCharge1) / (mu_staco_id_qoverp->at(indexMuCont));
        glob.lepTrackPt2 = par.at(MU_trackpt);
        //glob.lepTrackEta2 = -1 * TMath::Log(TMath::Tan(mu_staco_id_theta->at(indexMuCont) / 2));
        glob.lepTrackEta2 = par.at(MU_tracketa);
        glob.lepTrackPhi2 = par.at(MU_trackphi);
        glob.lepTrackz0pv2 = par.at(MU_trackz0pv);
        glob.lepTrackd0pv2 = par.at(MU_trackd0pv);
        glob.lepTrackz0beam2 = par.at(MU_trackz0beam);
        glob.lepTrackd0beam2 = par.at(MU_trackd0beam);
        glob.lepTrackTheta2 = par.at(MU_tracktheta);
        glob.lepTrackz0pvunbiased2 = par.at(MU_trackz0pvunbiased);
        glob.lepTrackd0pvunbiased2 = par.at(MU_trackd0pvunbiased);
        glob.lepTracksigd0pvunbiased2 = par.at(MU_tracksigd0pvunbiased);
        glob.lepnPixHits2 = par.at(MU_nPixHits);
        glob.lepnSCTHits2 = par.at(MU_nSCTHits);
        glob.lepnPixHoles2 = par.at(MU_nPixHoles);
        glob.lepnSCTHoles2 = par.at(MU_nSCTHoles);
        glob.lepnPixDeadSensors2 = par.at(MU_nPixelDeadSensors);
        glob.lepnSCTDeadSensors2 = par.at(MU_nSCTDeadSensors);

        //glob.lepIDSFWeight2 = (isMC) ? par.at(MU_idSF): 1.;
        if (isMC) {
            glob.lepIDSFWeight2 = par.at(MU_idSF);
            glob.lepIDSFWeight2_sys = par.at(MU_idSF_sysError) + par.at(MU_idSF_statError);
            glob.lepIsolationSF2 = par.at(MU_isolationSF);
            glob.lepIsolationSF2_sys = par.at(MU_isolationSF_sys);
        } else {
            glob.lepIDSFWeight2 = 1.;
            glob.lepIDSFWeight2_sys = 0.;
            glob.lepIsolationSF2 = 1.;
            glob.lepIsolationSF2_sys = 0.;
        }

    } else { // wrong value: something is wrong
        std::cout << "ERROR: GEtMuonInfo cannot retrieve info for leptons other than the two leading leptons" << std::endl;
        return -1;
    }
    //std::cout << "INFO: Done retrieving muon info" << std::endl;

    return 0;
}
