/*
 * RecoElectrons.cxx
 *
 *
 *
 *
 */

#include "Ana.h"
#include "Global.h"
#include "Histo.h"
#include "SystemOfUnits.h"
#include <iostream>
//#include <cstdlib>

#include<TROOT.h>
#include<TMath.h>
//#include<TClonesArray.h>
#include<TH1D.h>
#include<TH2F.h>
#include "LParticle.h"

#include<iostream>
#include<vector>
using namespace std;

extern Histo h;
extern Global glob;
const double kPI = TMath::Pi();

int Ana::RecoElectrons(Long64_t entry) {

    // Empty reco electron container
    glob.recoElectronObjects.clear();
    glob.truthElectronObjects.clear();


    //---Set random seed for electron energy rescaler
    glob.ers.SetRandomSeed(glob.eventNumber);

    //------------------------------------------------------
    // Loop over number of electrons
    //------------------------------------------------------
    for (unsigned int iEl = 0; iEl < (unsigned int)el_n; ++iEl) {

        // Skip electron is pT is too low
        if (el_pt->at(iEl) < 1000.) continue;

        //--------------------------------------------------
        // Compute electron likelihood
        //--------------------------------------------------
        bool passVTLH = false; 

        //get deltaPoverp
        double dpOverp = 0.;
        for (UInt_t i = 0; i < (UInt_t)(el_refittedTrack_LMqoverp->at(iEl)).size();++i) {
            if( (el_refittedTrack_author->at(iEl))[i] == 4) {
                dpOverp = 1. - ( el_trackqoverp->at(iEl) / (el_refittedTrack_LMqoverp->at(iEl))[i] );
            }
        }

        double eta = el_etas2->at(iEl);
        double Et = el_cl_E->at(iEl) / TMath::CosH(eta);
        double f3 = el_f3->at(iEl);
        double rHad = el_Ethad->at(iEl) / Et;
        double rHad1 = el_Ethad1->at(iEl) / Et;
        double Reta = el_reta->at(iEl);
        double w2 = el_weta2->at(iEl);
        double f1 = el_f1->at(iEl);
        double wstot = el_wstot->at(iEl);
        double eratio = (el_emaxs1->at(iEl)+el_Emax2->at(iEl) == 0) ? 0.: (el_emaxs1->at(iEl)-el_Emax2->at(iEl))/(el_emaxs1->at(iEl)+el_Emax2->at(iEl));
        double deltaEta = el_deltaeta1->at(iEl);
        double d0 = el_trackd0pvunbiased->at(iEl);
        double TRratio = el_TRTHighTOutliersRatio->at(iEl);
        double d0sigma = el_tracksigd0pvunbiased->at(iEl);
        double rphi = el_rphi->at(iEl);
        double deltaPoverP = dpOverp;
        double deltaphires = el_deltaphiRescaled->at(iEl);
        int nSi = el_nSiHits->at(iEl);
        int nSiDeadSensors = el_nPixelDeadSensors->at(iEl) + el_nSCTDeadSensors->at(iEl);
        int nPix = el_nPixHits->at(iEl);
        int nPixDeadSensors = el_nPixelDeadSensors->at(iEl);
        int nBlayer = el_nBLHits->at(iEl);
        int nBlayerOutliers = el_nBLayerOutliers->at(iEl);
        int expectBlayer = (el_expectHitInBLayer->at(iEl) == -999)? true: el_expectHitInBLayer->at(iEl);
        int convBit = el_isEM->at(iEl) & (0x1 << 1);
        double ip = glob.npv_trk2;

        double discriminant = glob.electronLLHTool->calculate(eta, Et, f3, rHad, rHad1, Reta, w2, f1, eratio, deltaEta, d0, TRratio, d0sigma, rphi, deltaPoverP, deltaphires, ip);
        passVTLH = (bool)glob.electronLLHTool->accept(discriminant, eta, Et, nSi, nSiDeadSensors, nPix, nPixDeadSensors, nBlayer, nBlayerOutliers, (bool)expectBlayer, convBit, ip);

        //--------------------------------------------------
        // Select good electron objects
        //--------------------------------------------------
        int nSCTHits = el_nSCTHits->at(iEl);
        double eleE = el_cl_E->at(iEl);
        double eleEta = el_cl_eta->at(iEl);
        double eleEt = ((nPix+nSCTHits) >= 4)? (eleE / TMath::CosH(el_tracketa->at(iEl))) : (eleE / TMath::CosH(eleEta));

        float bin = 0.;
        h.elecSelection->Fill(bin); ++bin;

        //---- Apply filter on electrons
        if ((el_author->at(iEl) != 1) && (el_author->at(iEl) != 3)) continue;
        h.elecSelection->Fill(bin); ++bin;
        if ((el_OQ->at(iEl) & 1446) != 0) continue;
        h.elecSelection->Fill(bin); ++bin;
        if ((fabs(eleEta) > 2.47) || (fabs(eleEta) > 1.37 && fabs(eleEta) < 1.52)) continue;
        h.elecSelection->Fill(bin); ++bin;
        if ((nPix + nSCTHits + nSiDeadSensors) < 4) continue;
        h.elecSelection->Fill(bin); ++bin;
        if ((nPix + el_nPixelDeadSensors->at(iEl)) < 1) continue;
        h.elecSelection->Fill(bin); ++bin;
        if ((nSCTHits + el_nSCTDeadSensors->at(iEl)) < 3) continue;
        h.elecSelection->Fill(bin); ++bin;
        if (!passVTLH) continue; //must be veryTightLH
        h.elecSelection->Fill(bin); ++bin;

        //---- Electron Et, smearing correction
        double eleEtc = eleEt;
        double eleAlpha(0.), eleAlpha_ZeeAllUp(0.), eleAlpha_ZeeAllDown(0.), eleAlpha_AllUp(0.0), eleAlpha_AllDown(0.);
        double eleSmf(1.0), eleSmf_ErrUp(-1.), eleSmf_ErrDown(-1.);
        double eleAFtoG4_sf = -1.0;
        if(isMC) {
            //compute electron energy scale, should apply to data? But now, applying to MC
            eleAlpha = glob.ers.getAlphaValue( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::Electron, egRescaler::EnergyRescalerUpgrade::Nominal);
            //eleAlpha_ZeeAllUp = glob.ers.getAlphaValue( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::Electron, egRescaler::EnergyRescalerUpgrade::ZeeAllUp);
            //eleAlpha_ZeeAllDown = glob.ers.getAlphaValue( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::Electron, egRescaler::EnergyRescalerUpgrade::ZeeAllDown);
            //eleAlpha_AllUp = glob.ers.getAlphaValue( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::Electron, egRescaler::EnergyRescalerUpgrade::AllUp);
            //eleAlpha_AllDown = glob.ers.getAlphaValue( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::Electron, egRescaler::EnergyRescalerUpgrade::AllDown);

            //compute the smearing correction factor 
            eleSmf = glob.ers.getSmearingCorrection( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::Nominal);
            //eleSmf_ErrUp = glob.ers.getSmearingCorrection( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::ERR_UP);
            //eleSmf_ErrDown = glob.ers.getSmearingCorrection( eleEta, eleE, egRescaler::EnergyRescalerUpgrade::ERR_DOWN);

            double eleScale = eleAlpha;
            if (glob.doElecEnergyScale == 0) {eleScale = eleAlpha;}
            //else if (glob.doElecEnergyScale == 1) {eleScale = eleAlpha_AllUp;}
            //else if (glob.doElecEnergyScale == -1) {eleScale = eleAlpha_AllDown;}

            double eleSmear = eleSmf;
            if (glob.doElecResolution == 0) {eleSmear = eleSmf;}
            //else if (glob.doElecResolution == 1) {eleSmear = eleSmf_ErrUp;}
            //else if (glob.doElecResolution == -1) {eleSmear = eleSmf_ErrDown;}

            // Corrected electron energy
            eleEtc = eleEt * (1. + eleScale) * eleSmear;

            // Correcting AFII shower to agree with G4
            if (glob.isAFII == 1) { 
                eleAFtoG4_sf = glob.ers.applyAFtoG4(eleEta); 
                eleEtc = eleEtc * eleAFtoG4_sf;
            }
        }

        // if value is NAN
        if (TMath::IsNaN(eleEtc)) {
            //std::cout <<"Electron Nan smear pT, initial value is " << eleEt << std::endl;
            eleEtc = eleEt;
        }

        //---- More filter on electrons
        if (eleEtc < 10000.) continue;
        h.elecSelection->Fill(bin); ++bin;

        double el_etcone30_corrected = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(iEl), eleE, el_etas2->at(iEl), el_etap->at(iEl), el_cl_eta->at(iEl), 30., (isMC? 1: 0), el_topoEtcone30->at(iEl));

        // Et cone and pT cone isolation
        if (eleEtc <= 15000.) {
            if ((el_etcone30_corrected / eleEtc) > 0.20) continue; 
            h.elecSelection->Fill(bin); ++bin;
            //
            if ((el_ptcone30->at(iEl) / eleEtc) > 0.06) continue; 
            h.elecSelection->Fill(bin); ++bin;
            
        } else if (eleEtc <= 20000.) {
            if ((el_etcone30_corrected / eleEtc) > 0.24) continue; 
            h.elecSelection->Fill(bin); ++bin;
            //
            if ((el_ptcone30->at(iEl) / eleEtc) > 0.08) continue; 
            h.elecSelection->Fill(bin); ++bin;

        } else {
            if ((el_etcone30_corrected / eleEtc) > 0.28) continue; 
            h.elecSelection->Fill(bin); ++bin;
            //
            if ((el_ptcone30->at(iEl) / eleEtc) > 0.10) continue; 
            h.elecSelection->Fill(bin); ++bin;
        }


        //std::cout << "INFO: Applied electron filters" << std::endl;

        //------------------------------------------------------------
        // Electron identification and reconstruction scale factors
        // Electron isolation scale factors
        //------------------------------------------------------------
        
        // Identification and reconstruction scale factors
        double eleReconSF = 1.0;
        double eleIdentSF_medium = 1.0;
        double eleIdentSF_tight = 1.0;
        double eleIdentSF_vTightLH = 1.0;
        //
        double eleReconSF_sys = 0.0;
        double eleIdentSF_medium_sys = 0.0;
        double eleIdentSF_tight_sys = 0.0;
        double eleIdentSF_vTightLH_sys = 0.0;

        // Isolation scale factors
        double eleIsoSF = 1.0;
        double eleIsoSF_sys = 0.0;

        // Compute scale factors
        if(isMC) {
            UInt_t randomRunNumber = glob.runNumber; 
            // needed for electron correction
            glob.dataType = FULLSIM;
            std::string isoOpts = "mc12aVTLHFullSim";

            if (glob.isAFII == 1) glob.dataType = ATFAST2;
            // compute electron reconstruction and identification weights
            PATCore::ParticleDataType::DataType particleDataType;
            if (glob.dataType == FULLSIM) {
                particleDataType = PATCore::ParticleDataType::Full;
                isoOpts = "mc12aVTLHFullSim";
            } else if (glob.dataType == ATFAST2) {
                particleDataType = PATCore::ParticleDataType::Fast;
                isoOpts = "mc12aVTLHAFII";
            } else {
                std::cout << "WARNING: Data type should be either Full simulation or ATFAST2, but got " << glob.dataType << std::endl;
            }
            const Root::TResult &result_reco = glob.tool_el_reco_SF.calculate(particleDataType, RunNumber, eleEta, eleEtc);
            const Root::TResult &result_medium = glob.tool_el_medium_SF.calculate(particleDataType, RunNumber, eleEta, eleEtc);
            const Root::TResult &result_tight = glob.tool_el_tight_SF.calculate(particleDataType, RunNumber, eleEta, eleEtc);
            const Root::TResult &result_vTightLH = glob.tool_el_vTightLLH_SF.calculate(particleDataType, RunNumber, eleEta, eleEtc);

            // Get scale factors
            eleReconSF = result_reco.getScaleFactor();
            eleIdentSF_medium = result_medium.getScaleFactor();
            eleIdentSF_tight = result_tight.getScaleFactor();
            eleIdentSF_vTightLH = result_vTightLH.getScaleFactor();
            //
            // Get total uncertainties
            eleReconSF_sys = result_reco.getTotalUncertainty();
            eleIdentSF_medium_sys = result_medium.getTotalUncertainty();
            eleIdentSF_tight_sys = result_tight.getTotalUncertainty();
            eleIdentSF_vTightLH_sys = result_vTightLH.getTotalUncertainty();

            // Get isolation scale factor
            eleIsoSF = glob.tool_elecIsoSF->GetIsolationSF(eleEtc, 15.0e3, false, isoOpts);
            eleIsoSF_sys = glob.tool_elecIsoSF->GetIsolationSFError(eleEtc, 15.0e3, false, isoOpts);
        }

        //--------------------------------------------------
        // Store all relevant electron branches
        //--------------------------------------------------

        double el_etcone20_corrected = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(iEl), eleE, el_etas2->at(iEl), el_etap->at(iEl), el_cl_eta->at(iEl), 20., (isMC? 1: 0), el_topoEtcone20->at(iEl));
        double el_etcone40_corrected = CaloIsoCorrection::GetPtEDCorrectedTopoIsolation(el_ED_median->at(iEl), eleE, el_etas2->at(iEl), el_etap->at(iEl), el_cl_eta->at(iEl), 40., (isMC? 1: 0), el_topoEtcone40->at(iEl));

        TLorentzVector ele_tlv;
        ele_tlv.SetPtEtaPhiM(eleEtc, eleEta, el_phi->at(iEl), el_m->at(iEl));

        // electron LParticle
        LParticle recoEl_lp;
        recoEl_lp.SetCharge( el_charge->at(iEl) );
        recoEl_lp.SetP (ele_tlv);
        //
        // set parameters following the enum ELEC order
        recoEl_lp.SetParameter( el_E->at(iEl) );
        recoEl_lp.SetParameter( el_Et->at(iEl) );
        recoEl_lp.SetParameter( el_pt->at(iEl) );
        recoEl_lp.SetParameter( el_m->at(iEl) );
        recoEl_lp.SetParameter( el_eta->at(iEl) );
        recoEl_lp.SetParameter( el_phi->at(iEl) );
        recoEl_lp.SetParameter( el_charge->at(iEl) );
        recoEl_lp.SetParameter( el_author->at(iEl) );
        recoEl_lp.SetParameter( el_OQ->at(iEl) );
        recoEl_lp.SetParameter( el_mediumPP->at(iEl) );
        recoEl_lp.SetParameter( passVTLH? 1: 0 );
        recoEl_lp.SetParameter( el_tightPP->at(iEl) );
        recoEl_lp.SetParameter( el_Etcone20->at(iEl) );
        recoEl_lp.SetParameter( el_Etcone30->at(iEl) );
        recoEl_lp.SetParameter( el_Etcone40->at(iEl) );
        recoEl_lp.SetParameter( el_ptcone20->at(iEl) );
        recoEl_lp.SetParameter( el_ptcone30->at(iEl) );
        recoEl_lp.SetParameter( el_ptcone40->at(iEl) );
        recoEl_lp.SetParameter( el_cl_E->at(iEl) );
        recoEl_lp.SetParameter( el_cl_pt->at(iEl) );
        recoEl_lp.SetParameter( el_cl_eta->at(iEl) );
        recoEl_lp.SetParameter( el_cl_phi->at(iEl) );
        recoEl_lp.SetParameter( el_trackd0->at(iEl) );
        recoEl_lp.SetParameter( el_trackz0->at(iEl) );
        recoEl_lp.SetParameter( el_trackd0pv->at(iEl) );
        recoEl_lp.SetParameter( el_trackz0pv->at(iEl) );
        recoEl_lp.SetParameter( el_trackd0beam->at(iEl) );
        recoEl_lp.SetParameter( el_trackz0beam->at(iEl) );
        recoEl_lp.SetParameter( el_trackd0pvunbiased->at(iEl) );
        recoEl_lp.SetParameter( el_trackz0pvunbiased->at(iEl) );
        recoEl_lp.SetParameter( el_tracksigd0pvunbiased->at(iEl) );
        recoEl_lp.SetParameter( el_trackpt->at(iEl) );
        recoEl_lp.SetParameter( el_trackphi->at(iEl) );
        recoEl_lp.SetParameter( el_tracketa->at(iEl) );
        recoEl_lp.SetParameter( el_tracktheta->at(iEl) );
        recoEl_lp.SetParameter( el_trackqoverp->at(iEl) );
        recoEl_lp.SetParameter( el_nPixHits->at(iEl) );
        recoEl_lp.SetParameter( el_nSCTHits->at(iEl) );
        recoEl_lp.SetParameter( el_nPixelDeadSensors->at(iEl) );
        recoEl_lp.SetParameter( el_nSCTDeadSensors->at(iEl) );
        recoEl_lp.SetParameter( el_nPixHoles->at(iEl) );
        recoEl_lp.SetParameter( el_nSCTHoles->at(iEl) );
        recoEl_lp.SetParameter( el_cl_E->at(iEl) / TMath::CosH(el_tracketa->at(iEl)) );
        recoEl_lp.SetParameter( eleAlpha );
        recoEl_lp.SetParameter( eleAlpha_ZeeAllUp );
        recoEl_lp.SetParameter( eleAlpha_ZeeAllDown );
        recoEl_lp.SetParameter( eleAlpha_AllUp );
        recoEl_lp.SetParameter( eleAlpha_AllDown );
        recoEl_lp.SetParameter( eleSmf );
        recoEl_lp.SetParameter( eleSmf_ErrUp );
        recoEl_lp.SetParameter( eleSmf_ErrDown );
        recoEl_lp.SetParameter( eleAFtoG4_sf );
        recoEl_lp.SetParameter( eleEtc );
        recoEl_lp.SetParameter( eleReconSF );
        recoEl_lp.SetParameter( eleIdentSF_medium );
        recoEl_lp.SetParameter( eleIdentSF_tight );
        recoEl_lp.SetParameter( eleIdentSF_vTightLH );
        recoEl_lp.SetParameter( eleReconSF_sys );
        recoEl_lp.SetParameter( eleIdentSF_medium_sys );
        recoEl_lp.SetParameter( eleIdentSF_tight_sys );
        recoEl_lp.SetParameter( eleIdentSF_vTightLH_sys );
        recoEl_lp.SetParameter( el_etcone20_corrected );
        recoEl_lp.SetParameter( el_etcone30_corrected );
        recoEl_lp.SetParameter( el_etcone40_corrected );
        recoEl_lp.SetParameter( eleIsoSF );
        recoEl_lp.SetParameter( eleIsoSF_sys );
        //
        // Save electron
        glob.recoElectronObjects.push_back(recoEl_lp);


        // Truth electron LParticle
        if (isMC) {
            LParticle truthEl_lp;
            //
            truthEl_lp.SetParameter( el_truth_E->at(iEl) );
            truthEl_lp.SetParameter( el_truth_pt->at(iEl) );
            truthEl_lp.SetParameter( el_truth_eta->at(iEl) );
            truthEl_lp.SetParameter( el_truth_phi->at(iEl) );
            truthEl_lp.SetParameter( el_truth_type->at(iEl) );
            truthEl_lp.SetParameter( el_truth_status->at(iEl) );
            truthEl_lp.SetParameter( el_truth_barcode->at(iEl) );
            truthEl_lp.SetParameter( el_truth_motherbarcode->at(iEl) );
            truthEl_lp.SetParameter( el_truth_matched->at(iEl) );
            truthEl_lp.SetParameter( el_truth_index->at(iEl) );
            truthEl_lp.SetParameter( el_truth_hasHardBrem->at(iEl) );
            truthEl_lp.SetParameter( el_type->at(iEl) );
            truthEl_lp.SetParameter( el_origin->at(iEl) );
            //
            // Save electron
            glob.truthElectronObjects.push_back(truthEl_lp);
        }

    } //--- End loop over electrons

    return 0;
}
