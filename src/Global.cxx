// put here some global variables 
#include <iostream>
using namespace std;

#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<TROOT.h>
#include"Global.h"
#include"TRandom3.h"
#include <algorithm>
#include <vector>


// constructor
Global::Global() {


    timer.Start();
    nev = 0;
    debug=false; 
    systematics=0;
    maxEvents=MAX_EVENTS;
    jfound=false;

    //maxEta=2.4;

    initializeVar();

    //---Good run list
    grlReader.SetXMLFile("lib_ext/data12_8TeV.periodAllYear_DetStatus-v61-pro14-02_DQDefects-00-01-00_PHYS_StandardGRL_All_Good.xml");
    grlReader.Interpret();
    grl=grlReader.GetMergedGoodRunsList();
    grl.Summary();

    //---------------------------------------------------------------
    // Initialize the electron efficiency tool
    //---------------------------------------------------------------
    // Electron reconstuction efficiency
	tool_el_reco_SF.addFileName("./lib_ext/ElectronEfficiencyCorrection/data/efficiencySF.offline.RecoTrk.2012.8TeV.rel17p2.GEO20.v08.root");
    tool_el_reco_SF.initialize();
    // electron identification quality efficiency
    tool_el_medium_SF.addFileName("./lib_ext/ElectronEfficiencyCorrection/data/efficiencySF.offline.Medium.2012.8TeV.rel17p2.v07.root");
    tool_el_medium_SF.initialize();
    tool_el_tight_SF.addFileName("./lib_ext/ElectronEfficiencyCorrection/data/efficiencySF.offline.Tight.2012.8TeV.rel17p2.v07.root");
    tool_el_tight_SF.initialize();
    tool_el_vTightLLH_SF.addFileName("./lib_ext/ElectronEfficiencyCorrection/data/efficiencySF.offline.VeryTightLLH.2012.8TeV.rel17p2.v07.root");
    tool_el_vTightLLH_SF.initialize();
    // Electron energy scale and smearing corrections
    ers.Init("./lib_ext/egammaAnalysisUtils/share/EnergyRescalerData.root","2012","es2012");


    //---------------------------------------------------------------
    // Initialize the muon efficiency tool
    //---------------------------------------------------------------
	//---Muon Smearing (Momentum Corrections)--------
	mcp_smear = new MuonSmear::SmearingClass("Data12","staco","pT","Rel17.2Repro","lib_ext/MuonMomentumCorrections/share/");
    //mcp_smear->RestrictCurvatureCorrections(2.5);
	mcp_smear->UseImprovedCombine();
	//---Muon Scale Factors----
    Analysis::AnalysisMuonConfigurableScaleFactors::Configuration config=Analysis::AnalysisMuonConfigurableScaleFactors::AverageOverPeriods;
    muonSF = new Analysis::AnalysisMuonConfigurableScaleFactors("lib_ext/MuonEfficiencyCorrections/share/", "STACO_CB_2012_SF.txt.gz", "MeV", config); 
	muonSF->Initialise();
  
    //---------------------------------------------------------------
    // Correction for global event such as pileup
    //---------------------------------------------------------------
    //--VertexPositionReweightingTool
    zvtx_tool = new VertexPositionReweightingTool(VertexPositionReweightingTool::MC12a, "lib_ext/egammaAnalysisUtils/share/zvtx_weights_2011_2012.root");

    //---------------------------------------------------------------
    // Initialize lepton isolation scale factor tool
    //---------------------------------------------------------------
    tool_elecIsoSF = new ElecIsolationSF();
    tool_muonIsoSF = new MuonIsolationSF();
}

// Initialize variables
void Global::initializeVar() {

    mc_event_weight = 1.; mcEventWeight = 1.;
    vertexPositionWeight = 1.;
    lepPDGID1 = 0; lepPDGID2 = 0;
    nLep = 0; nMuAll = 0; nMu = 0; nEleAll = 0; nEle = 0; runNumber = 0; eventNumber = 0;
    lepPt1 = -9.99; lepPt2 = -9.99; lepEta1 = -9.99; lepEta2 = -9.99; lepPhi1 = -9.99; lepPhi2 = -9.99; lepM1 = -9.99; lepM2 = -9.99; lepCharge1 = 0; lepCharge2 = 0;
    lepIsMediumPP1 = 0; lepIsTight1 = 0; lepIsTightPP1 = 0; lepIsMediumPPIso1 = 0; lepIsTightPPIso1 = 0; lepIsCombined1 = 0;
    lepIsMediumPP2 = 0; lepIsTight2 = 0; lepIsTightPP2 = 0; lepIsMediumPPIso2 = 0; lepIsTightPPIso2 = 0; lepIsCombined2 = 0;
    lepIsVeryTightLH1 = 0; lepIsVeryTightLH2 = 0;
    lepEtcone20_1 = -99.9; lepEtcone30_1 = -99.9; lepEtcone40_1 = -99.9; lepPtcone20_1 = -99.9; lepPtcone30_1 = -99.9; lepPtcone40_1 = -99.9;
    lepEtcone20_2 =-99.9; lepEtcone30_2 = -99.9; lepEtcone40_2 = -99.9; lepPtcone20_2 = -99.9; lepPtcone30_2 = -99.9; lepPtcone40_2 = -99.9;
    lepTrackPt1=-9.99; lepTrackPt2=-9.99; lepTrackEta1=-9.99; lepTrackEta2=-9.99; lepTrackPhi1=-9.99; lepTrackPhi2=-9.99; lepTrackqoverp1 = -99.9; lepTrackqoverp2 = -99.9;
    lepTrackz0pv1=-999.9; lepTrackd0pv1=-999.9; lepTrackz0pv2=-999.9; lepTrackd0pv2=-999.9;
    lepTrackz0beam1=-999.9; lepTrackz0beam2=-999.9; lepTrackd0beam1=-9.99; lepTrackd0beam2=-9.99;
    lepTrackTheta1=-9.99; lepTrackTheta2=-9.99; lepTrackz0pvunbiased1=-999.9; lepTrackz0pvunbiased2=-999.9; lepTrackd0pvunbiased1=-999.9; lepTrackd0pvunbiased2=-999.9; lepTracksigd0pvunbiased1=-99.9; lepTracksigd0pvunbiased2=-99.9;
    lepnPixHits1 = 0; lepnPixHits2=0; lepnSCTHits1=0; lepnSCTHits2=0; lepnPixHoles1=0; lepnPixHoles2=0; lepnSCTHoles1=0; lepnSCTHoles2=0; lepnPixDeadSensors1=0; lepnPixDeadSensors2=0; lepnSCTDeadSensors1=0; lepnSCTDeadSensors2=0;
    lepTrigMatched1 = false; lepTrigMatched2 = false;
    dilepTrigMatched = false;
    
    lepNMatchedTrk1=0; lepNMatchedTrk2=0;
    dilepdz0=-999.9; dilepAverageZ0=-999.9;
    
    //----General event
    lbn = -1;
    bcid = -1; 
    channel = 0;
    larError = 0; tileError = 0;
    EF_e24vhi_medium1=false; EF_e60_medium1=false;
    EF_mu24i_tight=false; EF_mu36_tight=false; 
    EF_mu18_tight_mu8_EFFS=false; EF_2e12Tvh_loose1=false;
    EF_e12Tvh_medium1_mu8 = false;
    
    //----Event scale factors
    trigSF_single_eTight = 1.;
    trigSF_single_eMedium = 1.;
    trigSF_single_eVTightLH = 1.;
    lepIDSFWeight1=1.; lepIDSFWeight2=1.; lepIDSFWeight1_sys=0.; lepIDSFWeight2_sys=0.;
    lepRecoSFWeight1=1.; lepRecoSFWeight1_sys=0.; lepRecoSFWeight2=1.; lepRecoSFWeight2_sys=0.;
    lepIsolationSF1=1.; lepIsolationSF2=1.; lepIsolationSF1_sys=0.; lepIsolationSF2_sys=0.;

    //---track variables---
	nTrkUnmatched=0; nTrkUnmatched_100mm=0; nTrkUnmatched_125mm=0; nTrkUnmatched_150mm=0; nTrkUnmatched_175mm=0;
    nTrkUnmatched_200mm=0; nTrkUnmatched_225mm=0; nTrkUnmatched_250mm=0; nTrkUnmatched_275mm=0; nTrkUnmatched_300mm=0;
    nTracksAll = 0; nTrkPix1SCT4 = 0;
    trkdz0_0=-999.9; trkdz0_1=-999.9; trkdz0_2=-999.9; trkdz0_3=-999.9; trkdz0_4=-999.9; trkdz0_5=-999.9; trkdz0_6=-999.9; trkdz0_7=-999.9; trkdz0_8=-999.9; trkdz0_9=-999.9; 
    trkpt_0=-9.99; trkpt_1=-9.99; trkpt_2=-9.99; trkpt_3=-9.99; trkpt_4=-9.99; trkpt_5=-9.99; trkpt_6=-9.99; trkpt_7=-9.99; trkpt_8=-9.99; trkpt_9=-9.99;
    trknpix_0=-999; trknpix_1=-999; trknpix_2=-999; trknpix_3=-999; trknpix_4=-999; trknpix_5=-999; trknpix_6=-999; trknpix_7=-999; trknpix_8=-999; trknpix_9=-999;
    trknsct_0=-999; trknsct_1=-999; trknsct_2=-999; trknsct_3=-999; trknsct_4=-999; trknsct_5=-999; trknsct_6=-999; trknsct_7=-999; trknsct_8=-999; trknsct_9=-999;

    matchedTrkPt1_0=-9.99; matchedTrkPt1_1=-9.99; matchedTrkPt1_2=-9.99; matchedTrkPt1_3=-9.99;
    matchedTrkPt2_0=-9.99; matchedTrkPt2_1=-9.99; matchedTrkPt2_2=-9.99; matchedTrkPt2_3=-9.99;
    matchedLep1_width=0.; matchedLep1_coreRes=-1.; matchedLep1_softRes=-1.; matchedLep1_dr=-1.0;
    matchedLep2_width=0.; matchedLep2_coreRes=-1.; matchedLep2_softRes=-1.; matchedLep2_dr=-1.0;
    
    //-----lepton variables------
    dPhill=100; mll=-1; transMass=-1; met=-1; 
    //mu_pt=-1; mu_eta=100;
    mu_isCombined=false; mu_isTight=false; mu_isTruthMatch=false;
    
    //ele_nsctHits=0; ele_npixHits=0;
    //ele_pt=-1; ele_eta=100;
    ele_isTruthMatch=false; ele_isTight=false; ele_isTightPP=false; ele_isMediumPP = false;
    
    //--- systematic variation
    doPileup = 0;
    doMuonTrigEfficiencySF=0; doMuonMSResolution=0; doMuonIDResolution=0; doMuonScale=0; doMuonEfficiencySF=0;
    doElecTrigEfficiencySF=0; doElecResolution=0; doElecIdentSF=0; doElecEnergyScale=0; doElecEfficiencySF=0;

    //---jet variables
    nJetsB4=-1; nJets=-1;
    jetEta1=-9999; jetPhi1=-9999; jetPt1=-1;
    
    //----vertex variables-----
    nVxp=0;

    //---jet variables
    nJetsB4=-1; nJets=-1;
    jetEta1=-9999; jetPhi1=-9999; jetPt1=-1;
    
    //-----MET variables-------
    MET_default=-1;
    MET_default_x=-1;
    MET_default_y=-1;
    
    nom_met_recalculated=-1;
    nom_metx_recalculated=-1;
    nom_mety_recalculated=-1;
    
    transMass_recalculated=-1;
}

// destructor
Global::~Global () {

    cout << "real time=" << timer.RealTime() << endl;
    timer.Stop();

}


// read ntuple list
void Global::getNtuples()
{


    string name="data.in";
    ifstream myfile;
    myfile.open(name.c_str(), ios::in);


    if (!myfile) {
      cerr << "Global::getNtuples(): Can't open input file:  " << name << endl;
      exit(1);
    } else {
        cout << "-> Read data file=" << name << endl;
      }

     string temp;
     while (myfile >> temp) {

 //the following line trims white space from the beginning of the string
           temp.erase(temp.begin(), std::find_if(temp.begin(), temp.end(), not1(ptr_fun<int, int>(isspace))));
            if (temp.find("#") == 0) continue; 
            ntup.push_back(temp);

     }
    cout << "-> Number of runs=" << ntup.size()  << endl;
    myfile.close();

    for (unsigned int i=0; i<ntup.size(); i++) {
           cout << ".. file to analyse="+ntup[i] << endl;
    }

}



// read initial parameters
void Global::getIni()
{

    std::string name="main.ini";
    ifstream myfile;
    myfile.open(name.c_str(), ios::in);

    if (!myfile) {
      std::cerr << "\nGlobal::getIni(): Can't open input file:  " << name << std::endl;
      exit(1);
    } else {
        std::cout << "\nRead file=" << name << std::endl;
    }

    // Read the configuration file line by line
    std::string line;
    while (getline(myfile, line)) {

        // skip commented lines
        if (line.find("#") == 0) continue;

        // skip very short line 
        int length = int(line.length());
        if (length < 2) continue;

        // read line
        std::istringstream ssline(line);
        std::string key = "";
        int value = 0;
        ssline >> key >> value;

        debug=false;  
        // read in the run configuration
        if (key.find("events") != std::string::npos) {
            maxEvents = value;
        } else if (key.find("debug") != std::string::npos) {
            if (value==1) debug = true;
        } else if (key.find("doPileup") != std::string::npos) {
            doPileup = value;
        } else if (key.find("doMuonTriggerEfficiencySF") != std::string::npos) {
            doMuonTrigEfficiencySF = value;
        } else if (key.find("doMuonMSResolution") != std::string::npos) {
            doMuonMSResolution = value;
        } else if (key.find("doMuonIDResolution") != std::string::npos) {
            doMuonIDResolution = value;
        } else if (key.find("doMuonScale") != std::string::npos) {
            doMuonScale = value;
        } else if (key.find("doMuonEfficiency") != std::string::npos) {
            doMuonEfficiencySF = value;
        } else if (key.find("doElecTriggerEfficiencySF") != std::string::npos) {
            doElecTrigEfficiencySF = value;
        } else if (key.find("doElecResolution") != std::string::npos) {
            doElecResolution = value;
        } else if (key.find("doElecIdentification") != std::string::npos) {
            doElecIdentSF = value;
        } else if (key.find("doElecEnergyScale") != std::string::npos) {
            doElecEnergyScale = value;
        } else if (key.find("doElecEfficiencySF") != std::string::npos) {
            doElecEfficiencySF = value;
        } else {
            std::cout << "Warning  Unknown configuration option: " << key << std::endl;
            std::cout << "Warning  Analysis code will ignore this option." << std::endl;
        }
    }

    //string message;
    //int    number;
    //myfile >> message >> maxEvents;
    //myfile >> message >> number;
    //myfile >> message >> systematics;
    //myfile >> message >> muonCorrSys;
    //debug=false;  
    //if (number==1) debug=true;
    myfile.close();

    ValidateRunConfig();  // validate the run configuration
    print_init();         // print initialisations

}

void Global::print_init() {
   std::cout << "\n\n  --Job input--: " << std::endl;
   std::cout << "\n   Max events=" << maxEvents <<
           "\n   Debug=" << debug <<  
           //"\n   Systematics=" << systematics << 
           "\n   doPileup=" << doPileup << 
           "\n   doMuonTrigEfficiencySF=" << doMuonTrigEfficiencySF << 
           "\n   doMuonMSResolution=" << doMuonMSResolution << 
           "\n   doMuonIDResolution=" << doMuonIDResolution << 
           "\n   doMuonScale=" << doMuonScale << 
           "\n   doMuonEfficiencySF=" << doMuonEfficiencySF << 
           "\n   doElecTrigEfficiencySF=" << doElecTrigEfficiencySF << 
           "\n   doElecResolution=" << doElecResolution << 
           "\n   doElecIdentS=" << doElecIdentSF << 
           "\n   doElecEnergyScale=" << doElecEnergyScale << 
           "\n   doElecEfficiencySF=" << doElecEfficiencySF << std::endl; 
}
 
// Validate the configuration options read from the file main.ini
// If value read is different form value expected, 
//   then set it to default. The run won't stop.
void Global::ValidateRunConfig() {
    // maximum events
    if (maxEvents < 0) {
        std::cout << "Warning  Total number of events from configuration file is negative. Set it to default: " << MAX_EVENTS << std::endl;
        maxEvents = MAX_EVENTS;
    }
    // debug flag
    if (debug != 0 && debug != 1) {
        std::cout << "Warning  Unknown debug flag. Set to off" << std::endl;
        debug = 0;
    }
    // pileup variation
    if (doPileup < -1 && doPileup > 1) {
        std::cout << "Warning  Got Pileup variation" << doPileup << ", expected -1, 0 or 1. Set to zero.\n";
        doPileup = 0;
    }
    // muon trigger efficiency scale factor variation
    if (doMuonTrigEfficiencySF < -1 && doMuonTrigEfficiencySF > 1) {
        std::cout << "Warning  Got muonTriggerEfficiencySF variation" << doMuonTrigEfficiencySF << ", expected -1, 0 or 1. Set to zero.\n";
        doMuonTrigEfficiencySF = 0;
    } 
    // muon MS resolution variation
    if (doMuonMSResolution < -1 && doMuonMSResolution > 1) {
        std::cout << "Warning  Got muonMSResolution variation" << doMuonMSResolution << ", expected -1, 0 or 1. Set to zero.\n";
        doMuonMSResolution = 0;
    }
    // muon ID resolution variation
    if (doMuonIDResolution < -1 && doMuonIDResolution > 1) {
        std::cout << "Warning  Got muonIDResolution variation" << doMuonIDResolution << ", expected -1, 0 or 1. Set to zero.\n";
        doMuonIDResolution = 0;
    }
    // muon MS resolution variation
    if (doMuonScale < -1 && doMuonScale > 1) {
        std::cout << "Warning  Got muonScale variation" << doMuonScale << ", expected -1, 0 or 1. Set to zero.\n";
        doMuonScale = 0;
    }
    // muon efficiency variation
    if (doMuonEfficiencySF < -1 && doMuonEfficiencySF > 1) {
        std::cout << "Warning  Got muonEfficiencySF variation" << doMuonEfficiencySF << ", expected -1, 0 or 1. Set to zero.\n";
        doMuonEfficiencySF = 0;
    }
    // electron trigger efficiency scale factor variation
    if (doElecTrigEfficiencySF < -1 && doElecTrigEfficiencySF > 1) {
        std::cout << "Warning  Got elecTriggerEfficiencySF variation" << doElecTrigEfficiencySF << ", expected -1, 0 or 1. Set to zero.\n";
        doElecTrigEfficiencySF = 0;
    } 
    // Electron resolution variation
    if (doElecResolution < -1 && doElecResolution > 1) {
        std::cout << "Warning  Got elecResolution variation" << doElecResolution << ", expected -1, 0 or 1. Set to zero.\n";
        doElecResolution = 0;
    }
    // Electron identification variation
    if (doElecIdentSF < -1 && doElecIdentSF > 1) {
        std::cout << "Warning  Got elecIdentSF variation" << doElecIdentSF << ", expected -1, 0 or 1. Set to zero.\n";
        doElecIdentSF = 0;
    }
    // Electron energy scale variation
    if (doElecEnergyScale < -1 && doElecEnergyScale > 1) {
        std::cout << "Warning  Got elecEnergyScale variation" << doElecEnergyScale << ", expected -1, 0 or 1. Set to zero.\n";
        doElecEnergyScale = 0;
    }
    // Electron Efficiency variation
    if (doElecEfficiencySF < -1 && doElecEfficiencySF > 1) {
        std::cout << "Warning  Got elecEfficiencySF variation" << doElecEfficiencySF << ", expected -1, 0 or 1. Set to zero.\n";
        doElecEfficiencySF = 0;
    }
}

// Sum event weights required for normalization
// Order of variables in this function should correspond 
//   to the bin index of the sum-of-weights histogram
int Global::SumMCEventWeight() {
    unWeightedEvent += 1;
    sumEventWeights_tot += mcEventWeight;
    sumEventWeights_tot_vtxpos += mcEventWeight * vertexPositionWeight;

    return 0;
}

// Reset sum-of-weights variables
int Global::ResetMCEventWeight() {
    unWeightedEvent = 0;
    sumEventWeights_tot = 0.;
    sumEventWeights_tot_vtxpos = 0.;

    return 0;
}

// Read the information from the cross-section file
// The format of the file is as below:
// name_of_dataset  dataset_id  xsection  k-factor  filter_efficicency  generator  process_info  total_number_events
//
// - Line begining with a '#' is a comment and should be skip
// - Empty line should also be skipt
void Global::readXsection()
{
   std::string xsfname="XsectionFile.txt";
   ifstream xsFile;
   xsFile.open(xsfname.c_str(), ios::in);
   if (!xsFile) {
     cerr << "\nGlobal::readXsection(): Can't open cross-section file:  " << xsfname << endl;
     exit(1);
   } else {
       cout << "\nRead cross-section file=" << xsfname << endl;
   }

   //clear the dataset container
   dsContainer.clear();

   //read line by line
   std::string line;
   while (getline(xsFile, line)) {

       if (line.find("#") == 0) continue;

       int length = int(line.length());
       if (length < 10) continue;

       std::istringstream ssline(line);
         //cout << ".. line="+line<< endl;
       std::string nametmp = "";
       std::string gentmp = "";
       std::string proctmp = "";
       std::string typtmp = "";
       int idtmp = 0;
       double xstmp = 1.;
       double kftmp = 1.;
       double eftmp = 1.;
       double netmp = 1.;
       ssline >> nametmp >> idtmp >> xstmp >> kftmp >> eftmp >> gentmp >> typtmp >> proctmp  >> netmp;
         //cout << ".. each="+nametmp << " " << idtmp << " " <<xstmp << " "<< kftmp << " " << eftmp << " " << netmp << endl;
       //the following line trims white space from the beginning of the string
       nametmp.erase(nametmp.begin(), std::find_if(nametmp.begin(), nametmp.end(), not1(ptr_fun<int, int>(isspace))));

       Dataset dataset;
       dataset.name = nametmp;
       dataset.dsID = idtmp;
       dataset.xSection = xstmp;
       dataset.kFactor = kftmp;
       dataset.filterEff = eftmp;
       dataset.generator = gentmp;
       dataset.dataType = typtmp;
       dataset.procInfo = proctmp;
       dataset.Nevents = netmp;

       //Add dataset to the container
       dsContainer.push_back(new Dataset(dataset));

   }

   //std::vector<Dataset*>::iterator itr;
   //for(itr = dsContainer.begin(); itr != dsContainer.end(); ++itr)
   //{
   //   Dataset *dstmp = *itr;
   //   std::cout << "xsf:"+dstmp->name << " " << dstmp->dsID << " " << dstmp->xSection << " "<< dstmp->kFactor << " " << dstmp->filterEff << " "
   //             << dstmp->generator << " " << dstmp->procInfo <<" " << dstmp->Nevents
   //             << std::endl;
   //}
}

