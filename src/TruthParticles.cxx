/*
 * RecoElectrons.cxx
 *
 *
 *
 *
 */

#include "Ana.h"
#include "Global.h"
#include "Histo.h"
#include "SystemOfUnits.h"
#include <iostream>
//#include <cstdlib>

#include<TROOT.h>
#include<TMath.h>
//#include<TClonesArray.h>
#include<TH1D.h>
#include<TH2F.h>
#include "LParticle.h"

#include<iostream>
#include<vector>
using namespace std;

extern Histo h;
extern Global glob;
const double kPI = TMath::Pi();

int Ana::TruthParticles(Long64_t entry) {

    // Empty the truth container
    glob.truthParticleObjects.clear();

    //------------------------------------------------------
    // Save relevant branch of truth particles
    for (int iTruth = 0; iTruth < (int)mc_n; ++iTruth) {

       // Skip if particle is not stable
       if (mc_status->at(iTruth) != 1) continue;

        // LParticle for iTruth particle
        LParticle lp;
        lp.SetCharge(mc_charge->at(iTruth));

        // truth particle TLorentzVector
        TLorentzVector truthPt_tlv;
        truthPt_tlv.SetPtEtaPhiM(mc_pt->at(iTruth), mc_eta->at(iTruth), mc_phi->at(iTruth), mc_m->at(iTruth));
        lp.SetP(truthPt_tlv);

        // Get the relevant branches
        lp.SetParameter( mc_pt->at(iTruth) );
        lp.SetParameter( mc_m->at(iTruth) );
        lp.SetParameter( mc_eta->at(iTruth) );
        lp.SetParameter( mc_phi->at(iTruth) );
        lp.SetParameter( mc_status->at(iTruth) );
        lp.SetParameter( mc_barcode->at(iTruth) );
        lp.SetParameter( mc_pdgId->at(iTruth) );
        lp.SetParameter( mc_charge->at(iTruth) );
        lp.SetParameter( mc_pt->at(iTruth) );

        // Store particle
        glob.truthParticleObjects.push_back(lp);
    }

    return 0;
}
