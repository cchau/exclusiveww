
#include "TriggerTools.h"
#include "Ana.h"

analysis ana;

TriggerTools::TriggerTools():
  fDebug     (false)
{}
  
TriggerTools::~TriggerTools(){}

//void TriggerTools::initialize()
//{
//  my_triggerNavigationVariables = new TriggerNavigationVariables();
//
//  std::cout << "TriggerTools::initialize - " << std::endl;
//  std::cout << " " << std::endl;
//  std::cout << "########## MuonTriggerMatching ########" << std::endl;
//  my_muonTriggerMatching = new MuonTriggerMatching(my_triggerNavigationVariables);
//  my_muonTriggerMatching->setDeltaR(0.15);
//  my_electronTriggerMatching = new ElectronTriggerMatching(my_triggerNavigationVariables);
//  my_electronTriggerMatching->setDeltaR(0.15);
//
//
//  std::cout << " " << std::endl;
//  std::cout << "########## LeptonTriggerSF ########" << std::endl;
//
//  my_leptonTriggerSF = new LeptonTriggerSF(2012, "./external", "muon_trigger_sf_2012_AtoL.p1328.root", "./lib_ext/ElectronEfficiencyCorrection/data", "rel17p2.v07");//rel17p2.v03
//}

//void TriggerTools::setTrigVars()
//{
//  my_triggerNavigationVariables->set_trig_DB_SMK(ana.trig_DB_SMK);
//	//cout<<"trig_DB_SMK == "<<my_triggerNavigationVariables.trig_DB_SMK<<endl;
//
//  my_triggerNavigationVariables->set_trig_Nav_n(ana.trig_Nav_n);
//  my_triggerNavigationVariables->set_trig_Nav_chain_ChainId(ana.trig_Nav_chain_ChainId);
//  my_triggerNavigationVariables->set_trig_Nav_chain_RoIType(ana.trig_Nav_chain_RoIType);
//  my_triggerNavigationVariables->set_trig_Nav_chain_RoIIndex(ana.trig_Nav_chain_RoIIndex);
//  // electron 
//  my_triggerNavigationVariables->set_trig_RoI_EF_e_egammaContainer_egamma_Electrons(ana.trig_RoI_EF_e_egammaContainer_egamma_Electrons);
//  my_triggerNavigationVariables->set_trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus(ana.trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus);
//  my_triggerNavigationVariables->set_trig_EF_el_n(ana.trig_EF_el_n);
//  my_triggerNavigationVariables->set_trig_EF_el_eta(ana.trig_EF_el_eta);
//  my_triggerNavigationVariables->set_trig_EF_el_phi(ana.trig_EF_el_phi);
//  // muon 
//  my_triggerNavigationVariables->set_trig_RoI_EF_mu_Muon_ROI(ana.trig_RoI_EF_mu_Muon_ROI);
//  my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFInfoContainer(ana.trig_RoI_EF_mu_TrigMuonEFInfoContainer);
//  my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus(ana.trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus);
//  my_triggerNavigationVariables->set_trig_RoI_L2_mu_CombinedMuonFeature(ana.trig_RoI_L2_mu_CombinedMuonFeature);
//  my_triggerNavigationVariables->set_trig_RoI_L2_mu_CombinedMuonFeatureStatus(ana.trig_RoI_L2_mu_CombinedMuonFeatureStatus);
//  my_triggerNavigationVariables->set_trig_RoI_L2_mu_MuonFeature(ana.trig_RoI_L2_mu_MuonFeature);
//  my_triggerNavigationVariables->set_trig_RoI_L2_mu_Muon_ROI(ana.trig_RoI_L2_mu_Muon_ROI);
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_CB_pt(ana.trig_EF_trigmuonef_track_CB_pt);
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_CB_eta(ana.trig_EF_trigmuonef_track_CB_eta);
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_CB_phi(ana.trig_EF_trigmuonef_track_CB_phi);
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_SA_pt(ana.trig_EF_trigmuonef_track_SA_pt);
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_SA_eta(ana.trig_EF_trigmuonef_track_SA_eta);
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_SA_phi(ana.trig_EF_trigmuonef_track_SA_phi);
//  my_triggerNavigationVariables->set_trig_EF_trigmugirl_track_CB_pt(ana.trig_EF_trigmugirl_track_CB_pt);
//  my_triggerNavigationVariables->set_trig_EF_trigmugirl_track_CB_eta(ana.trig_EF_trigmugirl_track_CB_eta);
//  my_triggerNavigationVariables->set_trig_EF_trigmugirl_track_CB_phi(ana.trig_EF_trigmugirl_track_CB_phi);
//  my_triggerNavigationVariables->set_trig_L2_combmuonfeature_eta(ana.trig_L2_combmuonfeature_eta);
//  my_triggerNavigationVariables->set_trig_L2_combmuonfeature_phi(ana.trig_L2_combmuonfeature_phi);
//  my_triggerNavigationVariables->set_trig_L2_muonfeature_eta(ana.trig_L2_muonfeature_eta);
//  my_triggerNavigationVariables->set_trig_L2_muonfeature_phi(ana.trig_L2_muonfeature_phi);
//  my_triggerNavigationVariables->set_trig_L1_mu_eta(ana.trig_L1_mu_eta);
//  my_triggerNavigationVariables->set_trig_L1_mu_phi(ana.trig_L1_mu_phi);
//  my_triggerNavigationVariables->set_trig_L1_mu_thrName(ana.trig_L1_mu_thrName);
//  my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFIsolationContainer(ana.trig_RoI_EF_mu_TrigMuonEFIsolationContainer); // for 2012 isolated trigger
//  my_triggerNavigationVariables->set_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus(ana.trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus); // for 2012 isolated trigger
//  //my_triggerNavigationVariables->set_trig_EF_trigmuonef_EF_mu15(ana.trig_EF_trigmuonef_EF_mu15);  // for ntuple made with TriggerMenuAnalysis-00-02-86
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_EF_mu24i_tight(ana.trig_EF_trigmuonef_EF_mu24i_tight);  // for ntuple made with TriggerMenuAnalysis-00-02-86
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_EF_mu36_tight(ana.trig_EF_trigmuonef_EF_mu36_tight); // for ntuple made with TriggerMenuAnalysis-00-02-86
//  my_triggerNavigationVariables->set_trig_EF_trigmuonef_track_MuonType(ana.trig_EF_trigmuonef_track_MuonType);  // for full scan trigger matching 
//
//  
//  if (not my_triggerNavigationVariables->isValid()) {
//    std::cerr << "VARIABLES NOT CORRECTLY SET\n";
//  }  
//}

bool TriggerTools::trigmatch_di_muon(const TLorentzVector& muon1,
                                       const TLorentzVector& muon2,
                                       const std::string& chain, 
                                       std::pair<bool, bool>& result1,
                                       std::pair<bool, bool>& result2){
  bool isMatched = false;

  double pt_thre1=0.;
  double pt_thre2=0.;
  if(chain=="EF_mu18_tight_mu8_EFFS"){
    pt_thre1 = 20000.;
    pt_thre2 = 10000.;
  }
  if(chain=="EF_2mu13"){
    pt_thre1 = 15000.;
    pt_thre2 = 15000.;
  }

  if((muon1.Pt() < pt_thre1) || (muon2.Pt() < pt_thre2))return false;

  bool isValid2mu = my_muonTriggerMatching->matchDimuon(muon1,muon2,chain,result1,result2);
  
  if(chain=="EF_mu18_tight_mu8_EFFS"){
    if(!isValid2mu){
      std::cout<< chain << "is not supported"<<std::endl;
    } else {
      isMatched = ( (result1.first && result2.second) || (result2.first && result1.second) );
      // result1.first: seeded trigger matching with muon1, result1.second: EFFS trigger matching with muon1 
      // result2.first: seeded trigger matching with muon2, result2.second: EFFS trigger matching with muon2 
    }
  } else {
    if(!isValid2mu){
      std::cout<< chain << "is not supported"<<std::endl;
    } else {
      isMatched = (result1.first && result2.first);
    }
  }
  
  return isMatched;

}

bool TriggerTools::trigmatch_di_electron(const TLorentzVector& electron1,
                                         const TLorentzVector& electron2,
                                         const std::string& chain,
                                         std::pair<bool, bool>& result1,
                                         std::pair<bool, bool>& result2) {
  bool isMatched = false;

  double pt_thre1=0.;
  double pt_thre2=0.;
  if(chain=="EF_2e12Tvh_loose1"){
    pt_thre1 = 14000.;
    pt_thre2 = 14000.;
  } else {
    std::cout << "Warning: expected EF_2e12Tvh_loose1, but got " << chain << std::endl;
    return false;
  }

  if((electron1.Pt() < pt_thre1) || (electron2.Pt() < pt_thre2))return false;

  bool isValid2Ele = my_electronTriggerMatching->matchDielectron(electron1, electron2, chain, result1, result2);
  if (!isValid2Ele) {
    std::cout<< chain << "is not supported"<<std::endl;
  } else {
    isMatched = (result1.first && result2.first);
  }

  return isMatched;
}

std::pair<double, double>
TriggerTools::trigsf_muons(int runnumber, 
			       bool useGeV, 
			       std::vector<TLorentzVector> muons, 
			       muon_quality q, 
			       int var=0,
			       std::string dilep_trigger_name=""){

  std::pair<double, double> sf;
  if(dilep_trigger_name=="EF_mu18_tight_mu8_EFFS"){
    std::vector<muon_quality> m_qual(muons.size(),q); 
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, m_qual, dilep_trigger_name, var);
	}

  return sf;
}

/*
std::pair<double, double>
TriggerTools::trigsf_leptons(int runnumber, 
			       bool useGeV, 
			       std::vector<TLorentzVector> muons, 
			       muon_quality q, 
			       std::vector<TLorentzVector> electrons, 
			       electron_quality p,
			       int var=0,
			       std::string dilep_trigger_name="",
			       const bool only_pass_dilep = false){

  std::pair<double, double> sf;

  if(dilep_trigger_name==""){
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, q, electrons, p, var);
  }else if(dilep_trigger_name=="mu24i_tight_or_mu36_tight_or_mu18_tight_mu8_EFFS"){
    std::vector<muon_quality> m_qual(muons.size(),q); 
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, m_qual, dilep_trigger_name, var);
  }else if(dilep_trigger_name=="e24vhi_medium1_or_e60_medium1_or_2e12Tvh_loose1"){
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, electrons, p, dilep_trigger_name, var);
  }else if(dilep_trigger_name=="EF_e12Tvh_medium1_mu8"){
    // If only the dilepton trigger fires, then 1+/-0.02 is used. This will be updated once the electron efficiency for e12Tvh_medium1 is available
    if(!only_pass_dilep) sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, q, electrons, p, var);
    //else { sf.first = 1.0; sf.second = 0.02; }
    else { sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, q, electrons, p, dilep_trigger_name, var); 
      //std::cout << "emu trigger: " << sf.first << " +/- " << sf.second << std::endl;
    }
  }

  return sf;
}*/

std::pair<double, double>
TriggerTools::trigsf_leptons(int runnumber, 
			       bool useGeV, 
			       std::vector<TLorentzVector> muons, 
			       muon_quality q, 
			       std::vector<TLorentzVector> electrons, 
			       electron_quality p,
			       int var=0,
			       std::string dilep_trigger_name="",
			       const bool only_pass_dilep = false){

  std::pair<double, double> sf;

  if(dilep_trigger_name==""){
    std::vector<muon_quality> m_qual(muons.size(),q); 
    std::vector<electron_quality> vec_electron_quality(electrons.size(),p); 
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, m_qual, electrons, vec_electron_quality, var);
  }else if(dilep_trigger_name=="mu24i_tight_or_mu36_tight_or_mu18_tight_mu8_EFFS"){
    std::vector<muon_quality> m_qual(muons.size(),q); 
    std::vector<electron_quality> vec_electron_quality(electrons.size(),p); 
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, m_qual, dilep_trigger_name, var);
  }else if(dilep_trigger_name=="e24vhi_medium1_or_e60_medium1_or_2e12Tvh_loose1"){
    std::vector<electron_quality> vec_electron_quality(electrons.size(),p); 
    sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, electrons, vec_electron_quality, dilep_trigger_name, var);
  }else if(dilep_trigger_name=="EF_e12Tvh_medium1_mu8"){
    // If only the dilepton trigger fires, then 1+/-0.02 is used. This will be updated once the electron efficiency for e12Tvh_medium1 is available
    std::vector<electron_quality> vec_electron_quality(electrons.size(),p); 
    std::vector<muon_quality> m_qual(muons.size(),q); 
    if(!only_pass_dilep) sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, m_qual, electrons, vec_electron_quality, var);
    //else { sf.first = 1.0; sf.second = 0.02; }
    else { sf = my_leptonTriggerSF->GetTriggerSF(runnumber, useGeV, muons, m_qual, electrons, vec_electron_quality, dilep_trigger_name, var); 
      //std::cout << "emu trigger: " << sf.first << " +/- " << sf.second << std::endl;
    }
  }

  return sf;
}
