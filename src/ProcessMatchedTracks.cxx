
#include "Ana.h"
#include "Global.h"
#include "Histo.h"
#include "SystemOfUnits.h"
#include <iostream>
//#include <cstdlib>

#include<TROOT.h>
#include<TMath.h>
#include<TH1D.h>
#include<TH2F.h>
#include "LParticle.h"

#include<iostream>
#include<vector>

extern Histo h;
extern Global glob;
const double kPI = TMath::Pi();

// compare function for vector of LParticles, sorting with pT
bool my_compare_lp( LParticle lpa, LParticle lpb){
    return (lpa.GetP()).Pt() > (lpb.GetP()).Pt();
}

int Ana:: ProcessMatchedTracks(std::vector<LParticle> &track_lp, TLorentzVector &lepton_tlv, std::vector<double> &output) {

    std::vector<LParticle> trkObject = track_lp;
    int nObj = (int)trkObject.size();

    // sorting LParticle with descending pT
    std::sort(trkObject.begin(), trkObject.end(), my_compare_lp);

    double trkPt = 0.; 
    TLorentzVector leadingTrk;       // the leading track of those matched to lepton
    //TLorentzVector leadingTrk2;       //    ... to lepton2 
    TLorentzVector trk_sumTlv;// trk2_tlv; // LorentzVector sum of tracks except the leading
    TLorentzVector tmp_tlv;

    // 1st element
    std::vector<Double32_t> trkPar = trkObject.at(0).GetParameters();
    trkPt = trkPar.at(TRK_pt);
    leadingTrk.SetPtEtaPhiM(trkPt, trkPar.at(TRK_eta), trkPar.at(TRK_phi_wrtBL), 0.);

    // Loop from the 2nd LParticle element
    if (nObj > 1) {
        for (int i = 1; i < nObj; ++i) {
           // Get parameters from LParticel track 
           std::vector<Double32_t> trkPar = trkObject.at(i).GetParameters();

            // Summing track vector
            tmp_tlv.SetPtEtaPhiM(trkPt, trkPar.at(TRK_eta), trkPar.at(TRK_phi_wrtBL), 0.);
            trk_sumTlv = trk_sumTlv + tmp_tlv;
        }
    }

    double ratio_pt = (nObj > 1)? trk_sumTlv.Pt() / leadingTrk.Pt(): 0.;
    double dif_leadingTrk = (leadingTrk.Pt() - lepton_tlv.Pt()) / lepton_tlv.Pt();
    double dif_otherTrk = (nObj > 1)? (trk_sumTlv.Pt() - lepton_tlv.Pt()) / lepton_tlv.Pt(): -1.0;
    double deltaR = leadingTrk.DeltaR(lepton_tlv);

    // values return by the function
    output.push_back(ratio_pt);
    output.push_back(dif_leadingTrk);
    output.push_back(dif_otherTrk);
    output.push_back(deltaR);

    return 0;
}
