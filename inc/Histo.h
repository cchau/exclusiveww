#ifndef HISTO_H
#define HISTO_H
/***************************************************************************
 *            histo.h
 *
 *  Fri Sep 21 16:08:23 2007
 *  Copyright  2007  chekanov
 *  chekanov@mail.desy.de
 ****************************************************************************/
using namespace std;
#include<iostream>
#include "TROOT.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TTree.h"
#include "TProfile.h"
#include "TString.h"

class Histo {

  public:

// for ntuple production
    TTree * m_ntuple;
    TClonesArray *arr0;
    TClonesArray *arr1;
    TClonesArray *arr2;
    TClonesArray *arr3;
    TClonesArray *arr4;
    TClonesArray *arr5;
    TClonesArray *arr6;
    TClonesArray *arr7;
    TClonesArray *arr8;

    //TTree *vars, *ee, *mumu, *emu, *mue, *el, *mu, *trac;    
    TTree *dilep, *el, *mu, *trac;

    // temporary TO BE REMOVED
    TH1D *nElOverlap;    

    // sum of weights
    TH1D *sumWeights;

    // Number of events at different stages of filtering
    TH1D *elecSelection;
    TH1D *muonSelection;
    TH1D *trkPixel;
    TH1D *trkSCT;
    TH1D *trkHoles;
    TH1D *trkpt;
    TH1D *trketa;
    TH1D *trkphi;
    TH1D *trkptPix1SCT4;
    TH1D *trketaPix1SCT4;
    TH1D *trkphiPix1SCT4;
    TH1D *trkpt_badQual;
    TH1D *trketa_badQual;
    TH1D *trkphi_badQual;
    TH1D *trk_dz0BL2PV;
    TH1D *lep_dz0BL2PV;
    TH1D *cutFlow;
    TH1D *cutFlow_weighted;

    // number of good leptons
    TH1D *nLepton;

// ---- do not modify below ----- 
    Histo();
    virtual ~Histo();
    void setHistograms();
    void setOutput();
    void finalize();
    char *ffile;
    TFile *RootFile;
    TH1D *debug;
    TH1D *select;

}; 
#endif /* HISTO_H*/
