#ifndef __METTOOLS_H
#define __METTOOLS_H


#include <TH1F.h>
#include <TH1D.h>
#include <TString.h>
#include "TLorentzVector.h"
#include "TMath.h"
#include "TRandom3.h"
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <map>
#include <vector>
#include <fstream>
#include <set>
#include <unistd.h>
#include <iomanip>
#include <utility>

#include "MissingETUtility/METUtility.h"

#include "MuonTools.h"


class METTools
{ 
 public:
  
  METTools();
  ~METTools();
  
  METUtility *m_util;	

  void initialize();
		void MET_Recalculation(vector<vector<float>* > &electrons,
													 vector<vector<vector<float> >* > &electrons_extra,
													 vector<vector<unsigned int> > *electron_sw,
													 vector<vector<float>* > &muons,
													 vector<vector<vector<float> >* > &muons_extra,
													 vector<vector<unsigned int> > *muon_sw,
													 vector<vector<float>* > &jets,
													 vector<vector<vector<float> >* > &jets_extra,
													 vector<vector<unsigned int> > *jet_sw,
													 vector<float> met,
                           float &met_et, float &met_x, float &met_y 
													);


 private:

  // Properties:
  bool   fDebug;

};

#endif

