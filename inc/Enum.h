#ifndef ENUM_H
#define ENUM_H

////////////////////////////////////////////////////////////
//
//  Enumeration name space
//    ELEC:     reco electrons
//    MUON:     reco muons
//    TRACK:    reco tracks
//    TRUTH:    truth particle
//    TRUTHTRK: truth tracks
//
////////////////////////////////////////////////////////////


/***********************************************************
 * Enum name space fro truth particles
 *
 *   Index: 100-199: electrons
 *          200-299: muons
 *
 **********************************************************/
enum TRUTH {
    MC_pt,
    MC_m,
    MC_eta,
    MC_phi,
    MC_status,
    MC_barcode,
    MC_pdgId,
    MC_charge
};


/***********************************************************
 * Truth::lepton
 *   Store truth variables from el_ container for electrons
 *   or mu_staco_ for muons
 *
 **********************************************************/
enum LEPTRUTH {
    LEP_truth_E,
    LEP_truth_pt,
    LEP_truth_eta,
    LEP_truth_phi,
    LEP_truth_type,
    LEP_truth_status,
    LEP_truth_barcode,
    LEP_truth_motherbarcode,
    LEP_truth_matched,
    LEP_truth_index,
    LEP_truth_hasHardBrem,
    LEP_type,
    LEP_origin
};


/***********************************************************
 * ELEC::electrons
 *
 *   Parameters index: 
 *          start: variables directly from d3pd
 *          end-of-vector: variables with correction applied
 *
 **********************************************************/
enum ELEC {
    EL_E,
    EL_Et,
    EL_pt,
    EL_m,
    EL_eta,
    EL_phi,
    EL_charge,
    EL_author,
    EL_OQ,
    EL_mediumPP,
    EL_veryTightLH,
    EL_tightPP,
    EL_Etcone20,
    EL_Etcone30,
    EL_Etcone40,
    EL_ptcone20,
    EL_ptcone30,
    EL_ptcone40,
    EL_cl_E,
    EL_cl_pt,
    EL_cl_eta,
    EL_cl_phi,
    EL_trackd0,
    EL_trackz0,
    EL_trackd0pv,
    EL_trackz0pv,
    EL_trackd0beam,
    EL_trackz0beam,
    EL_trackd0pvunbiased,
    EL_trackz0pvunbiased,
    EL_tracksigd0pvunbiased,
    EL_trackpt,
    EL_trackphi,
    EL_tracketa,
    EL_tracktheta,
    EL_trackqoverp,
    EL_nPixHits,
    EL_nSCTHits,
    EL_nPixelDeadSensors,
    EL_nSCTDeadSensors,
    EL_nPixHoles,
    EL_nSCTHoles,
    EL_unCor_Et,
    EL_cor_alpha,
    EL_cor_alphaZeeAllUp,
    EL_cor_alphaZeeAllDown,
    EL_cor_alphaAllUp,
    EL_cor_alphaAllDown,
    EL_cor_smearSF,
    EL_cor_smearSFErrUp,
    EL_cor_smearSFErrDown,
    EL_cor_AFtoG4,
    EL_cor_Et,
    EL_recoSF,
    EL_idSF_medium,
    EL_idSF_tight,
    EL_idSF_vTightLH,
    EL_recoSF_sys,
    EL_idSF_medium_sys,
    EL_idSF_tight_sys,
    EL_idSF_vTightLH_sys,
    EL_cor_Etcone20,
    EL_cor_Etcone30,
    EL_cor_Etcone40,
    EL_isoSF,
    EL_isoSF_sys
};


/***********************************************************
 * MUON::muons
 *
 *   Parameters index: 
 *          start: variables directly from d3pd
 *          end-of-vector: variables with correction applied
 *
 **********************************************************/
enum MUON {
    MU_E,
    MU_pt,
    MU_m,
    MU_eta,
    MU_phi,
    MU_charge,
    MU_author,
    MU_etcone20,
    MU_etcone30,
    MU_etcone40,
    MU_ptcone20,
    MU_ptcone30,
    MU_ptcone40,
    MU_isCombinedMuon,
    MU_tight,
    MU_nPixHits,
    MU_nSCTHits,
    MU_nPixelDeadSensors,
    MU_nSCTDeadSensors,
    MU_nPixHoles,
    MU_nSCTHoles,
    MU_trackd0,
    MU_trackz0,
    MU_trackd0pv,
    MU_trackz0pv,
    MU_trackd0beam,
    MU_trackz0beam,
    MU_trackd0pvunbiased,
    MU_trackz0pvunbiased,
    MU_tracksigd0pvunbiased,
    MU_trackpt,
    MU_trackphi,
    MU_tracketa,
    MU_tracktheta,
    MU_trackqoverp,
    MU_idSF,
    MU_idSF_statError,
    MU_idSF_sysError,
    MU_isolationSF,
    MU_isolationSF_sys,
    MU_cor_pt,
    MU_cor_etcone20,
    MU_cor_etcone30,
    MU_cor_etcone40,
    MU_cor_ptcone30,
    MU_id_theta,
    MU_id_phi,
    MU_id_qoverp
};

/***********************************************************
 * TRACK::track
 *
 *
 **********************************************************/
enum TRACK {
    TRK_match,
    TRK_dz0TrkLep,
    TRK_pt,
    TRK_eta,
    TRK_phi_wrtBL,
    TRK_d0_wrtBL,
    TRK_z0_wrtBL,
    TRK_d0_wrtPV,
    TRK_z0_wrtPV,
    TRK_phi_wrtPV,
    TRK_theta_wrtPV,
    TRK_qoverp_wrtPV,
    TRK_nPixHits,
    TRK_nSCTHits,
    TRK_nDeadSensors,
    TRK_nHoles
};

#endif // ENUM_H
