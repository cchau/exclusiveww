#ifndef GLOBAL_H
#define GLOBAL_H
/***************************************************************************
 *            global.h
 *
 ****************************************************************************/




using namespace std;
#include<iostream>

// S.Chekanov (ANL)

#include "TFile.h"
#include "TH1D.h"
#include "TStopwatch.h"
#include <string>
#include <vector>
#include <map>
#include "GoodRunsLists/TGoodRunsList.h"
#include "GoodRunsLists/TGoodRunsListReader.h"
// #include "TrigDecisionToolD3PD.h"
#include "PileupReweighting/TPileupReweighting.h"
#include "ElectronEfficiencyCorrection/TElectronEfficiencyCorrectionTool.h"
#include "egammaAnalysisUtils/EnergyRescalerUpgrade.h"
#include "egammaAnalysisUtils/VertexPositionReweightingTool.h"
#include "ElectronPhotonSelectorTools/TElectronLikelihoodTool.h"
#include "MuonIsolationCorrection/CorrectCaloIso.h"
#include "egammaAnalysisUtils/CaloIsoCorrection.h"
#include "MuonMomentumCorrections/SmearingClass.h"
#include "MuonEfficiencyCorrections/AnalysisMuonConfigurableScaleFactors.h"
#include "LParticle.h"
#include "IsolationScaleFactors/ElecIsolationSF.h"
#include "IsolationScaleFactors/MuonIsolationSF.h"


// type of simulation
#define FULLSIM 1
#define ATFAST2 3

// default parameters
const int MAX_EVENTS = 10000000;

// Dataset structure
struct Dataset{
   std::string name;
   int dsID;
   double xSection;
   double kFactor;
   double filterEff;
   std::string generator;
   std::string procInfo;
   std::string dataType;
   double Nevents;
};

class Global {

  public:

    Global ();
    virtual ~Global ();

    int TotalEvents;
    void getNtuples();
    void getIni();
    void initializeVar();
    void getLumi();
    int SumMCEventWeight();
    int ResetMCEventWeight();
    void ValidateRunConfig();
    void print_init();
    int nev;			// events processed 
    vector <string> ntup;	// ntuple list
    map <int,bool>   goodruns; 
    bool jfound;
    double mc_event_weight;    //generator weight
    double mcEventWeight;      //per event product of weights (such electrons and muon scale factors)

    // sum of weights over all events in a given dataset container
    // If a dataset has more than one container, 
    // then merging the output files after running this code
    // is required to get the dataset's total sum of weights
    int unWeightedEvent;
    double sumEventWeights_tot;
    double sumEventWeights_tot_vtxpos;

    // method to store dataset information
    std::vector<Dataset*> dsContainer;
    void readXsection();

    //info on the dataset being processed
    int isGoodMC; //good MC=1 if dataset found in cross-section file
    int isAFII; // value = 1 if sample is generated with AFII, 0 otherwise
    int dsID;
    double xSection;
    double kFactor;
    double filterEff;
    double Nevents;

    bool doTruthMatching;

    // good run
    double averageIntPerXing;
    Root::TGoodRunsList grl;
    Root::TGoodRunsListReader grlReader;
    //
    // pile up reweighting
    Root::TPileupReweighting *my_PileUpReweighting;
    //Electron reco and ident efficiencies
    Root::TElectronEfficiencyCorrectionTool tool_el_reco_SF;
    Root::TElectronEfficiencyCorrectionTool tool_el_medium_SF;
    Root::TElectronEfficiencyCorrectionTool tool_el_tight_SF;
    Root::TElectronEfficiencyCorrectionTool tool_el_vTightLLH_SF;
    //Electron energy scale and smearing
    egRescaler::EnergyRescalerUpgrade ers;
    // Electron likelihood
    Root::TElectronLikelihoodTool* electronLLHTool;
    //
    // Muon smearing and momentum
    MuonSmear::SmearingClass *mcp_smear;	
    Analysis::AnalysisMuonConfigurableScaleFactors *muonSF;
    // Muon isolation correction
    CorrectCaloIso *muonIsoTool;
    //
    // vertex position reweighting tool
    VertexPositionReweightingTool *zvtx_tool;
    //
    // Isolation scale factors
    ElecIsolationSF *tool_elecIsoSF;
    MuonIsolationSF *tool_muonIsoSF;

    // Job configuration
    TFile *RootFile;
    TH1D *h1;
    TStopwatch timer;
    //char *ffile;
    int maxEvents;
    int debug;
    int systematics;

    // type of simulation: Full simulation = 1, ATFAST2 = 3
    unsigned int dataType;

    //double minPTall;
    //double maxEta;

    // Containers for selected electrons, muons or tracks, ...
    std::vector<LParticle> recoElectronObjects;
    std::vector<LParticle> recoMuonObjects;
    std::vector<LParticle> truthParticleObjects;
    std::vector<LParticle> truthElectronObjects;
    std::vector<LParticle> truthMuonObjects;
    std::vector<LParticle> recoMatchedTrackObjects;
    std::vector<LParticle> recoUnmatchedTrackObjects;

    //----Lepton variables-----
    Int_t lepPDGID1, lepPDGID2;
	UInt_t nLep, nMuAll, nMu, nEleAll, nEle, runNumber, eventNumber;
	Double_t lepPt1, lepPt2, lepEta1, lepEta2, lepPhi1, lepPhi2, lepM1, lepM2, lepCharge1, lepCharge2;
    Int_t lepIsMediumPP1, lepIsTight1, lepIsTightPP1, lepIsMediumPPIso1, lepIsTightPPIso1, lepIsCombined1;
    Int_t lepIsMediumPP2, lepIsTight2, lepIsTightPP2, lepIsMediumPPIso2, lepIsTightPPIso2, lepIsCombined2;
    Int_t lepIsVeryTightLH1, lepIsVeryTightLH2;
    Double_t lepEtcone20_1, lepEtcone30_1, lepEtcone40_1, lepPtcone20_1, lepPtcone30_1, lepPtcone40_1;
    Double_t lepEtcone20_2, lepEtcone30_2, lepEtcone40_2, lepPtcone20_2, lepPtcone30_2, lepPtcone40_2;
    Double_t lepTrackPt1, lepTrackPt2, lepTrackEta1, lepTrackEta2, lepTrackPhi1, lepTrackPhi2, lepTrackqoverp1, lepTrackqoverp2;
    Double_t lepTrackTheta1, lepTrackTheta2, lepTrackz0pvunbiased1, lepTrackz0pvunbiased2, lepTrackd0pvunbiased1, lepTrackd0pvunbiased2, lepTracksigd0pvunbiased1, lepTracksigd0pvunbiased2;
    Double_t lepTrackz0pv1, lepTrackd0pv1, lepTrackz0pv2, lepTrackd0pv2;
    Double_t lepTrackz0beam1, lepTrackz0beam2, lepTrackd0beam1, lepTrackd0beam2, absD0ll;
    Int_t lepnPixHits1, lepnPixHits2, lepnSCTHits1, lepnSCTHits2, lepnPixHoles1, lepnPixHoles2, lepnSCTHoles1, lepnSCTHoles2, lepnPixDeadSensors1, lepnPixDeadSensors2, lepnSCTDeadSensors1, lepnSCTDeadSensors2;
    Bool_t lepTrigMatched1, lepTrigMatched2;
    Bool_t dilepTrigMatched;
    //Bool_t trigMatched;

    // lepton correction
    Double_t lepPt1_uncorrected, lepPt2_uncorrected;
    //Double_t lepEnergyScale1, lepEnergyScale2, lepPtSmearing1, lepPtSmearing2;
    Int_t doPileup;
    Int_t doMuonTrigEfficiencySF, doMuonMSResolution, doMuonIDResolution, doMuonScale, doMuonEfficiencySF;
    Int_t doElecTrigEfficiencySF, doElecResolution, doElecIdentSF, doElecEnergyScale, doElecEfficiencySF;

    Int_t lepNMatchedTrk1, lepNMatchedTrk2;
    Double_t dilepdz0, dilepAverageZ0;

    //----General event
    Int_t lbn;
    Int_t bcid; 
    UInt_t channel;
    UInt_t larError, tileError;
    Bool_t EF_e24vhi_medium1, EF_e60_medium1;
    Bool_t EF_mu24i_tight, EF_mu36_tight; 
    Bool_t EF_mu18_tight_mu8_EFFS, EF_2e12Tvh_loose1;
    Bool_t EF_e12Tvh_medium1_mu8;
    
    std::string muon_trigger_chain;
    std::string electron_trigger_chain;
    std::string electronMuon_trigger_chain;

    //----Event scale factors
    Double_t trigSF_single_eTight;
    Double_t trigSF_single_eMedium;
    Double_t trigSF_single_eVTightLH;
    Double_t lepIDSFWeight1, lepIDSFWeight2, lepIDSFWeight1_sys, lepIDSFWeight2_sys;
    Double_t lepRecoSFWeight1, lepRecoSFWeight2, lepRecoSFWeight1_sys, lepRecoSFWeight2_sys;
    Double_t lepIsolationSF1, lepIsolationSF2, lepIsolationSF1_sys, lepIsolationSF2_sys;

	//---track variables---
	Int_t nTrkUnmatched, nTrkUnmatched_100mm, nTrkUnmatched_125mm, nTrkUnmatched_150mm, nTrkUnmatched_175mm;
    Int_t nTrkUnmatched_200mm, nTrkUnmatched_225mm, nTrkUnmatched_250mm, nTrkUnmatched_275mm, nTrkUnmatched_300mm;
    Int_t nTracksAll, nTrkPix1SCT4;
	Double_t trkdz0_0, trkdz0_1, trkdz0_2, trkdz0_3, trkdz0_4, trkdz0_5, trkdz0_6, trkdz0_7, trkdz0_8, trkdz0_9; 
    Double_t trkpt_0, trkpt_1, trkpt_2, trkpt_3, trkpt_4, trkpt_5, trkpt_6, trkpt_7, trkpt_8, trkpt_9;
    int trknpix_0, trknpix_1, trknpix_2, trknpix_3, trknpix_4, trknpix_5, trknpix_6, trknpix_7, trknpix_8, trknpix_9;
    int trknsct_0, trknsct_1, trknsct_2, trknsct_3, trknsct_4, trknsct_5, trknsct_6, trknsct_7, trknsct_8, trknsct_9;

    //matched tracks
    double matchedTrkPt1_0, matchedTrkPt1_1, matchedTrkPt1_2, matchedTrkPt1_3;
    double matchedTrkPt2_0, matchedTrkPt2_1, matchedTrkPt2_2, matchedTrkPt2_3;
    double matchedLep1_width, matchedLep1_coreRes, matchedLep1_softRes, matchedLep1_dr;
    double matchedLep2_width, matchedLep2_coreRes, matchedLep2_softRes, matchedLep2_dr;

	double dPhill, pTll, mll, met, transMass, trdZ0, trAvZ0; 
	//----electron variables--------
	double ele_pt, ele_eta;
	bool ele_isTight, ele_isTightPP, ele_isMediumPP;
    bool ele_isTruthMatch;

	//----mu variables--------
	bool mu_isCombined, mu_isTight, mu_isTruthMatch;
	
	//--Even per event weight
	double lepTrigSFEventWeight, lepTrigSFEventWeightUp, lepTrigSFEventWeightDown;
    double pileUpEventWeight; 
    double vertexPositionWeight;
	int nTruth;

	//--jet variables
	int nJets, nJetsB4;
	double jetEta1, jetPhi1, jetPt1;


	//---vertex variables---
	int nVxp;
    int npv_trk2;
    int npv_trk3;
    int vtx_index;
    int vtx_nTracks;
    int vtx_nTracksPV;
    double dvtx;
    double dvtxPV;
    int vtxpv_index;
    int vtxpv_nTracks;
    int vtxpv_nTracksPV;
    double dvtxpv;
    double dvtxpvPV;

    int m_run, m_evt;
    int LumiBlockNumber;
    int BCIDbeamCrossId;


	//----MET variables------
	float MET_default, MET_default_x, MET_default_y, nom_met_recalculated, nom_metx_recalculated, nom_mety_recalculated;
	float transMass_recalculated; 
 
    // D3PD::TrigDecisionToolD3PD m_tdt;
 


};
#endif /* GLOBAL_H*/
