#ifndef __TRIGGERTOOLS_H
#define __TRIGGERTOOLS_H

#include "TrigMuonEfficiency/MuonTriggerMatching.h"
#include "TrigMuonEfficiency/ElectronTriggerMatching.h"
#include "TrigMuonEfficiency/TriggerNavigationVariables.h"
#include "TrigMuonEfficiency/LeptonTriggerSF.h"  

#include <TH1F.h>
#include <TH1D.h>
#include <TString.h>
#include "TLorentzVector.h"
#include "TMath.h"
#include "TRandom3.h"
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <map>
#include <vector>
#include <fstream>
#include <set>
#include <unistd.h>
#include <iomanip>
#include <utility>

class TriggerTools
{ 
 public:
  
  TriggerTools();
  ~TriggerTools();
  
  //void initialize();

  bool trigmatch_di_muon(const TLorentzVector& muon1,
                         const TLorentzVector& muon2,
                         const std::string& chain,
                         std::pair<bool, bool>& result1,
                         std::pair<bool, bool>& result2);
 
  bool trigmatch_di_electron(const TLorentzVector& electron1,
                             const TLorentzVector& electron2,
                             const std::string& chain,
                             std::pair<bool, bool>& result1,
                             std::pair<bool, bool>& result2);
 
  std::pair<double, double>
    trigsf_muons(int runnumber, bool useGeV, std::vector<TLorentzVector> muons, muon_quality q, int var, std::string dilep_trigger_name);

  std::pair<double, double>
    trigsf_leptons(int runnumber, bool useGeV, std::vector<TLorentzVector> muons, muon_quality q, std::vector<TLorentzVector> electrons, electron_quality p, int var, std::string dilep_trigger_name, const bool only_pass_dilep);

  MuonTriggerMatching* my_muonTriggerMatching;
  ElectronTriggerMatching* my_electronTriggerMatching;
  LeptonTriggerSF* my_leptonTriggerSF;     //for single lepton
  LeptonTriggerSF* my_leptonTriggerSF_ee;  //for dilepton
  TriggerNavigationVariables* my_triggerNavigationVariables;
  

 private:

  // Properties:
  bool   fDebug;


};

#endif

