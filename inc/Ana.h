// Developed at ASC ANL
// S.Chekanov (ANL). chakanau@hep.anl.gov
// Feb. 2010 

#include "analysis.h"
#include "Histo.h"
#include "Global.h"
#include <TROOT.h>
#include <TTree.h>

#include "Enum.h"

// main analysis class. inherent analysis.h which should be rebuilkd each time

class Ana: public analysis  {

public:
    Ana(TTree *tree);
    virtual ~Ana();
    int RecoElectrons(Long64_t entry);
    int RecoMuons(Long64_t entry);
    int RecoTracks(Long64_t entry);
    int TruthParticles(Long64_t entry);
    int TruthTracks(Long64_t entry);
    void Loop();

private:

    // Methods to retrieve the leading or the subleading lepton info
    // Will save the info to the variables declared in the class Global
    int GetElectronInfo(int isLeading, LParticle & electron);
    int GetMuonInfo(int isLeading, LParticle & muon);

    // Process tracks matched to selected leptons
    int ProcessMatchedTracks(std::vector<LParticle> &track_lp, TLorentzVector &lepton, std::vector<double> &output);
};
