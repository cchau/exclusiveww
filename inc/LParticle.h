#ifndef LParticle_H 
#define LParticle_H

#include "TMath.h"
#include "TObject.h"
#include "TLorentzVector.h"
#include <vector>
#include <map>
#include <string>
//#include "CParticle.h"


class LParticle: public TObject {
private:
  Int_t m_charge;             // Particle charge
  TLorentzVector momentum;    // Initial momentum (x,y,z,tot)
  std::vector<Double32_t>  parameters;

public:


  LParticle();
  LParticle(Double_t px, Double_t py, Double_t pz, Double_t e, Int_t charge);
  LParticle(LParticle* p);

  LParticle(Int_t charge);
  ~LParticle();

  Int_t GetCharge(){return m_charge;};
  TLorentzVector GetP(){return momentum;};

  void     SetCharge(Int_t q) {m_charge = q;};
  void     SetP(const TLorentzVector& mom){ momentum = mom; };
  void     SetParameter(Double32_t q) { parameters.push_back( q ); };

  std::vector<Double32_t>  GetParameters() { return parameters; };


};

#endif
