#ifndef __MUONTOOLS_H
#define __MUONTOOLS_H

#include "MuonMomentumCorrections/SmearingClass.h"

#include <TH1F.h>
#include <TH1D.h>
#include <TString.h>
#include "TLorentzVector.h"
#include "TMath.h"
#include "TRandom3.h"
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <map>
#include <vector>
#include <fstream>
#include <set>
#include <unistd.h>
#include <iomanip>
#include <utility>

#include "MuonEfficiencyCorrections/AnalysisMuonConfigurableScaleFactors.h"

using namespace MuonSmear;

class MuonTools
{ 
 public:
  
  MuonTools();
  ~MuonTools();
  
  void initialize();

	void tlvSort(std::vector<TLorentzVector> vec);
	void tlvSortWithIndex(std::vector<TLorentzVector> &vec, std::vector<int> &index);
	std::pair<double,double> GetIDSF(TLorentzVector &muons_tlv, int muon_charge, int randomRunNumber);

  SmearingClass *mcp_smear;	
  Analysis::AnalysisMuonConfigurableScaleFactors *muonSF;

 private:

  // Properties:
  bool   fDebug;


};

#endif

