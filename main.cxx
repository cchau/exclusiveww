/***************************************************************************
 *            main.cxx
 *
 *  Fri Sep 21 15:05:23 2007
 *  Copyright  2007  chekanov
 *  chekanov@mail.desy.de
 ****************************************************************************/



#include<iostream>
#include<fstream>
#include<stdlib.h>

#include <TROOT.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TBrowser.h>
#include <TStyle.h>
#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"

// analysis 
#include "Ana.h"
#include "Global.h" 
#include "Histo.h"

//using namespace std;

// user global variables
// user global variables
Global glob;
Histo  h;


int main(int argc, char **argv)
{


    // get dictinary for vector
    gROOT->ProcessLine("#include <vector>");

    // initialize
    glob.getIni();
    // Validate configuration options
    glob.ValidateRunConfig();
    // read list of ntuples
    glob.getNtuples();
    // read cross-section file
    glob.readXsection();

    // set output and histograms
    h.setOutput();
    h.setHistograms();

    const int Nfiles = glob.ntup.size();
    std::cout << " -> No of files to read:" << Nfiles << std::endl;

    // reset sum of weights
    //glob.sumEventWeights_tot = 0.;
    //glob.sumEventWeights_tot_vtxpos = 0.;
    //glob.unWeightedEvent = 0;
    glob.ResetMCEventWeight();


    for (int i = 0; i < Nfiles; i++) {

       float donefiles= (float)i / (float)Nfiles;
       std::cout << "\nPercentage done = " << (int)(donefiles*100) <<  " %" << std::endl;
       std::cout << "\nfile to read:" << glob.ntup[i] << std::endl;

        TFile *f = TFile::Open(glob.ntup[i].c_str()); 
        if (f->IsZombie()) {
            std::cout << "\nThe file looks bad. Skip it:" << glob.ntup[i] << std::endl;
            continue;  
        }

        TTree *tree = (TTree *)f->Get("physics");
        if (tree==0) {
            std::cout << "\nThe tree looks bad. Skip it:" << std::endl;
            continue;
        }


        if (glob.nev >= glob.maxEvents)
            break;

        Ana ana(tree);


        ana.Notify();

        ana.Loop();             // Loop on all entries


        delete tree;
        delete f;
 

    }


    // write histograms  
    h.finalize();
    //std::cout << "--> Number of events requested:" <<  glob.MaxEvents  << std::endl;
    std::cout << "--> Number of events processed:" <<  glob.nev  << std::endl;
    std::cout << "--> Number of events  selected:" <<  glob.TotalEvents << std::endl;


    return 0;
}
