#!/bin/bash

"exec" "python" "-Wignore" "$0" "$@"


from ROOT import *
ROOT.gROOT.SetBatch(1)
from math import *

import sys
print "sys.argv = ", sys.argv


######## get input files
inputFiles=[]
for line in open('inputFiles.txt','r'):
    line=line.replace("\n","")


inputFiles=line.split(",")
print "inputFiles = ", inputFiles


ff=open("data.in","w")
for file in inputFiles:
    ff.write(file+"\n")
