#!/bin/bash
#
# Read a file containing cpp enum 
#  and format each enum element as
#  '${obj}.${funct}( enum element );'
# Input file is of the form:
#   enum ELEC {
#           el0,
#           el1,
#           ...,
#           eln
#   }
#

# Object type
obj='recoEl_lp'
#obj='recoMu_lp'

# function
funct='SetParameter'

# index
ind='iEl'
#ind='iMu'

# flag
goodBlock=false

# enum name space
enum='ELEC'
#enum='MUON'

# input file
file='../inc/Enum.h'

while read line
do

    if [ "$goodBlock" = true ]; then
        str=${line:0:1}
        if [ $str = '}' ]; then
            goodBlock=false
        else
            br1=${line%?}  # trim the last character
            ch=${br1:0:1}
            br2=${br1:2}   # trim the 1st character
            if [ "$ch" = 'E' ]; then
                branch="el${br2}"
            elif [ "$ch" = 'M' ]; then
                branch="mu_staco${br2}"
            else
                branch=''
            fi

            #format output
            echo "${obj}.${funct}( ${branch}->at(${ind}) );"
        fi
    else
        str=${line:0:4}
        if [ "$str" = 'enum' ]; then
            str1=${line:5:4}
            if [ "$str1" = "$enum" ]; then
                goodBlock=true
            fi
        fi
    fi
        
done < ${file}

