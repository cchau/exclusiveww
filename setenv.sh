#!/bin/bash

#export PATH=$HOME/bin:$PATH
export PATH=`pwd`/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$ROOTSYS/lib:$PYTHONPATH
export PATH=.:$PATH
GOODRUN=`pwd`/lib_ext/GoodRunsLists/StandAlone/
ROOTCORE=`pwd`/lib_ext/RootCore/lib/
export LD_LIBRARY_PATH=$GOODRUN:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$ROOTCORE:$LD_LIBRARY_PATH

