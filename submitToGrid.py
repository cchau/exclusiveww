#!/usr/bin/env python
import getopt,os,sys,re
import subprocess

system=os.system

#define grid name
user="user.cchau."
version="_jun_mu1"

if os.path.exists('./lib_ext'):
	system('rm -rf lib_ext external')

# the first argument in the command line is a text file with a list of input dataset
inputFile = sys.argv[1]

# loop over the contents of the input file, to submit jobs for each dataset
f = open(inputFile)
for line in f.readlines():
	input_ds = line.strip()
	if not input_ds.startswith("#"):
  	# replace features of the input dataset
		output_ds = user+input_ds
		output_ds = output_ds.replace('incl','')
		output_ds = output_ds.replace('_Auto','')
		output_ds = output_ds.replace('_CT10','')
		output_ds = output_ds.replace('_AUET2CTEQ6L1','')
		output_ds = output_ds.replace('_P2011C','')
		output_ds = output_ds.replace('_Filtered','')
		output_ds = output_ds.replace('_2LeptonFilter5','')
		output_ds = output_ds.replace('_LeptonPhotonFilter','')
		output_ds = output_ds.replace('Excl','')
		output_ds = output_ds.replace('.merge','')
		output_ds = output_ds.replace('.NTUP_SMWZ','')
		output_ds = output_ds.replace('notauhad.','')
		output_ds = output_ds.replace('/','')
		#output_ds = output_ds.replace('_','')
		#output_ds = re.sub("\.[a-z][0-9](.*)$","",output_ds)
		output_ds = output_ds+version
		rootoutfile = "output.root"	
		print " * input_ds:", input_ds
		print " * output_ds:",output_ds
		print " * root file:",rootoutfile

		#cmd = 'prun --athenaTag 19.0.3.1 --rootVer 5.34.13 --cmtConfig=x86_64-slc6-gcc47-opt --nFilesPerJob=1 --nFiles=1 --extFile=external.tar.gz,lib_ext1.tar.gz,analysis.h,XsectionFile.txt --exec "echo %IN > inputFiles.txt; ./A_RUN clean;" --outDS='+output_ds+' --inDS='+input_ds+' --outputs='+rootoutfile
		#cmd = 'prun --athenaTag 19.0.3.1 --rootVer 5.34.13 --cmtConfig=x86_64-slc6-gcc47-opt --nGBPerJob=MAX --extFile=external.tar.gz,lib_ext.tar.gz,analysis.h, XsectionFile.txt --exec "echo %IN > inputFiles.txt; ./A_RUN clean;" --outDS='+output_ds+' --inDS='+input_ds+' --outputs='+rootoutfile
		#cmd = 'prun --athenaTag 19.0.3.1 --rootVer 5.34.13 --cmtConfig=x86_64-slc6-gcc47-opt --nGBPerJob=MAX --extFile=external.tar.gz,lib_ext.tar.gz,analysis.h, XsectionFile.txt --exec "echo %IN > inputFiles.txt; ./A_RUN clean;" --outDS='+output_ds+' --inDS='+input_ds+' --outputs='+rootoutfile
		cmd = 'prun --athenaTag 19.0.3.1 --rootVer 5.34.13 --cmtConfig=x86_64-slc6-gcc47-opt --excludedSite=ANALY_NIKHEF-ELPROD,ANALY_NIKHEF-ELPROD_SHORT --nGBPerJob=MAX --extFile=external.tar.gz,lib_ext.tar.gz,analysis.h, XsectionFile.txt --exec "echo %IN > inputFiles.txt; ./A_RUN clean;" --outDS='+output_ds+' --inDS='+input_ds+' --outputs='+rootoutfile
		print input_ds
		print cmd
		system( cmd )
