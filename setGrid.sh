#!/bin/bash
echo "Set environment for the GRID"
export AVERS=19.0.3.1
#export ALRB_localConfigDir="/share/sl6/cvmfs/localConfig"
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup $AVERS,slc6,here #  --testarea=$TEST_AREA
#localSetupDQ2Wrappers
#localSetupDQ2Client
#localSetupPandaClient --noAthenaCheck
localSetupPandaClient
#voms-proxy-init -voms atlas -valid 96:00
localSetupROOT
#tar czf lib_ext.tar.gz lib_ext/*
#tar czf external.tar.gz external/*
